<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<!-- General CSS Files -->
<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

<!-- CSS Libraries -->
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/sweetalert2/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">

<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

<style>
  .navbar-bg {
    background-color: #Da2827!important;
  }

.main-sidebar .sidebar-menu li.active a {
    color: #Da2827;
  }
  .main-sidebar .sidebar-menu li ul.dropdown-menu li.active > a {
    color: #Da2827;
    font-weight: 600;
  }

.main-sidebar .sidebar-menu li ul.dropdown-menu li a:hover {
    color: #Da2827;
}

@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
     max-width:1200px;
    }
  }

  .breadcrumb-item a {
    color: #Da2827!important;
  }
</style>
