@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                @endif
            </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown active">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li class="active"><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Actual Cost</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">Cost Management</div>
                <div class="breadcrumb-item active">Actual Cost</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                @if(Session::get('role_id') == 2)
                                <a onclick="addData()" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if(Session::get('role_id') == 1)
                            <label for="#"><b>Filter :</b></label>
                                <div class="row">
                                    <div class="col-3">
                                        <select class="form-control select2" style="width:100%" name="company" id="company">
                                            
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-striped" id="driver-cost-datatable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- Modal --}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="status" name="status" value="1">
                    <input type="hidden" id="company_id" name="company_id" value="{{ Session::get('company_id')}}">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Nama</label>
                        <input type="text" name="name" class="form-control" id="name" minlength=3 required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}

@endsection

@section('javascript')

<script type="text/javascript">
$(function() {
    var role_id = "{{Session::get('role_id')}}";
    
    if (role_id == 1) {
        var table = $('#driver-cost-datatable').DataTable();

        $.ajax({
            url: "{{ url('api/v1/companies') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#company').empty();
                $('#company').append('<option disabled selected>-- Select Company --</option>');
                $.each(response.data, function(key, val){
                    $('#company').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $("#company").change(function () {
            var company_id = $(this).val();
            var url = "{{ url('api/v1/costs')}}" +  "?company_id=" + company_id + "&status=1";

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    table.clear().draw();
                    $.each(response.data, function (key, val) {
                        table.row.add([
                            val.name, val.action
                        ]).draw();
                    });
                }
            });
        });

    } else {
        var url = "{{ url('api/v1/costs')}}" +  "?company_id=" + "{{ Session::get('company_id')}}" + "&status=1";
        var table = $('#driver-cost-datatable').DataTable({
            processing: true,
            ajax: url,
            columns: [
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    }

});

    function refresh() {
        $('#driver-cost-datatable').DataTable().ajax.reload();
    }

    $('#modal-form').on('hidden.bs.modal', function (e) {
        $(".form-control").prop("disabled", false);
        $('button[type="submit"]').show();
    })

    function showData(id) {
        save_method = "show";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('#modal-form form')[0].reset();

        $.ajax({
            url: "{{ url('api/v1/costs') }}" + '/' + id,
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Show Data');

                $('#id').val(response.data.id);
                $('#name').val(response.data.name);

                $(".form-control").prop("disabled", true);
                $('button[type="submit"]').hide();
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function addData() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Input Data');
    }

    function editData(id) {
        save_method = "edit";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/costs') }}" + '/' + id,
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                $('#name').val(response.data.name);
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $.ajax({
                url : "{{ url('api/v1/costs') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success : function(response) {
                    $('#driver-cost-datatable').DataTable().ajax.reload();
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function (response) {
                    swal({
                        title: 'Oops...',
                        text: response.responseText,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/v1/costs') }}";
                else url = "{{ url('api/v1/costs') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : $('#modal-form form').serialize(),
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        $('#driver-cost-datatable').DataTable().ajax.reload();
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                      $('#modal-form').modal('hide');
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

</script>

@endsection
