<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
        </ul>
    </form>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
                <div class="dropdown-header">Notifications
                <div class="float-right">
                    <a href="#" id="notification-read-all">Mark All As Read</a>
                </div>
                </div>
                <div class="dropdown-list-content dropdown-list-icons" id="notifications">
                    
                </div>
                
            </div>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown"
                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ asset('assets/img/avatar/avatar-1.png') }}" width="30"
                    class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block">{{ Session::get('username')}}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-divider"></div>
                <a href="#" onclick="downloadManualbook();" class="dropdown-item has-icon">
                        <i class="fas fa-book"></i> Manual Book
                    </a>
                <a href="#" id="change-password" class="dropdown-item has-icon">
                    <i class="fas fa-key"></i> Change Password
                </a>
                <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
