<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fleet Management</title>

  @include('common.styles')

</head>
<body>

  <div id="app">
    <div class="wrapper">
      @include('components.header')
        @yield('content')
      @include('password.modal-change-password')
      @include('components.footer')
    </div>
  </div>

  @include('common.scripts')
  @include('password.scripts-change-password')
  @include('notifications.scripts-notification')
  @include('manualbook.script-download')
  @yield('javascript')

</body>
</html>