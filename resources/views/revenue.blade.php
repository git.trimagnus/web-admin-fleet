@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                @endif
            </ul>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
        </li>
        <li class="dropdown active">
            <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
            </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Revenue</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item active">Revenue</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <label for="datepicker"><b>Filter :</b></label>
                            <div class="row">
                                <div class="col-2">
                                    <select class="form-control result" name="year" id="year">
                                        <option disabled selected>-- Select Year --</option>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <a onclick="refresh()" class="btn btn-outline-warning">
                                            <i class="fas fa-sync-alt"></i> Refresh</a>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-striped" id="revenue-datatable">
                                    <thead>
                                        <tr>
                                            <th>Bulan</th>
                                            <th>Revenue</th>
                                            <th>Total Company</th>  
                                            <th>Rata - rata</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>January</td>
                                            <td class="sum-revenue" id="total_revenue_january"></td>
                                            <td class="sum-company" id="total_company_january"></td>
                                            <td class="sum-average" id="average_january"></td>
                                        </tr>
                                        <tr>
                                            <td>February</td>
                                            <td class="sum-revenue" id="total_revenue_february"></td>
                                            <td class="sum-company" id="total_company_february"></td>
                                            <td class="sum-average" id="average_february"></td>
                                        </tr>
                                        <tr>
                                            <td>March</td>
                                            <td class="sum-revenue" id="total_revenue_march"></td>
                                            <td class="sum-company" id="total_company_march"></td>
                                            <td class="sum-average" id="average_march"></td>
                                        </tr>
                                        <tr>
                                            <td>April</td>
                                            <td class="sum-revenue" id="total_revenue_april"></td>
                                            <td class="sum-company" id="total_company_april"></td>
                                            <td class="sum-average" id="average_april"></td>
                                        </tr>
                                        <tr>
                                            <td>Mei</td>
                                            <td class="sum-revenue" id="total_revenue_mei"></td>
                                            <td class="sum-company" id="total_company_mei"></td>
                                            <td class="sum-average" id="average_mei"></td>
                                        </tr>
                                        <tr>
                                            <td>June</td>
                                            <td class="sum-revenue" id="total_revenue_june"></td>
                                            <td class="sum-company" id="total_company_june"></td>
                                            <td class="sum-average" id="average_june"></td>
                                        </tr>
                                        <tr>
                                            <td>July</td>
                                            <td class="sum-revenue" id="total_revenue_july"></td>
                                            <td class="sum-company" id="total_company_july"></td>
                                            <td class="sum-average" id="average_july"></td>
                                        </tr>
                                        <tr>
                                            <td>August</td>
                                            <td class="sum-revenue" id="total_revenue_august"></td>
                                            <td class="sum-company" id="total_company_august"></td>
                                            <td class="sum-average" id="average_august"></td>
                                        </tr>
                                        <tr>
                                            <td>September</td>
                                            <td class="sum-revenue" id="total_revenue_september"></td>
                                            <td class="sum-company" id="total_company_september"></td>
                                            <td class="sum-average" id="average_september"></td>
                                        </tr>
                                        <tr>
                                            <td>October</td>
                                            <td class="sum-revenue" id="total_revenue_october"></td>
                                            <td class="sum-company" id="total_company_october"></td>
                                            <td class="sum-average" id="average_october"></td>
                                        </tr>
                                        <tr>
                                            <td>November</td>
                                            <td class="sum-revenue" id="total_revenue_november"></td>
                                            <td class="sum-company" id="total_company_november"></td>
                                            <td class="sum-average" id="average_november"></td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td class="sum-revenue" id="total_revenue_december"></td>
                                            <td class="sum-company" id="total_company_december"></td>
                                            <td class="sum-average" id="average_december"></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th id="sum_revenue">-</th>
                                            <th id="sum_company">-</th>
                                            <th id="sum_average">-</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('javascript')

<script type="text/javascript">
    $(function(){
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;

        $.ajax({
            url: "{{url('/api/v1/years')}}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#year').empty();
                $('#year').append('<option disabled selected>-- Select Year --</option>');
                $.each(response.data, function(key, val){
                    $('#year').append('<option>' + val.year + '</option>');
                });

                $("#year").val(year).attr('selected','selected');
            result(year);
            }
        });
    });

    $(function() {
        $(".result").change(function () {
            var year = $('#year').val();
            result(year);
        });
    });

    function result(year) {
        var url = "{{url('/')}}" + "/api/v1/revenues?year=" + year;
            $.ajax({
                url: url,
                type: "GET",
                data:{
                    'year' : year
                },
                success: function (response) {

                    var total_revenue_january = response.data.total_revenue_january;
                    var total_company_january = response.data.total_company_january;
                    var average_january = response.data.average_january;
                    var total_revenue_february = response.data.total_revenue_february;
                    var total_company_february = response.data.total_company_february;
                    var average_february = response.data.average_february;
                    var total_revenue_march = response.data.total_revenue_march;
                    var total_company_march = response.data.total_company_march;
                    var average_march = response.data.average_march;
                    var total_revenue_april = response.data.total_revenue_april;
                    var total_company_april = response.data.total_company_april;
                    var average_april = response.data.average_april;
                    var total_revenue_mei = response.data.total_revenue_mei;
                    var total_company_mei = response.data.total_company_mei;
                    var average_mei = response.data.average_mei;
                    var total_revenue_june = response.data.total_revenue_june;
                    var total_company_june = response.data.total_company_june;
                    var average_june = response.data.average_june;
                    var total_revenue_july = response.data.total_revenue_july;
                    var total_company_july = response.data.total_company_july;
                    var average_july = response.data.average_july;
                    var total_revenue_august = response.data.total_revenue_august;
                    var total_company_august = response.data.total_company_august;
                    var average_august = response.data.average_august;
                    var total_revenue_september = response.data.total_revenue_september;
                    var total_company_september = response.data.total_company_september;
                    var average_september = response.data.average_september;
                    var total_revenue_october = response.data.total_revenue_october;
                    var total_company_october = response.data.total_company_october;
                    var average_october = response.data.average_october;
                    var total_revenue_november = response.data.total_revenue_november;
                    var total_company_november = response.data.total_company_november;
                    var average_november = response.data.average_november;
                    var total_revenue_december = response.data.total_revenue_december;
                    var total_company_december = response.data.total_company_december;
                    var average_december = response.data.average_december;

                    $('#total_revenue_january').text(total_revenue_january == null ? 0 : total_revenue_january);
                    $('#total_company_january').text(total_company_january == null ? 0 : total_company_january);
                    $('#average_january').text(average_january == null ? 0 : average_january);
                    $('#total_revenue_february').text(total_revenue_february == null ? 0 : total_revenue_february);
                    $('#total_company_february').text(total_company_february == null ? 0 : total_company_february);
                    $('#average_february').text(average_february == null ? 0 : average_february);
                    $('#total_revenue_march').text(total_revenue_march == null ? 0 : total_revenue_march);
                    $('#total_company_march').text(total_company_march == null ? 0 : total_company_march);
                    $('#average_march').text(average_march == null ? 0 : average_march);
                    $('#total_revenue_april').text(total_revenue_april == null ? 0 : total_revenue_april);
                    $('#total_company_april').text(total_company_april == null ? 0 : total_company_april);
                    $('#average_april').text(average_april == null ? 0 : average_april);
                    $('#total_revenue_mei').text(total_revenue_mei == null ? 0 : total_revenue_mei);
                    $('#total_company_mei').text(total_company_mei == null ? 0 : total_company_mei);
                    $('#average_mei').text(average_mei == null ? 0 : average_mei);
                    $('#total_revenue_june').text(total_revenue_june == null ? 0 : total_revenue_june);
                    $('#total_company_june').text(total_company_june == null ? 0 : total_company_june);
                    $('#average_june').text(average_june == null ? 0 : average_june);
                    $('#total_revenue_july').text(total_revenue_july == null ? 0 : total_revenue_july);
                    $('#total_company_july').text(total_company_july == null ? 0 : total_company_july);
                    $('#average_july').text(average_july == null ? 0 : average_july);
                    $('#total_revenue_august').text(total_revenue_august == null ? 0 : total_revenue_august);
                    $('#total_company_august').text(total_company_august == null ? 0 : total_company_august);
                    $('#average_august').text(average_august == null ? 0 : average_august);
                    $('#total_revenue_september').text(total_revenue_september == null ? 0 : total_revenue_september);
                    $('#total_company_september').text(total_company_september == null ? 0 : total_company_september);
                    $('#average_september').text(average_september == null ? 0 : average_september);
                    $('#total_revenue_october').text(total_revenue_october == null ? 0 : total_revenue_october);
                    $('#total_company_october').text(total_company_october == null ? 0 : total_company_october);
                    $('#average_october').text(average_october == null ? 0 : average_october);
                    $('#total_revenue_november').text(total_revenue_november == null ? 0 : total_revenue_november);
                    $('#total_company_november').text(total_company_november == null ? 0 : total_company_november);
                    $('#average_november').text(average_november == null ? 0 : average_november);
                    $('#total_revenue_december').text(total_revenue_december == null ? 0 : total_revenue_december);
                    $('#total_company_december').text(total_company_december == null ? 0 : total_company_december);
                    $('#average_december').text(average_december == null ? 0 : average_december);

                    var sum_revenue = 0;
                    $('.sum-revenue').each(function(){
                        sum_revenue += parseFloat($(this).text());
                    });

                    var sum_company = 0;
                    $('.sum-company').each(function(){
                        sum_company += parseFloat($(this).text());
                    });

                    var sum_average = 0;
                    $('.sum-average').each(function(){
                        sum_average += parseFloat($(this).text());
                    });

                    $('#sum_revenue').text(sum_revenue);
                    $('#sum_company').text(sum_company);
                    $('#sum_average').text(sum_average);

                }
            });
    }

    function refresh() {
        var year = $('#year').val();
        
        var url = "{{url('/')}}" + "/api/v1/revenues?year=" + year;
            $.ajax({
                url: url,
                type: "GET",
                data:{
                    'year' : year
                },
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success: function (response) {

                    var total_revenue_january = response.data.total_revenue_january;
                    var total_company_january = response.data.total_company_january;
                    var average_january = response.data.average_january;
                    var total_revenue_february = response.data.total_revenue_february;
                    var total_company_february = response.data.total_company_february;
                    var average_february = response.data.average_february;
                    var total_revenue_march = response.data.total_revenue_march;
                    var total_company_march = response.data.total_company_march;
                    var average_march = response.data.average_march;
                    var total_revenue_april = response.data.total_revenue_april;
                    var total_company_april = response.data.total_company_april;
                    var average_april = response.data.average_april;
                    var total_revenue_mei = response.data.total_revenue_mei;
                    var total_company_mei = response.data.total_company_mei;
                    var average_mei = response.data.average_mei;
                    var total_revenue_june = response.data.total_revenue_june;
                    var total_company_june = response.data.total_company_june;
                    var average_june = response.data.average_june;
                    var total_revenue_july = response.data.total_revenue_july;
                    var total_company_july = response.data.total_company_july;
                    var average_july = response.data.average_july;
                    var total_revenue_august = response.data.total_revenue_august;
                    var total_company_august = response.data.total_company_august;
                    var average_august = response.data.average_august;
                    var total_revenue_september = response.data.total_revenue_september;
                    var total_company_september = response.data.total_company_september;
                    var average_september = response.data.average_september;
                    var total_revenue_october = response.data.total_revenue_october;
                    var total_company_october = response.data.total_company_october;
                    var average_october = response.data.average_october;
                    var total_revenue_november = response.data.total_revenue_november;
                    var total_company_november = response.data.total_company_november;
                    var average_november = response.data.average_november;
                    var total_revenue_december = response.data.total_revenue_december;
                    var total_company_december = response.data.total_company_december;
                    var average_december = response.data.average_december;

                    $('#total_revenue_january').text(total_revenue_january == null ? 0 : total_revenue_january);
                    $('#total_company_january').text(total_company_january == null ? 0 : total_company_january);
                    $('#average_january').text(average_january == null ? 0 : average_january);
                    $('#total_revenue_february').text(total_revenue_february == null ? 0 : total_revenue_february);
                    $('#total_company_february').text(total_company_february == null ? 0 : total_company_february);
                    $('#average_february').text(average_february == null ? 0 : average_february);
                    $('#total_revenue_march').text(total_revenue_march == null ? 0 : total_revenue_march);
                    $('#total_company_march').text(total_company_march == null ? 0 : total_company_march);
                    $('#average_march').text(average_march == null ? 0 : average_march);
                    $('#total_revenue_april').text(total_revenue_april == null ? 0 : total_revenue_april);
                    $('#total_company_april').text(total_company_april == null ? 0 : total_company_april);
                    $('#average_april').text(average_april == null ? 0 : average_april);
                    $('#total_revenue_mei').text(total_revenue_mei == null ? 0 : total_revenue_mei);
                    $('#total_company_mei').text(total_company_mei == null ? 0 : total_company_mei);
                    $('#average_mei').text(average_mei == null ? 0 : average_mei);
                    $('#total_revenue_june').text(total_revenue_june == null ? 0 : total_revenue_june);
                    $('#total_company_june').text(total_company_june == null ? 0 : total_company_june);
                    $('#average_june').text(average_june == null ? 0 : average_june);
                    $('#total_revenue_july').text(total_revenue_july == null ? 0 : total_revenue_july);
                    $('#total_company_july').text(total_company_july == null ? 0 : total_company_july);
                    $('#average_july').text(average_july == null ? 0 : average_july);
                    $('#total_revenue_august').text(total_revenue_august == null ? 0 : total_revenue_august);
                    $('#total_company_august').text(total_company_august == null ? 0 : total_company_august);
                    $('#average_august').text(average_august == null ? 0 : average_august);
                    $('#total_revenue_september').text(total_revenue_september == null ? 0 : total_revenue_september);
                    $('#total_company_september').text(total_company_september == null ? 0 : total_company_september);
                    $('#average_september').text(average_september == null ? 0 : average_september);
                    $('#total_revenue_october').text(total_revenue_october == null ? 0 : total_revenue_october);
                    $('#total_company_october').text(total_company_october == null ? 0 : total_company_october);
                    $('#average_october').text(average_october == null ? 0 : average_october);
                    $('#total_revenue_november').text(total_revenue_november == null ? 0 : total_revenue_november);
                    $('#total_company_november').text(total_company_november == null ? 0 : total_company_november);
                    $('#average_november').text(average_november == null ? 0 : average_november);
                    $('#total_revenue_december').text(total_revenue_december == null ? 0 : total_revenue_december);
                    $('#total_company_december').text(total_company_december == null ? 0 : total_company_december);
                    $('#average_december').text(average_december == null ? 0 : average_december);

                    var sum_revenue = 0;
                    $('.sum-revenue').each(function(){
                        sum_revenue += parseFloat($(this).text());
                    });

                    var sum_company = 0;
                    $('.sum-company').each(function(){
                        sum_company += parseFloat($(this).text());
                    });

                    var sum_average = 0;
                    $('.sum-average').each(function(){
                        sum_average += parseFloat($(this).text());
                    });

                    $('#sum_revenue').text(sum_revenue);
                    $('#sum_company').text(sum_company);
                    $('#sum_average').text(sum_average);

                    swal.close();

                }
            });
    }

</script>

@endsection
