@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                @endif
            </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown active">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Maintenance</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item active">Maintenance</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                @if(Session::get('role_id' ) == 2)
                                <a onclick="addData()" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if(Session::get('role_id') == 1)
                            <label for="#"><b>Filter :</b></label>
                                <div class="row">
                                    <div class="col-3">
                                        <select class="form-control select2" style="width:100%" name="company" id="company">
                                            
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-striped" id="maintenance-datatable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>No. Maintenance</th>
                                            <th>Tanggal</th>
                                            <th>Armada</th>
                                            <th>Jenis</th>
                                            <th>Status</th>      
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('maintenances.modal-form')

@endsection

@section('javascript')

<script type="text/javascript">
$(function() {
    var role_id = "{{Session::get('role_id')}}";
    
    if (role_id == 1) {
        var table = $('#maintenance-datatable').DataTable({
            columnDefs: [
                {
                    targets: [ 0 ],
                    visible: false,
                    searchable: false
                },
            ],
            order: [[ 0, "desc" ]]
        });

        $.ajax({
            url: "{{ url('api/v1/companies') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#company').empty();
                $('#company').append('<option disabled selected>-- Select Company --</option>');
                $.each(response.data, function(key, val){
                    $('#company').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $("#company").change(function () {
            var company_id = $(this).val();
            var url = "{{ url('api/v1/maintenances')}}" +  "?company_id=" + company_id;

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    table.clear().draw();
                    $.each(response.data, function (key, val) {
                        table.row.add([
                            val.id, val.no_maintenance, val.created_at, val.armada.name, 
                            val.maintenancetype.name, val.status, val.action
                        ]).draw();
                    });
                }
            });
        });

    } else {
        var url = "{{ url('api/v1/maintenances')}}" +  "?company_id=" + "{{ Session::get('company_id')}}";
        var table = $('#maintenance-datatable').DataTable({
            processing: true,
            ajax: url,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'no_maintenance', name: 'no_maintenance'},
                {data: 'created_at', name: 'created_at'},
                {data: 'armada.name', name: 'armada.name'},
                {data: 'maintenancetype.name', name: 'maintenancetype.name'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            columnDefs: [
                {
                    targets: [ 0 ],
                    visible: false,
                    searchable: false
                },
            ],
            order: [[ 0, "desc" ]]
        });
    }

});

    $('#date_accident').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });

    $('#expected_complete').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });

    function showFile(fileInput, img, showName) {
      if (fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(img).attr('src', e.target.result);
        }
        reader.readAsDataURL(fileInput.files[0]);
        }
        $('#image_1_preview').text(fileInput.files[0].name)
        $('#image_2_preview').text(fileInput.files[0].name)
    }
    
    $('#image_1').on('change', function() {
        showFile(this, '#image_1_preview');
    });

    $('#image_2').on('change', function() {
        showFile(this, '#image_2_preview');
    });

    function refresh() {
        $('#maintenance-datatable').DataTable().ajax.reload();
    }

    $('#modal-form').on('hidden.bs.modal', function (e) {
        $(".form-control").prop("disabled", false);
        $('button[type="submit"]').show();
    })

    function showData(id) {
        save_method = "show";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('#modal-form form')[0].reset();

        $.ajax({
            url: "{{ url('api/v1/maintenances') }}" + '/' + id,
            type: "GET",
            data : {'company_id' : company_id},
            success: function (response) {
                var armada_id = response.data.armada_id;

                $.ajax({
                    url: "{{ url('api/v1/armadas') }}",
                    type: "GET",
                    data : {'company_id' : company_id},
                    success: function (response) {
                        $('#armada_id').empty();
                        $('#armada_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == armada_id) {
                                $('#armada_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $('#modal-form').modal('show');
                $('.modal-title').text('Show Data');

                $('#id').val(response.data.id);
                $('#maintenancetype_id').val(response.data.maintenancetype_id).attr('selected','selected');
                $('#reporter').val(response.data.reporter);
                $('#odometer').val(response.data.odometer);
                $('#date_accident').val(response.data.date_accident);
                $('#expected_complete').val(response.data.expected_complete);
                $('#location_accident').val(response.data.location_accident);
                $('#description_accident').val(response.data.description_accident);
                $('#chronologic').val(response.data.chronologic);
                $('#hidden_image_1').val(response.data.image_1);
                $('#image_1_preview').attr('src', response.data.image_1);
                $('#hidden_image_2').val(response.data.image_2);
                $('#image_2_preview').attr('src', response.data.image_2);
                $('#vendor').val(response.data.vendor);
                $('#performance_vendor').val(response.data.performance_vendor);
                $('#cost').val(response.data.cost);
                $('#note').val(response.data.note);
                $('#status').val(response.data.status).attr('selected','selected');

                $(".form-control").prop("disabled", true);
                $('button[type="submit"]').hide();

            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function addData() {
        save_method = "add";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#image_1_preview').attr('src', '#');
        $('#image_2_preview').attr('src', '#');
        $('#modal-form form')[0].reset();

        $.ajax({
            url: "{{ url('api/v1/armadas') }}",
            type: "GET",
            data : {'company_id' : company_id},
            success: function (response) {
                $('#armada_id').empty();
                $('#armada_id').append('<option value="" disabled selected>-</option>');
                $.each(response.data, function(key, val){
                    $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $('#status').prop('selectedIndex',0);

        $('.modal-title').text('Input Data');
    }

    function editData(id) {
        save_method = "edit";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();

        $.ajax({
            url: "{{ url('api/v1/maintenances') }}" + '/' + id,
            type: "GET",
            data : {'company_id' : company_id},
            success: function (response) {
                var armada_id = response.data.armada_id;

                $.ajax({
                    url: "{{ url('api/v1/armadas') }}",
                    type: "GET",
                    data : {'company_id' : company_id},
                    success: function (response) {
                        $('#armada_id').empty();
                        $('#armada_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == armada_id) {
                                $('#armada_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                $('#maintenancetype_id').val(response.data.maintenancetype_id).attr('selected','selected');
                $('#reporter').val(response.data.reporter);
                $('#odometer').val(response.data.odometer);
                $('#date_accident').val(response.data.date_accident);
                $('#expected_complete').val(response.data.expected_complete);
                $('#location_accident').val(response.data.location_accident);
                $('#description_accident').val(response.data.description_accident);
                $('#chronologic').val(response.data.chronologic);
                $('#hidden_image_1').val(response.data.image_1);
                $('#image_1_preview').attr('src', response.data.image_1);
                $('#hidden_image_2').val(response.data.image_2);
                $('#image_2_preview').attr('src', response.data.image_2);
                $('#vendor').val(response.data.vendor);
                $('#performance_vendor').val(response.data.performance_vendor);
                $('#cost').val(response.data.cost);
                $('#note').val(response.data.note);
                $('#status').val(response.data.status).attr('selected','selected');

            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $.ajax({
                url : "{{ url('api/v1/maintenances') }}" + '/' + id,
                type : "POST",
                data : {
                    '_method' : 'DELETE', 
                    '_token' : csrf_token,
                    'company_id' : company_id
                },
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success : function(response) {
                    $('#maintenance-datatable').DataTable().ajax.reload();
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function () {
                    swal({
                        title: 'Oops...',
                        text: response.responseText,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/v1/maintenances') }}";
                else url = "{{ url('api/v1/maintenances') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : new FormData($("#form")[0]),
                    contentType : false, 
                    processData : false,
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        $('#maintenance-datatable').DataTable().ajax.reload();
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                      $('#modal-form').modal('hide');
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

</script>

@endsection
