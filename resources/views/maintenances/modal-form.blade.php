{{-- Modal Form --}}
<div class="modal fade" id="modal-form" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="form" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="company_id" name="company_id" value="{{ Session::get('company_id')}}">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="keterangan-maintenance-tab" data-toggle="tab" href="#keterangan-maintenance" role="tab" aria-controls="keterangan-maintenance" aria-selected="true">Keterangan Maintenance</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="additional-tab" data-toggle="tab" href="#additional" role="tab" aria-controls="additional" aria-selected="false">
                                Additional Service / Bengkel Luar
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="keterangan-maintenance" role="tabpanel" aria-labelledby="keterangan-maintenance-tab">
                            <div class="row">
                                <div class="col-4 offset-2">
                                    <div class="form-group">
                                        <label for="maintenancetype_id">Jenis Perawatan*</label>
                                        <select class="form-control" id="maintenancetype_id" name="maintenancetype_id" required>
                                            <option disabled selected>-</option>
                                            <option value="1">Rutin</option>
                                            <option value="2">Non Rutin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="status">Status Maintenance*</label>
                                        <select class="form-control" id="status" name="status" required>
                                            <option disabled value="" selected>-</option>
                                            <option value="0">Open</option>
                                            <option value="1">On Progress</option>
                                            <option value="2">Pending</option>
                                            <option value="3">Done</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="armada_id">Armada*</label>
                                        <select class="form-control select2" style="width:100%" id="armada_id" name="armada_id" required>
                                     
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="odometer">Odometer</label>
                                        <input type="text" name="odometer" class="form-control" id="odometer">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="reporter">Nama Pelapor*</label>
                                        <input type="text" name="reporter" class="form-control" id="reporter">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="date_accident">Tanggal Kejadian</label>
                                        <input type="text" name="date_accident" class="form-control" id="date_accident">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="expected_complete">Tanggal Ekspektasi Selesai</label>
                                        <input type="text" name="expected_complete" class="form-control" id="expected_complete">
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="location_accident">Lokasi Kejadian(Kecelakaan)</label>
                                        <textarea name="location_accident" class="form-control" id="location_accident" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="description_accident">Detail Kerusakan</label>
                                        <textarea name="description_accident" class="form-control" id="description_accident" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="chronologic">Kronologi</label>
                                        <textarea name="chronologic" class="form-control" id="chronologic" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image_1">Photo 1</label>
                                        <input type='file' name="image_1" class="form-control" id="image_1" accept="image/*">
                                        <input type="hidden" name="hidden_image_1" id="hidden_image_1">
                                        <img id="image_1_preview" src="#" width="350" height="300" alt="image_1">      
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image_2">Photo 2</label>
                                        <input type="file" name="image_2" class="form-control" id="image_2" accept="image/*">
                                        <input type="hidden" name="hidden_image_2" id="hidden_image_2">
                                        <img id="image_2_preview" src="#" width="350" height="300" alt="image_2" data-name="coba">
                                    </div>
                                </div>
                            </div>        
                        </div>
                        <div class="tab-pane fade" id="additional" role="tabpanel" aria-labelledby="additional">
                            <div class="row"> 
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="vendor">Nama Bengkel</label>
                                        <input type="text" name="vendor" class="form-control" id="vendor">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="performance_vendor">Kinerja Bengkel</label>
                                        <select class="form-control" id="performance_vendor" name="performance_vendor">
                                            <option value="" selected>-</option>
                                            <option value="kurang">kurang</option>
                                            <option value="cukup">cukup</option>
                                            <option value="bagus">bagus</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="cost">Total Biaya</label>
                                        <input type="text" name="cost" class="form-control" id="cost">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="note">Notes</label>
                                        <textarea name="note" class="form-control" id="note" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal Form --}}