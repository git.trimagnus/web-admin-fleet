<script type="text/javascript">

function downloadManualbook() {
    swal({
        title: 'Download Manual Book?',
        // text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Download',
    }).then(function () {
        var url = "{{config('global.url')}}" + '/uploads/manualbook.zip';
        window.open(url);
    });
}
  
</script>
