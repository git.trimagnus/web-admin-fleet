<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

  <style>
    .btn-danger, .btn-danger.disabled {
        box-shadow: 0 2px 6px #acb5f6;
        background-color: #Da2827;
        border-color: #Da2827;
    }
  </style>
</head>
<body>
    <div id="app">
        <section class="section">
          <div class="container mt-5">
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    <img src="{{URL::to('icons/logo.png')}}" alt="Logo" width="140px">
                </div>
    
                <div class="card card-danger">
                  <div class="card-header"><h4>Login</h4></div>

                  <div class="card-body">
                    <form action="{{ action('Auth\LoginController@doLogin') }}" method="post">
                        @csrf
                      <div class="form-group has-feedback">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control" name="email" required>
                      </div>
    
                      <div class="form-group has-feedback">
                        <div class="d-block">
                            <label for="password" class="control-label">Password</label>
                        </div>
                        <input id="password" type="password" class="form-control" name="password" minlength="8" required>
                        @if ($message = Session::get('error'))
                        <span>
                          <strong>{{ $message }}</strong>
                        </span>
                        @endif
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn btn-danger btn-lg btn-block" tabindex="4">
                          Login
                        </button>
                      </div>

                      <div class="form-group">
                      <a class="btn btn-link" href="{{ route('password.reset') }}">
                          {{ __('Forgot Your Password?') }}
                      </a>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; 2019 PT Fitlogindo Mega Tama All rights reserved.
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<!-- General JS Scripts -->
<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
<script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/stisla.js') }}"></script>

<!-- Template JS File -->
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>