{{-- Modal --}}
<div class="modal fade" id="modal-form-change-password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('POST')
                    <div class="form-group">
                        <label for="old_password" class="col-form-label">Old Password</label>
                        <input type="text" name="old_password" class="form-control" id="old_password" required>
                    </div>
                    <div class="form-group">
                        <label for="new_password" class="col-form-label">New Password</label>
                        <input type="text" name="new_password" class="form-control" id="new_password" minlength="8" required>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="col-form-label">New Password Confirmation</label>
                        <input type="text" name="password_confirmation" class="form-control" id="password_confirmation" minlength="8" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}