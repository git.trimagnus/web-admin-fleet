<script type="text/javascript">
    $(function () {
        $('#change-password').on('click', function () {
            $('#modal-form-change-password form input[name=_method]').val('PUT');
            $('#modal-form-change-password').modal('show');
            $('#modal-form-change-password form')[0].reset();
            $('#modal-form-change-password .modal-title').text('Change Password');
        });

        $('#modal-form-change-password form').on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                $.ajax({
                    url: "{{ url('api/v1/users/change-password') }}",
                    type: "POST",
                    data: $('#modal-form-change-password form').serialize(),
                    success: function (response) {
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                        $('#modal-form-change-password').modal('hide');
                    },
                    error: function (response) {
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });
</script>
