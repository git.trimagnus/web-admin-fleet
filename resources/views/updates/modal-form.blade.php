{{-- Modal --}}
<div class="modal fade" id="modal-form" trole="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="form" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="text" id="id" name="id">
                <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Input Title</label>
                                <input type="text" name="Title" class="form-control" id="Title" placeholder="Title">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Input URL Media / Youtube</label>
                                <input type="text" name="video_url" class="form-control" id="video_url" placeholder="URL">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="image_upload">Input Photo</label>
                                <input type="file" name="image_upload" class="form-control" id="image_upload" accept="image/*">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Tampilkan di website ?</label>
                                <select class="form-control" id="tampilkan" name="tampilkan" class="tampilkan">
                                    <option value="1">YES</option>
                                    <option value="0">NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Select Category</label>
                                <select class="form-control" id="is_image" name="is_image" class="is_image">
                                    <option value="1">Image</option>
                                    <option value="0">Video</option>
                                    <option value="2">Blog</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <input type="hidden" name="hidden_image_preview" id="hidden_image_preview">
                            <img id="image_preview" src="#" width="100%" height="auto" alt="image_preview">
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Desciption</label>
                                <center>
                                    <textarea class="description" name="description" id="description"></textarea>
                                    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
                                    <script>
                                        tinymce.init({
                                            selector:'textarea.description',
                                            width: 900,
                                            height: 300
                                        });
                                    </script>
                                </center>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}