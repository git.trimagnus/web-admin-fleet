<script type="text/javascript">
    $(function () {
        $.ajax({
            url: "{{ url('api/v1/notifications') }}",
            type: "GET",
            success: function (response) {
                
                if (response.unread != 0) {
                    $(".notification-toggle").addClass("beep");
                }

                $('#notifications').empty();
                $.each(response.data, function(key, val){
                    var notifications = '<a href="#" class="dropdown-item">'+
                    '                        <div class="dropdown-item-icon bg-info text-white">'+
                    '                        <i class="fas fa-bell"></i>'+
                    '                        </div>'+
                    '                        <div class="dropdown-item-desc">'+
                    '                            <b>'+val.data.title+'</b>'+
                    '                            <p>'+val.data.message+'</p>'+
                    '                        <div class="time">'+val.created_at+'</div>'+
                    '                        </div>'+
                    '                    </a>';

                    $('#notifications').append(notifications);
                });
            }
        });

        $('.notification-toggle').on('click', function () {
            $.ajax({
                url: "{{ url('api/v1/notifications') }}",
                type: "GET",
                success: function (response) {

                    if (response.unread != 0) {
                        $(".notification-toggle").addClass("beep");
                    }
                    
                    $('#notifications').empty();
                    $.each(response.data, function(key, val){
                        var notifications = '<a href="#" class="dropdown-item">'+
                        '                        <div class="dropdown-item-icon bg-info text-white">'+
                        '                        <i class="fas fa-bell"></i>'+
                        '                        </div>'+
                        '                        <div class="dropdown-item-desc">'+
                        '                            <b>'+val.data.title+'</b>'+
                        '                            <p>'+val.data.message+'</p>'+
                        '                        <div class="time">'+val.created_at+'</div>'+
                        '                        </div>'+
                        '                    </a>';

                        $('#notifications').append(notifications);
                    });
                }
            });
        });

        $('#notification-read-all').on('click', function () {
            $.ajax({
                url: "{{ url('api/v1/notifications') . '?read_at=true' }}",
                type: "GET",
                success: function (response) {
                    $(".notification-toggle").removeClass("beep");
                    $('#notifications').empty();
                    $.each(response.data, function(key, val){
                        var notifications = '<a href="#" class="dropdown-item">'+
                        '                        <div class="dropdown-item-icon bg-info text-white">'+
                        '                        <i class="fas fa-bell"></i>'+
                        '                        </div>'+
                        '                        <div class="dropdown-item-desc">'+
                        '                            <b>'+val.data.title+'</b>'+
                        '                            <p>'+val.data.message+'</p>'+
                        '                        <div class="time">'+val.created_at+'</div>'+
                        '                        </div>'+
                        '                    </a>';

                        $('#notifications').append(notifications);
                    });
                }
            });
        });
    });
</script>
