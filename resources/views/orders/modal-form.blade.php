{{-- Modal Form --}}
<div class="modal fade" id="modal-form" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="form" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="company_id" name="company_id" value="{{ Session::get('company_id')}}">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="keterangan-pemesanan-tab" data-toggle="tab" href="#keterangan-pemesanan" role="tab" aria-controls="keterangan-pemesanan" aria-selected="true">Keterangan Pemesanan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tujuan-tab" data-toggle="tab" href="#tujuan" role="tab" aria-controls="tujuan" aria-selected="false">Tujuan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="detail-barang-tab" data-toggle="tab" href="#detail-barang" role="tab" aria-controls="detail-barang" aria-selected="false">Detail Barang</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="customer-cost-tab" data-toggle="tab" href="#customer-cost" role="tab" aria-controls="customer-cost" aria-selected="false">Bill Cost</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="budget-driver-tab" data-toggle="tab" href="#budget-driver" role="tab" aria-controls="budget-driver" aria-selected="false">Actual Cost</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="actual-driver-tab" data-toggle="tab" href="#actual-driver" role="tab" aria-controls="actual-driver" aria-selected="false">Actual Driver</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="keterangan-pemesanan" role="tabpanel" aria-labelledby="keterangan-pemesanan-tab">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="customer_id">Customer*</label>
                                        <select class="form-control select2" style="width:100%" id="customer_id" name="customer_id" required>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="departure_date">Keberangkatan*</label>
                                        <input type="text" name="departure_date" class="form-control result" id="departure_date" required>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="arrival_date">Tanggal Sampai*</label>
                                        <input type="text" name="arrival_date" class="form-control result" id="arrival_date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="armada_id">Armada*</label>
                                        <select class="form-control select2" style="width:100%" id="armada_id" name="armada_id" required>
                                     
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="user_id">Pengemudi*</label>
                                        <select class="form-control select2" style="width:100%" id="user_id" name="user_id" required>
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="status_order">Status Order*</label>
                                        <select class="form-control" id="status_order" name="status_order" required>
                                            <option disabled value="" selected>-- Select Status --</option>
                                            <option value="0">Open</option>
                                            <option value="1">On Progress</option>
                                            <option value="6">Pending</option>
                                            <option value="3">Verified</option>
                                            <option value="2">Delivered</option>
                                            <option value="4">Closed</option>
                                            <option value="5">Cancel</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="order_id_digdeplus">ID Order Digdeplus</label>
                                        <input type="text" name="order_id_digdeplus" class="form-control result" id="order_id_digdeplus"><br>
                                        <button id="Getdigdeplus" type="button" class="btn btn-success" onclick="getData()">Get Order Digdeplus</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tujuan" role="tabpanel" aria-labelledby="tujuan">
                            <div class="row"> 
                                <div class="col-6">
                                        <label for="#"> <b>Dari :</b></label>
                                    <div class="form-group">
                                        <label for="#">Alamat*</label>
                                        <input type="text" name="r_address[0]" class="form-control" id="#" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        {{-- <label for="#">r_name*</label> --}}
                                        <input type="hidden" name="r_name[0]" class="form-control" id="#">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        {{-- <label for="#">r_latitude*</label> --}}
                                        <input type="hidden" name="r_latitude[0]" class="form-control" id="#" value="-6.205168" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        {{-- <label for="#">r_longitude*</label> --}}
                                        <input type="hidden" name="r_longitude[0]" class="form-control" id="#" value="106.817657" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="#">Nama PIC*</label>
                                        <input type="text" name="r_pic_name[0]" class="form-control" id="#" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="#">Kontak PIC*</label>
                                        <input type="text" name="r_pic_contact[0]" class="form-control" id="#" required>
                                        <input type="hidden" name="r_status[0]" class="form-control" id="#"  value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-6">
                                    <label for="#"> <b>Ke :</b></label>
                                    <div class="form-group">
                                        <label for="#">Alamat*</label>
                                        <input type="text" name="r_address[1]" class="form-control" id="#" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label for="#"></label>
                                    <div class="form-group">
                                        {{-- <label for="#">r_name*</label> --}}
                                        <input type="hidden" name="r_name[1]" class="form-control" id="#">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        {{-- <label for="#">r_latitude*</label> --}}
                                        <input type="hidden" name="r_latitude[1]" class="form-control" id="#" value="-6.227038" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        {{-- <label for="#">r_longitude*</label> --}}
                                        <input type="hidden" name="r_longitude[1]" class="form-control" id="#" value="106.797187" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="#">Nama PIC*</label>
                                        <input type="text" name="r_pic_name[1]" class="form-control" id="#" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="#">Kontak PIC*</label>
                                        <input type="text" name="r_pic_contact[1]" class="form-control" id="#" required>
                                        <input type="hidden" name="r_status[1]" class="form-control" id="#" value="1">
                                    </div>
                                </div>
                            </div>
                        </div>                 
                        <div class="tab-pane fade" id="detail-barang" role="tabpanel" aria-labelledby="detail-barang">
                            <div class="table-responsive">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="#"></label>
                                        <input type="button" class="btn btn-secondary form-control" id="addItemDetail" value="Add">
                                    </div>    
                                </div>
                                <!-- ini kolom detail barang -->
                                <table class="table table-striped" id="table-customer-item">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center">Action</th>
                                            <th ></th>
                                            <th style="text-align:center">Kode</th>
                                            <th style="text-align:center">Nama</th>
                                            <th style="text-align:center">Jenis</th>
                                            <th style="text-align:center">Jumlah</th>
                                            <th style="text-align:center">Satuan</th>
                                            <th style="text-align:center">Dimensi(M3)</th>
                                            <th style="text-align:center">Berat(Kg)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="itemdetails">
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="item_detail">Keterangan Barang</label>
                                        <textarea name="item_detail" id="item_detail" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="driver_note">Note to Driver</label>
                                        <textarea name="driver_note" id="driver_note" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image_item">Upload Foto Barang</label>
                                        <input type='file' name="image_item" class="form-control" id="image_item" accept="image/*">
                                        <input type="hidden" name="hidden_image_item" id="hidden_image_item">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <img id="image_item_preview" src="#" width="350" height="300" alt="Gambar">
                                </div>
                            </div>   -->
                        </div>
                        
                        <div class="tab-pane fade" id="customer-cost" role="tabpanel" aria-labelledby="customer-cost-tab">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="#">Bill Cost</label>
                                        <select class="form-control select2" style="width:100%" id="itemCustomerCost">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="#">Value</label>
                                        <input style="text-align:right;" type="text" min="0" class="form-control" id="itemCustomerCostValue">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="#"></label>
                                        <input type="button" class="btn btn-secondary form-control" id="addRowCustCost" value="Add">
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-customer-cost">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Bill Cost</th>
                                            <th>Value</th>    
                                        </tr>
                                    </thead>
                                    <tbody id="orderdetailcosts">
                                    </tbody>
                                </table>
                            </div>
                            <hr width="100%">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="netto" class="col-2 offset-4 col-form-label"><b>Subtotal</b></label>
                                                <div class="col-4 offset-2">
                                                    <input style="text-align:right;" type="text" min="0" name="bruto" class="form-control" id="bruto" value="0" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="disc" class="col-2 offset-4 col-form-label"><b>Discount ( % )</b></label>
                                                <div class="col-2">
                                                    <input style="text-align:right;" type="text" min="0" name="disc" onkeyup="updateCustomerCost();" class="form-control" id="disc" value="0">
                                                </div>
                                                <div class="col-4">
                                                    <input style="text-align:right;" type="text" min="0" name="disc_value" class="form-control" id="disc_value" value="0" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ppn" class="col-2 offset-4 col-form-label"><b>PPN ( % )</b></label>
                                                <div class="col-2">
                                                    <input style="text-align:right;" type="text" min="0" name="ppn" onkeyup="updateCustomerCost();" class="form-control" id="ppn" value="0">
                                                </div>
                                                <div class="col-4">
                                                    <input style="text-align:right;" type="text" min="0" name="ppn_value" class="form-control" id="ppn_value" value="0" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="pph" class="col-2 offset-4 col-form-label"><b>PPH ( % )</b></label>
                                                <div class="col-2">
                                                    <input style="text-align:right;" type="text" min="0" name="pph" onkeyup="updateCustomerCost();" class="form-control" id="pph" value="0">
                                                </div>
                                                <div class="col-4">
                                                    <input style="text-align:right;" type="text" min="0" name="pph_value" class="form-control" id="pph_value" value="0" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="netto" class="col-2 offset-4 col-form-label"><b>Total</b></label>
                                                <div class="col-4 offset-2">
                                                    <input style="text-align:right;" type="text" min="0" name="netto" class="form-control" id="netto" value="0" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="budget-driver" role="tabpanel" aria-labelledby="budget-driver-tab">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="#">Actual Cost</label>
                                        <select class="form-control select2" style="width:100%" id="itemDriverCost">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="#">Value</label>
                                        <input style="text-align:right;" type="text" min="0" class="form-control" id="itemDriverCostValue">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="#"></label>
                                        <input type="button" class="btn btn-secondary form-control" id="addRowDriverCost" value="Add">
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-driver-cost">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Actual Cost</th>
                                            <th>Kasbon Admin</th>
                                            <th>Actual Cost Driver</th>      
                                        </tr>
                                    </thead>
                                    <tbody id="budgetdetails">
                                    
                                    </tbody>
                                </table>
                                <hr width="100%">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label for="total_budgetvalue" class="col-2 offset-4 col-form-label"><b>Total Kasbon Admin</b></label>
                                                    <div class="col-4 offset-2">
                                                        <input style="text-align:right;" type="text" min="0" name="total_budgetvalue" class="form-control" id="total_budgetvalue" value="0" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="total_budgetrealization" class="col-2 offset-4 col-form-label"><b>Total Actual Cost Driver</b></label>
                                                    <div class="col-4 offset-2">
                                                        <input style="text-align:right;" type="text" min="0" name="total_budgetrealization" class="form-control" id="total_budgetrealization" value="0" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="difference" class="col-2 offset-4 col-form-label"><b>Difference</b></label>
                                                    <div class="col-4 offset-2">
                                                        <input style="text-align:right;" type="text" min="0" name="difference" class="form-control" id="difference" value="0" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="actual-driver" role="tabpanel" aria-labelledby="actual-driver-tab">
                            <div class="row">
                                <div class="col-4 offset-2">
                                    <div class="form-group">
                                        <label for="#">Aktual Keberangkatan Pengemudi</label>
                                        <input type="text" class="form-control" id="driver_departure_date" readonly>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="#">Aktual Kedatangan Pengemudi</label>
                                        <input type="text" class="form-control" id="driver_arrival_date" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal Form --}}