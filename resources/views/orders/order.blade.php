@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                @endif
            </ul>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown active">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Order</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item active">Order</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                @if(Session::get('role_id') == 2)
                                <a onclick="addData()" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if(Session::get('role_id') == 1)
                            <label for="#"><b>Filter :</b></label>
                                <div class="row">
                                    <div class="col-3">
                                        <select class="form-control select2" style="width:100%" name="company" id="company">
                                            
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-striped" id="order-datatable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>No. Order</th>
                                            <th>Tgl. Keberangkatan</th>  
                                            <th>Tujuan</th>  
                                            <th>Customer</th>  
                                            <th>Status</th>  
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('orders.modal-form')
@include('orders.rating-modal-form')

@endsection

@section('javascript')

<script type="text/javascript">
$(function() {
    var role_id = "{{Session::get('role_id')}}";
    
    if (role_id == 1) {
        var table = $('#order-datatable').DataTable({
            columnDefs: [
                {
                    targets: [ 0 ],
                    visible: false,
                    searchable: false
                },
            ],
            order: [[ 0, "desc" ]]
        });

        $.ajax({
            url: "{{ url('api/v1/companies') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#company').empty();
                $('#company').append('<option disabled selected>-- Select Company --</option>');
                $.each(response.data, function(key, val){
                    $('#company').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $("#company").change(function () {
            var company_id = $(this).val();
            var url = "{{ url('api/v1/orders')}}" +  "?company_id=" + company_id;

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    table.clear().draw();
                    $.each(response.data, function (key, val) {
                        table.row.add([
                            val.id, val.no_order, val.departure_date,
                            val.destination, val.customer.name,
                            val.status_order, val.action_order
                        ]).draw();
                    });
                }
            });
        });

    } else {
        var url = "{{ url('api/v1/orders')}}" +  "?company_id=" + "{{ Session::get('company_id')}}";
        var table = $('#order-datatable').DataTable({
            processing: true,
            ajax: url,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'no_order', name: 'no_order'},
                {data: 'departure_date', name: 'departure_date'},
                {data: 'destination', name: 'destination'},
                {data: 'customer.name', name: 'customer.name'},
                {data: 'status_order', name: 'status_order'},
                {data: 'action_order', name: 'action_order', orderable: false, searchable: false }
            ],
            columnDefs: [
                {
                    targets: [ 0 ],
                    visible: false,
                    searchable: false
                },
            ],
            order: [[ 0, "desc" ]]
        });
    }

});

    function sj(id) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $.ajax({
            url : "{{ url('api/v1/orders') }}" + '/' + id + '/sj' + '?company_id=' + company_id,
            type : "GET",
            data : {'_token' : csrf_token},
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success : function(response) {
                const linkSource = `data:application/pdf;base64,${response.sj}`;
                download(linkSource, "surat_jalan.pdf", "application/pdf");
                swal.close();
                $('#order-datatable').DataTable().ajax.reload();
            },
            error : function(response){
                swal({
                    title: 'Opps...',
                    text: response.message,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
    }

    function tandaterima(id) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $.ajax({
            url : "{{ url('api/v1/orders') }}" + '/' + id + '/tandaterima' + '?company_id=' + company_id,
            type : "GET",
            data : {'_token' : csrf_token},
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success : function(response) {
                const linkSource = `data:application/pdf;base64,${response.tandaterima}`;
                download(linkSource, "tanda_terima.pdf", "application/pdf");
                swal.close();
                $('#order-datatable').DataTable().ajax.reload();
            },
            error : function(response){
                swal({
                    title: 'Opps...',
                    text: response.message,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
    }

    function inv(id) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $.ajax({
            url : "{{ url('api/v1/orders') }}" + '/' + id + '/inv' + '?company_id=' + company_id,
            type : "GET",
            data : {'_token' : csrf_token},
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success : function(response) {
                const linkSource = `data:application/pdf;base64,${response.inv}`;
                download(linkSource, "Invoice.pdf", "application/pdf");
                swal.close();
                $('#order-datatable').DataTable().ajax.reload();
            },
            error : function(response){
                swal({
                    title: 'Opps...',
                    text: response.message,
                    type: 'error',
                    timer: '2000'
                })
            }
        });
    }

    $('#departure_date').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        }
    });

    $('#arrival_date').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        }
    });

    function refresh() {
        $('#order-datatable').DataTable().ajax.reload();
    }

    $('#modal-form').on('hidden.bs.modal', function (e) {
        $(".form-control").prop("disabled", false);
        $('button[type="submit"]').show();
    })

    function showData(id) {
        save_method = "show";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('#actual-driver-tab').show();
        $("#orderdetailcosts").empty();
        $("#budgetdetails").empty();
        $("#itemdetails").empty();
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/orders') }}" + '/' + id,
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {
                var customer_id =  response.data.customer_id;
                var user_id =  response.data.user_id;
                var armada_id = response.data.armada_id;
   
                $.ajax({
                    url: "{{ url('api/v1/customers') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#customer_id').empty();
                        $('#customer_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == customer_id) {
                                $('#customer_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#customer_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });                 
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/users/driver') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#user_id').empty();
                        $('#user_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == user_id) {
                                $('#user_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#user_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/armadas') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#armada_id').empty();
                        $('#armada_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == armada_id) {
                                $('#armada_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/costs') }}",
                    type: "GET",
                    data: {'company_id' : company_id, 'status' : 0},
                    success: function (response) {
                        $('#itemCustomerCost').empty();
                        $('#itemCustomerCost').append('<option value="" disabled selected>-</option>');
                        $.each(response.data, function(key, val){
                            $('#itemCustomerCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/costs') }}",
                    type: "GET",
                    data: {'company_id' : company_id, 'status' : 1},
                    success: function (response) {
                        $('#itemDriverCost').empty();
                        $('#itemDriverCost').append('<option value="" disabled selected>-</option>');
                        $.each(response.data, function(key, val){
                            $('#itemDriverCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                        });
                    }
                });       

                $('#modal-form').modal('show');
                $('.modal-title').text('Show Data');

                $('#id').val(response.data.id);
                $('#customer_id').val(response.data.customer_id);
                $('#departure_date').val(response.data.departure_date);
                $('#arrival_date').val(response.data.arrival_date);
                $('#armada_id').val(response.data.armada_id);
                $('#order_id_digdeplus').val(response.data.order_id_digdeplus);

                $("input[name='r_name[0]']").val(response.data.routes[0].name);
                $("input[name='r_address[0]']").val(response.data.routes[0].address);
                $("input[name='r_latitude[0]']").val(response.data.routes[0].latitude);
                $("input[name='r_longitude[0]']").val(response.data.routes[0].longitude);
                $("input[name='r_pic_name[0]']").val(response.data.routes[0].pic_name);
                $("input[name='r_pic_contact[0]']").val(response.data.routes[0].pic_contact);
                $("input[name='r_status[0]']").val(response.data.routes[0].status);

                $("input[name='r_name[1]']").val(response.data.routes[1].name);
                $("input[name='r_address[1]']").val(response.data.routes[1].address);
                $("input[name='r_latitude[1]']").val(response.data.routes[1].latitude);
                $("input[name='r_longitude[1]']").val(response.data.routes[1].longitude);
                $("input[name='r_pic_name[1]']").val(response.data.routes[1].pic_name);
                $("input[name='r_pic_contact[1]']").val(response.data.routes[1].pic_contact);
                $("input[name='r_status[1]']").val(response.data.routes[1].status);

                $('#item_type').val(response.data.item_type);
                $('#status_order').val(response.data.status_order).attr('selected','selected');
                $('#item_weight').val(response.data.item_weight);
                $('#item_qty').val(response.data.item_qty);
                $('#item_detail').val(response.data.item_detail);
                $('#driver_note').val(response.data.driver_note);
                $('#hidden_image_item').val(response.data.image_item);
                $('#image_item_preview').attr('src', response.data.image_item);

                $.each(response.data.orderdetailcosts, function(key, val){
                    orderdetailcosts = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowCustCost" disabled>Delete</button></td>' +
                    '<td><input type="hidden" name="cust_cost_id[]" class="itemCustomerCost'+val.cost_id+'" value="'+val.cost_id+'">' + val.cost.name + '</td>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="cust_cost_value[]" onkeyup="updateCustomerCost();" class="form-control custCostValue" value="'+val.value+'" required>' +
                    '</td></tr>' ;
                    $('#orderdetailcosts').append(orderdetailcosts);
                });
                $('#bruto').val(response.data.bruto);
                $('#disc').val(response.data.disc);
                $('#disc_value').val(response.data.disc_value);
                $('#ppn').val(response.data.ppn);
                $('#ppn_value').val(response.data.ppn_value);
                $('#pph').val(response.data.pph);
                $('#pph_value').val(response.data.pph_value);
                $('#netto').val(response.data.netto);

                $('#driver_departure_date').val(response.data.driver_departure_date);
                $('#driver_arrival_date').val(response.data.driver_arrival_date);

                $.each(response.data.budgetdetails, function(key, val){
                    budgetdetails = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowDriverCost" disabled>Delete</button></td>' +
                    '<td><input type="hidden" name="driver_cost_id[]" class="itemDriverCost'+val.cost_id+'" value="'+val.cost_id+'">' + val.cost.name + '</td>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_value[]" onkeyup="updateDriverCost();" class="form-control driverCostValue" value="'+val.value+'" required>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_realization[]" class="form-control driverCostRealization" value="'+val.realization+'" readonly></td>'+
                    '</td></tr>' ;
                    $('#budgetdetails').append(budgetdetails);
                });

                $('#total_budgetvalue').val(response.data.total_budgetvalue);
                updateDriverCost();
                $('#status_order').val(response.data.status_order).attr('selected','selected');

                $(".form-control").prop("disabled", true);
                $('button[type="submit"]').hide();


                $.each(response.data.itemdetails, function(key, val){
                    itemdetails = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowItemDetail" disabled>Delete</button></td>' +
                    '<td></td>' +
                    '<td><input type="hidden" name="item_detail_id[]" class="itemOrderDetails'+val.id+'</td>' +
                    '<td><input type="hidden" style="width:100px!important;" name="item_code[]" class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.item_code + '</td>' +
                    '<td><input type="hidden" style="width:100px!important;" name="item_name[]" class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.item_name + '</td>' +
                    '<td><input type="hidden" style="width:100px!important;" name="item_type_sempag[]" class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.item_type + '</td>' +
                    '<td style="text-align:right;"><input type="hidden" style="width:100px!important;" name="qty[]" class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.qty + '</td>' +
                    '<td><input type="hidden" style="width:100px!important;" name="unit[]" class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.unit + '</td>' +
                    '<td style="text-align:center;"><input type="hidden" style="width:100px!important;" name="dimensi[]" class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.dimensi + '</td>' +
                    '<td style="text-align:right;"><input type="hidden" style="width:100px!important;" name="weight[]"  class="itemOrderDetails'+val.id+'" value="'+val.id+'">' + val.weight + '</td>' +
                    '</td></tr>' ;
                    $('#itemdetails').append(itemdetails);
                });

                $(".form-control").prop("disabled", true);
                $('button[type="submit"]').hide();
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function getData(){
        save_method = "add";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";
        var id_dig =  $('#order_id_digdeplus').val();

        $('input[name=_method]').val('POST');
        $('#actual-driver-tab').show();
        $("#orderdetailcosts").empty();
        $("#budgetdetails").empty();
        $("#itemdetails").empty();
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/get-digdeplus') }}",
            type: "GET",
            data: {'order_id_digdeplus' : id_dig,'id_digkontrol':company_id},
            success: function (response) {
                 swal({
                    title: 'Success!',
                    text: 'Data Order Digdeplus Ditemukan',
                    type: 'success',
                    timer: '5050',
                    showConfirmButton: true
                }) 

                var customer_id =  response.data.customer_id;
                var user_id =  response.data.user_id;
                var armada_id = response.data.armada_id;
                $('#order_id_digdeplus').val(response.data.id);

   
                $.ajax({
                    url: "{{ url('api/v1/customers') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#customer_id').empty();
                        $('#customer_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.is_digdeplus == 1 ) {
                                $('#customer_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#customer_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });                 
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/users/driver') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#user_id').empty();
                        $('#user_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == user_id) {
                                $('#user_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#user_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/armadas') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#armada_id').empty();
                        $('#armada_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == armada_id) {
                                $('#armada_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/costs') }}",
                    type: "GET",
                    data: {'company_id' : company_id, 'status' : 0},
                    success: function (response) {
                        $('#itemCustomerCost').empty();
                        $('#itemCustomerCost').append('<option value="" disabled selected>-</option>');
                        $.each(response.data, function(key, val){
                            $('#itemCustomerCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/costs') }}",
                    type: "GET",
                    data: {'company_id' : company_id, 'status' : 1},
                    success: function (response) {
                        $('#itemDriverCost').empty();
                        $('#itemDriverCost').append('<option value="" disabled selected>-</option>');
                        $.each(response.data, function(key, val){
                            $('#itemDriverCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                        });
                    }
                });       

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                // $('#customer_id').val(response.data.customer_id);
                // $('#customer_id').val(response.data.customer_id).attr('selected','selected');

                $('#departure_date').val(response.data.departure_date);
                $('#arrival_date').val(response.data.arrival_date);
                $('#armada_id').val(response.data.armada_id);

                $("input[name='r_name[0]']").val(response.data.routes[0].name);
                $("input[name='r_address[0]']").val(response.data.routes[0].address);
                $("input[name='r_latitude[0]']").val(response.data.routes[0].latitude);
                $("input[name='r_longitude[0]']").val(response.data.routes[0].longitude);
                $("input[name='r_pic_name[0]']").val(response.data.routes[0].pic_name);
                $("input[name='r_pic_contact[0]']").val(response.data.routes[0].pic_contact);
                $("input[name='r_status[0]']").val(response.data.routes[0].status);

                $("input[name='r_name[1]']").val(response.data.routes[1].name);
                $("input[name='r_address[1]']").val(response.data.routes[1].address);
                $("input[name='r_latitude[1]']").val(response.data.routes[1].latitude);
                $("input[name='r_longitude[1]']").val(response.data.routes[1].longitude);
                $("input[name='r_pic_name[1]']").val(response.data.routes[1].pic_name);
                $("input[name='r_pic_contact[1]']").val(response.data.routes[1].pic_contact);
                $("input[name='r_status[1]']").val(response.data.routes[1].status);

                $('#item_type').val(response.data.item_type).attr('selected','selected');
                $('#status_order').val(response.data.status_order).attr('selected','selected');
                $('#item_weight').val(response.data.item_weight);
                $('#item_qty').val(response.data.item_qty);
                $('#item_detail').val(response.data.item_detail);
                $('#driver_note').val(response.data.driver_note);
                $('#hidden_image_item').val(response.data.image_item);
                $('#image_item_preview').attr('src', response.data.image_item);
                // $('#itemdetails').val(response.data.itemdetails.item_type).attr('selected','selected');

                $.each(response.data.orderdetailcosts, function(key, val){
                    orderdetailcosts = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowCustCost">Delete</button></td>' +
                    '<td><input type="hidden" name="cust_cost_id[]" class="itemCustomerCost'+val.cost_id+'" value="'+val.cost_id+'">' + val.cost.name + '</td>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="cust_cost_value[]" onkeyup="updateCustomerCost();" class="form-control custCostValue" value="'+val.value+'" required>' +
                    '</td></tr>' ;
                    $('#orderdetailcosts').append(orderdetailcosts);
                });
                $('#bruto').val(response.data.bruto);
                $('#disc').val(response.data.disc);
                $('#disc_value').val(response.data.disc_value);
                $('#ppn').val(response.data.ppn);
                $('#ppn_value').val(response.data.ppn_value);
                $('#pph').val(response.data.pph);
                $('#pph_value').val(response.data.pph_value);
                $('#netto').val(response.data.netto);

                $('#driver_departure_date').val(response.data.driver_departure_date);
                $('#driver_arrival_date').val(response.data.driver_arrival_date);

                $.each(response.data.budgetdetails, function(key, val){
                    budgetdetails = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowDriverCost">Delete</button></td>' +
                    '<td><input type="hidden" name="driver_cost_id[]" class="itemDriverCost'+val.cost_id+'" value="'+val.cost_id+'">' + val.cost.name + '</td>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_value[]" onkeyup="updateDriverCost();" class="form-control driverCostValue" value="'+val.value+'" required>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_realization[]" class="form-control driverCostRealization" value="'+val.realization+'" readonly></td>'+
                    '</td></tr>' ;
                    $('#budgetdetails').append(budgetdetails);
                });

                $('#total_budgetvalue').val(response.data.total_budgetvalue);
                updateDriverCost();
                $('#status_order').val(response.data.status_order).attr('selected','selected');

                $.each(response.data.itemdetails, function(key, val){
                    // console.log(val.item_type);
                    itemdetails = '<tr>' +
                    '<td><button type="button" class="btn btn-danger btnDelRowItemDetail">Delete</button></td>' +
                    '<td><input type="hidden" name="item_detail_id[]" class="form-control"'+val.id+'</td>' +
                    '<td><input type="text" style="width:100px!important;" name="item_code[]" class="form-control" value="'+val.item_code+'"></td>' +
                    '<td><input type="text" style="width:100px!important;" name="item_name[]" class="form-control" value="'+val.item_name+'"></td>' +
                    '<td><select style="width:150px!important;" class="form-control" name="item_type_sempag[]" value="'+val.id+'"><option selected value="'+val.item_type+'">'+val.item_type+'</option><option name="item_type_sempag[]" value="Padat">Padat</option><option name="item_type_sempag[]" value="Cairan">Cairan</option><option name="item_type_sempag[]" value="Minyak">Minyak</option><option name="item_type_sempag[]" value="Gas">Gas</option></select></td>' +
                    '<td><input type="text" style="width:80px!important;" name="qty[]" class="form-control" value="'+val.qty+'"></td>' +
                    '<td><input type="text" style="width:100px!important;" name="unit[]" class="form-control" value="'+val.unit+'"></td>' +
                    '<td><input type="text" style="width:80px!important;" name="dimensi[]" class="form-control" value="'+val.dimensi+'"></td>' +
                    '<td><input type="text" style="width:80px!important;" name="weight[]" class="form-control" value="'+val.weight+'"></td>' +
                    '</td></tr>' ;
                    $('#itemdetails').append(itemdetails);
                });

            },
            error: function () {
               swal({
                  title: 'Data Order Tidak Ditemukan',
                  html: 'Silahkan Periksa ID Order Anda',
                  type: 'warning'
                })
            }
        });
    }

    function addData() {
        save_method = "add";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('input[name=_method]').val('POST');
        $('#actual-driver-tab').hide();
        $('#armada_id').empty();
        $('#armada_id').empty();
        $('#user_id').empty();
        $("#orderdetailcosts").empty();
        $("#budgetdetails").empty();
        $("#itemdetails").empty();
        $('#image_item_preview').attr('src', '#');
        $('#modal-form form')[0].reset();
        
        $.ajax({
            url: "{{ url('api/v1/customers') }}",
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {
                $('#customer_id').empty();
                $('#customer_id').append('<option value="" disabled selected>-</option>');
                $.each(response.data, function(key, val){
                    $('#customer_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $.ajax({
            url: "{{ url('api/v1/costs') }}",
            type: "GET",
            data: {'company_id' : company_id, 'status' : 0},
            success: function (response) {
                $('#itemCustomerCost').empty();
                $('#itemCustomerCost').append('<option value="" disabled selected>-</option>');
                $.each(response.data, function(key, val){
                    $('#itemCustomerCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $.ajax({
            url: "{{ url('api/v1/costs') }}",
            type: "GET",
            data: {'company_id' : company_id, 'status' : 1},
            success: function (response) {
                $('#itemDriverCost').empty();
                $('#itemDriverCost').append('<option value="" disabled selected>-</option>');
                $.each(response.data, function(key, val){
                    $('#itemDriverCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });  

        $('#modal-form form')[0].reset();
        // $('#item_type').prop('selectedIndex',0);
        $('#status_order').prop('selectedIndex',0);

        $('#modal-form').modal('show');
        $('.modal-title').text('Input Data');
    }

// disini file
    function showFile(fileInput, img, showName) {
      if (fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $(img).attr('src', e.target.result);
        }
        reader.readAsDataURL(fileInput.files[0]);
        }
        $('#image_item_preview').text(fileInput.files[0].name);
      }
    
      $('#image_item').on('change', function() {
          showFile(this, '#image_item_preview');
        });

    function editData(id) {
        save_method = "edit";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('input[name=_method]').val('PATCH');
        $('#actual-driver-tab').show();
        $("#orderdetailcosts").empty();
        $("#budgetdetails").empty();
        $("#itemdetails").empty();
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/orders') }}" + '/' + id,
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {
                var customer_id =  response.data.customer_id;
                var user_id =  response.data.user_id;
                var armada_id = response.data.armada_id;
   
                $.ajax({
                    url: "{{ url('api/v1/customers') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#customer_id').empty();
                        $('#customer_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == customer_id) {
                                $('#customer_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#customer_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });                 
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/users/driver') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#user_id').empty();
                        $('#user_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == user_id) {
                                $('#user_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#user_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/armadas') }}",
                    type: "GET",
                    data: {'company_id' : company_id},
                    success: function (response) {
                        $('#armada_id').empty();
                        $('#armada_id').append('<option value="" disabled>-</option>');
                        $.each(response.data, function(key, val){
                            if (val.id == armada_id) {
                                $('#armada_id').append('<option value="'+ val.id +'" selected>' + val.name + '</option>');
                            } else {
                                $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                            }
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/costs') }}",
                    type: "GET",
                    data: {'company_id' : company_id, 'status' : 0},
                    success: function (response) {
                        $('#itemCustomerCost').empty();
                        $('#itemCustomerCost').append('<option value="" disabled selected>-</option>');
                        $.each(response.data, function(key, val){
                            $('#itemCustomerCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                        });
                    }
                });

                $.ajax({
                    url: "{{ url('api/v1/costs') }}",
                    type: "GET",
                    data: {'company_id' : company_id, 'status' : 1},
                    success: function (response) {
                        $('#itemDriverCost').empty();
                        $('#itemDriverCost').append('<option value="" disabled selected>-</option>');
                        $.each(response.data, function(key, val){
                            $('#itemDriverCost').append('<option value="'+ val.id +'">' + val.name + '</option>');
                        });
                    }
                });       

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                $('#customer_id').val(response.data.customer_id);
                $('#departure_date').val(response.data.departure_date);
                $('#arrival_date').val(response.data.arrival_date);
                $('#armada_id').val(response.data.armada_id);order_id_digdeplus
                $('#order_id_digdeplus').val(response.data.order_id_digdeplus);

                $("input[name='r_name[0]']").val(response.data.routes[0].name);
                $("input[name='r_address[0]']").val(response.data.routes[0].address);
                $("input[name='r_latitude[0]']").val(response.data.routes[0].latitude);
                $("input[name='r_longitude[0]']").val(response.data.routes[0].longitude);
                $("input[name='r_pic_name[0]']").val(response.data.routes[0].pic_name);
                $("input[name='r_pic_contact[0]']").val(response.data.routes[0].pic_contact);
                $("input[name='r_status[0]']").val(response.data.routes[0].status);

                $("input[name='r_name[1]']").val(response.data.routes[1].name);
                $("input[name='r_address[1]']").val(response.data.routes[1].address);
                $("input[name='r_latitude[1]']").val(response.data.routes[1].latitude);
                $("input[name='r_longitude[1]']").val(response.data.routes[1].longitude);
                $("input[name='r_pic_name[1]']").val(response.data.routes[1].pic_name);
                $("input[name='r_pic_contact[1]']").val(response.data.routes[1].pic_contact);
                $("input[name='r_status[1]']").val(response.data.routes[1].status);

                $('#item_type').val(response.data.item_type).attr('selected','selected');
                $('#status_order').val(response.data.status_order).attr('selected','selected');
                $('#item_weight').val(response.data.item_weight);
                $('#item_qty').val(response.data.item_qty);
                $('#item_detail').val(response.data.item_detail);
                $('#driver_note').val(response.data.driver_note);
                $('#hidden_image_item').val(response.data.image_item);
                $('#image_item_preview').attr('src', response.data.image_item);
                // $('#itemdetails').val(response.data.itemdetails.item_type).attr('selected','selected');

                $.each(response.data.orderdetailcosts, function(key, val){
                    orderdetailcosts = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowCustCost">Delete</button></td>' +
                    '<td><input type="hidden" name="cust_cost_id[]" class="itemCustomerCost'+val.cost_id+'" value="'+val.cost_id+'">' + val.cost.name + '</td>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="cust_cost_value[]" onkeyup="updateCustomerCost();" class="form-control custCostValue" value="'+val.value+'" required>' +
                    '</td></tr>' ;
                    $('#orderdetailcosts').append(orderdetailcosts);
                });
                $('#bruto').val(response.data.bruto);
                $('#disc').val(response.data.disc);
                $('#disc_value').val(response.data.disc_value);
                $('#ppn').val(response.data.ppn);
                $('#ppn_value').val(response.data.ppn_value);
                $('#pph').val(response.data.pph);
                $('#pph_value').val(response.data.pph_value);
                $('#netto').val(response.data.netto);

                $('#driver_departure_date').val(response.data.driver_departure_date);
                $('#driver_arrival_date').val(response.data.driver_arrival_date);

                $.each(response.data.budgetdetails, function(key, val){
                    budgetdetails = '<tr>' + 
                    '<td><button type="button" class="btn btn-danger btnDelRowDriverCost">Delete</button></td>' +
                    '<td><input type="hidden" name="driver_cost_id[]" class="itemDriverCost'+val.cost_id+'" value="'+val.cost_id+'">' + val.cost.name + '</td>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_value[]" onkeyup="updateDriverCost();" class="form-control driverCostValue" value="'+val.value+'" required>' +
                    '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_realization[]" class="form-control driverCostRealization" value="'+val.realization+'" readonly></td>'+
                    '</td></tr>' ;
                    $('#budgetdetails').append(budgetdetails);
                });

                $('#total_budgetvalue').val(response.data.total_budgetvalue);
                updateDriverCost();
                $('#status_order').val(response.data.status_order).attr('selected','selected');

                $.each(response.data.itemdetails, function(key, val){
                    // console.log(val.item_type);
                    itemdetails = '<tr>' +
                    '<td><button type="button" class="btn btn-danger btnDelRowItemDetail">Delete</button></td>' +
                    '<td><input type="hidden" name="item_detail_id[]" class="form-control"'+val.id+'</td>' +
                    '<td><input type="text" style="width:100px!important;" name="item_code[]" class="form-control" value="'+val.item_code+'"></td>' +
                    '<td><input type="text" style="width:100px!important;" name="item_name[]" class="form-control" value="'+val.item_name+'"></td>' +
                    '<td><select style="width:150px!important;" class="form-control" name="item_type_sempag[]" value="'+val.id+'"><option selected value="'+val.item_type+'">'+val.item_type+'</option><option name="item_type_sempag[]" value="Padat">Padat</option><option name="item_type_sempag[]" value="Cairan">Cairan</option><option name="item_type_sempag[]" value="Minyak">Minyak</option><option name="item_type_sempag[]" value="Gas">Gas</option></select></td>' +
                    '<td><input type="text" style="width:80px!important;" name="qty[]" class="form-control" value="'+val.qty+'"></td>' +
                    '<td><input type="text" style="width:100px!important;" name="unit[]" class="form-control" value="'+val.unit+'"></td>' +
                    '<td><input type="text" style="width:80px!important;" name="dimensi[]" class="form-control" value="'+val.dimensi+'"></td>' +
                    '<td><input type="text" style="width:80px!important;" name="weight[]" class="form-control" value="'+val.weight+'"></td>' +
                    '</td></tr>' ;
                    $('#itemdetails').append(itemdetails);
                });

            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    $(function () {
        $(".result").change(function () {

            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            var start_date = $('#departure_date').val();
            var end_date = $('#arrival_date').val();

            $.ajax({
                url: "{{url('/api/v1/schedules/armada-available')}}",
                type: "GET",
                data : {
                    '_token' : csrf_token,
                    'start_date' : start_date,
                    'end_date' : end_date
                },
                success: function (response) {

            // console.log(response.data);

                function getSchedule(params, status) {
                    if (params.length > 0 && status !== 0) {
                        const max = params.reduce((prev, current) =>
                        prev.id > current.id ? prev : current
                        );
                        let StringStatus = "";

                        switch (max.status_order) {
                        case 0:
                            StringStatus = "Open";
                            break;

                        case 1:
                            StringStatus = "Proses";
                            break;

                        case 2:
                            StringStatus = "Delivered";
                            break;

                        case 3:
                            StringStatus = "Verified";
                            break;

                        case 4:
                            StringStatus = "Close";
                            break;

                        case 5:
                            StringStatus = "Cancel";
                            break;

                        case 6:
                            StringStatus = "Pending";
                            break;

                        case 7:
                            StringStatus = "Loaded";
                            break;

                        default:
                            StringStatus = null
                            break;
                        }
                        return max.number + " - " + StringStatus;
                    } else {
                        return "Available";
                    }
                }
                    $('#armada_id').empty();
                    $('#armada_id').append('<option value="" disabled selected>-</option>');
                    $.each(response.data, function(key, val){
                        if (getSchedule(val.schedulearmadas, val.status) === "Available") {
                            $('#armada_id').append('<option value="'+ val.id + '">'+ val.name + ' - '+ getSchedule(val.schedulearmadas, val.status) + '</option>')
                        } else {
                            $('#armada_id').append('<option value="'+ val.id + '" disabled>'+ val.name +' - Unavailable - ' + getSchedule(val.schedulearmadas, val.status) +'</option>');
                        }
                        // $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                    });
                },
                error: function (response) {
                    $('#armada_id').empty();
                }
            });

            $.ajax({
                url: "{{url('/api/v1/schedules/driver-available')}}",
                type: "GET",
                data : {
                    '_token' : csrf_token,
                    'start_date' : start_date,
                    'end_date' : end_date
                },
                success: function (response) {
                    function getSchedule(params, status) {
                        if (params.length > 0 && status !== 0) {
                            const max = params.reduce((prev, current) =>
                            prev.id > current.id ? prev : current
                            );
                            let StringStatus = "";

                            switch (max.status_order) {
                            case 0:
                                StringStatus = "Open";
                                break;

                            case 1:
                                StringStatus = "Proses";
                                break;

                            case 2:
                                StringStatus = "Delivered";
                                break;

                            case 3:
                                StringStatus = "Verified";
                                break;

                            case 4:
                                StringStatus = "Close";
                                break;

                            case 5:
                                StringStatus = "Cancel";
                                break;

                            case 6:
                                StringStatus = "Pending";
                                break;

                            case 7:
                                StringStatus = "Loaded";
                                break;

                            default:
                                StringStatus = null
                                break;
                            }
                            return max.number + " - " + StringStatus;
                        } else {
                            return "Available";
                        }
                    }
                    $('#user_id').empty();
                    $('#user_id').append('<option value="" disabled selected>-</option>');
                    $.each(response.data, function(key, val){
                        if (getSchedule(val.scheduleusers, val.status) === "Available") {
                            $('#user_id').append('<option value="'+ val.id + '">'+ val.name +' - '+ getSchedule(val.scheduleusers, val.status) +'</option>')
                        } else {
                            $('#user_id').append('<option value="'+ val.id + '" disabled>'+ val.name +' - Unavailable - '+ getSchedule(val.scheduleusers, val.status)+'</option>');
                        }
                    });
                },
                error: function (response) {
                    $('#user_id').empty();
                }
            });
        });
    });

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $.ajax({
                url : "{{ url('api/v1/orders') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success : function(response) {
                    $('#order-datatable').DataTable().ajax.reload();
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function (response) {
                    swal({
                        title: 'Oops...',
                        text: response.responseText,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }

    function ratingDriver(id) {
        $('#modal-form-rating').modal('show');
        $('#modal-form-rating form')[0].reset();
        $('#modal-form-rating .modal-title').text('Rating Driver');
        $.ajax({
            url: "{{ url('api/v1/orders') }}" + '/' + id + '/rating',
            type: "GET",
            success: function (response) {
                $('#modal-form-rating form input[name=id]').val(id);
                $("#modal-form-rating form select[name=disciplinerating]").val(response.data.disciplinerating).attr('selected','selected');
                $("#modal-form-rating form select[name=servicerating]").val(response.data.servicerating).attr('selected','selected');
                $("#modal-form-rating form select[name=safetyrating]").val(response.data.safetyrating).attr('selected','selected');
                $('#modal-form-rating form textarea[name=commentrating]').val(response.data.commentrating);
            }
        });
    }

    $('#modal-form-rating form').on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            var id = $('#modal-form-rating form input[name=id]').val();
            $.ajax({
                url: "{{ url('api/v1/orders') }}" + '/' + id + '/rating',
                type: "POST",
                data: $('#modal-form-rating form').serialize(),
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success: function (response) {
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '1500'
                    })
                    $('#modal-form-rating').modal('hide');
                },
                error: function (response) {
                    swal({
                        title: 'Opps...',
                        text: response.responseText,
                        type: 'error',
                        timer: '2000'
                    })
                }
            });
            return false;
        }
    });


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/v1/orders') }}";
                else url = "{{ url('api/v1/orders') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : new FormData($("#form")[0]),
                    contentType : false, 
                    processData : false,
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        $('#order-datatable').DataTable().ajax.reload();
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                      $('#modal-form').modal('hide');
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

    $(function () {
        //Add Row
        $("#addRowCustCost").on("click", function () {
            var html = "";
            var itemCustomerCost = $('#itemCustomerCost').val();
            var itemCustomerCostValue = $('#itemCustomerCostValue').val();
            
            if(itemCustomerCost == null) {
                alert("Can't Add Row. Please choose name cost");
                return false;
            }  else if ($( '.itemCustomerCost'+itemCustomerCost ).length  || isNaN(itemCustomerCostValue)) {
                alert("Can't Add Row. Duplicate Name Cost");
                return false;
            } else if (itemCustomerCostValue < 1){
                alert("Can't Add Row. Cost value can't be null");
                return false;
            }



            html += '<tr>';
            html +=  '<td><button type="button" class="btn btn-danger btnDelRowCustCost">Delete</button></td>';
            html += '<td><input type="hidden" name="cust_cost_id[]" class="itemCustomerCost'+itemCustomerCost+'" value="'+itemCustomerCost+'">';
            html += $('#itemCustomerCost option:selected').text()+'</td>';
            html += '<td><input style="text-align:right;" type="text" min="0" name="cust_cost_value[]" onkeyup="updateCustomerCost();" class="form-control custCostValue" value="'+itemCustomerCostValue+'" required></td>';
            html += '</tr>';

            $('#orderdetailcosts').append(html);

            //clear input
            $('#itemCustomerCostValue').val('');
            updateCustomerCost();
        });


        $("#table-customer-cost").on("click", ".btnDelRowCustCost", function (event) {
            $(this).closest("tr").remove();
            updateCustomerCost();
        });

    });

    

    $(function () {
        $("#addItemDetail").on("click", function () {

            var html = "";
            var itemCode = $('#itemCode').val();
            var itemName = $('#itemName').val();
            var itemType = $('#itemType').val();
            var qty = $('#qty').val();
            var unit = $('#unit').val();
            var dimensi = $('#dimensi').val();
            var weight = $('#weight').val();
 
            html += '<tr>';
            html += '<td><button type="button" class="btn btn-danger btnDelRowItemDetail">Delete</button></td>';
            html += '<td><input type="hidden" name="item_detail_id[]" class="form-control"</td>';
            html += '<td><input type="text" style="width:100px!important;" name="item_code[]" class="form-control "></td>';
            html += '<td><input type="text" style="width:100px!important;" name="item_name[]" class="form-control "></td>';
            html += '<td><select style="width:150px!important;" name="item_type_sempag[]" class="form-control"><option disabled value="" selected>-- Pilih Jenis --</option><option value="Padat">Padat</option><option value="Cairan">Cairan</option><option value="Minyak">Minyak</option><option value="Gas">Gas</option></select></td>';
            html += '<td><input type="text" style="width:100px!important;" name="qty[]" class="form-control "></td>';
            html += '<td><input type="text" style="width:100px!important;" name="unit[]" class="form-control "></td>';
            html += '<td><input type="text" style="width:100px!important;" name="dimensi[]" class="form-control "></td>';
            html += '<td><input type="text" style="width:100px!important;" name="weight[]" class="form-control "></td>';
            html += '</tr>';


            $('#itemdetails').append(html);
            //clear input
            $('#itemOrderDetailsValue').val('');
            updateItemDetail();
        });

        $("#table-customer-item").on("click", ".btnDelRowItemDetail", function (event) {
            $(this).closest("tr").remove();
            updateItemDetail();
        });

    });

    function updateCustomerCost() {
        var sum = 0;
        $('.custCostValue').each(function() {
            sum += Number($(this).val());
        });
        $('#bruto').val(sum);

        var bruto = $('#bruto').val();
        var disc = $('#disc').val();
        var disc_value = bruto * disc / 100;
        var sub_total = bruto - disc_value;        
        var ppn = $('#ppn').val();
        var ppn_value = sub_total * ppn / 100;
        var pph = $('#pph').val();
        var pph_value = sub_total * pph / 100;
        var netto = sub_total + ppn_value - pph_value;
        $('#disc_value').val(disc_value);
        $('#ppn_value').val(ppn_value);
        $('#pph_value').val(pph_value);
        $('#netto').val(netto);
    }

    function updateItemDetail() {

    }


    $(function () {
        $("#addRowDriverCost").on("click", function () {
            var html = "";
            var itemDriverCost = $('#itemDriverCost').val();
            var itemDriverCostValue = $('#itemDriverCostValue').val();

            if(itemDriverCost == null) {
                alert("Can't Add Row. Please choose name cost");
                return false;
            }  else if ($( '.itemDriverCost'+itemDriverCost ).length  || isNaN(itemDriverCostValue)) {
                alert("Can't Add Row. Duplicate Name Cost");
                return false;
            } else if (itemDriverCostValue < 1){
                alert("Can't Add Row. Cost value can't be null");
                return false;
            } 
 
            html += '<tr>';
            html +=  '<td><button type="button" class="btn btn-danger btnDelRowDriverCost">Delete</button></td>';
            html += '<td><input type="hidden" name="driver_cost_id[]" class="itemDriverCost'+itemDriverCost+'" value="'+itemDriverCost+'">';
            html += $('#itemDriverCost option:selected').text()+'</td>';
            html += '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_value[]" onkeyup="updateDriverCost();" class="form-control driverCostValue" value="'+itemDriverCostValue+'" required></td>';
            html += '<td><input style="text-align:right;" type="text" min="0" name="driver_cost_realization[]" class="form-control driverCostRealization" value="0" readonly></td>';
            html += '</tr>';


            $('#budgetdetails').append(html);

            //clear input
            $('#itemDriverCostValue').val('');
            updateDriverCost();
        });

        $('#customer_id').on('change', function() {
            var id = this.value;
            $.ajax({
                url: "{{ url('api/v1/customers') . '/' }}" + id,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    if (response.data.tax == 1) {
                        $('#ppn').val(10);
                    } else {
                        $('#ppn').val(0);
                    }

                    updateCustomerCost();
                }
            });
        });

        $("#table-driver-cost").on("click", ".btnDelRowDriverCost", function (event) {
            $(this).closest("tr").remove();
            updateDriverCost();       
        });

    });
    
    function updateDriverCost() {
        var sumDriverCostValue = 0;
        $('.driverCostValue').each(function() {
            sumDriverCostValue += Number($(this).val());
        });
        $('#total_budgetvalue').val(sumDriverCostValue);


        var sumDriverCostRealization = 0;
        $('.driverCostRealization').each(function() {
            sumDriverCostRealization += Number($(this).val());
        });
        
        $('#total_budgetrealization').val(sumDriverCostRealization);

        var difference = sumDriverCostValue - sumDriverCostRealization;
        $('#difference').val(difference);

    }

</script>

@endsection
