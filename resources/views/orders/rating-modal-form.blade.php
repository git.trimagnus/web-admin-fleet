{{-- Rating Modal --}}
<div class="modal fade" id="modal-form-rating" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="#">Kepatuhan</label>
                        <select class="form-control" id="disciplinerating" name="disciplinerating" required>
                            <option disabled selected>-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="#">Pelayanan</label>
                        <select class="form-control" id="servicerating" name="servicerating" required>
                            <option disabled selected>-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                        <div class="form-group">
                            <label for="#">Keselamatan</label>
                            <select class="form-control" id="safetyrating" name="safetyrating" required>
                                <option disabled selected>-</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    <div class="form-group">
                        <label for="commentrating" class="col-form-label">Comment</label>
                        <textarea name="commentrating" id="commentrating" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Rating Modal --}}