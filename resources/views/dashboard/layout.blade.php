@extends('layouts.master')

@if(Session::get('role_id') == 1)
    @include('dashboard.super-admin.content')
@endif

@if(Session::get('role_id') == 2)
    @include('dashboard.admin.content')
@endif
    


