@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Navigation</li>
            <li class="dropdown active">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                    @if(Session::get('role_id') == 2)
                        <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                    @endif
                </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                    <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada
                        Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                    <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                    <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                    <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/payment') }}" class="nav-link"><i
                        class="fas fa-money-bill"></i><span>Payment</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/maintenance') }}" class="nav-link"><i
                        class="fas fa-table"></i><span>Maintenance</span></a>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
            </li>
            @if(Session::get('role_id') == 1)
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                    <ul class="dropdown-menu">
                            <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                            <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                            <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                            <br>
                        </ul>
                </li>
            @endif
        </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-city"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Company Terdaftar</h4>
                        </div>
                        <div class="card-body" id="total-company">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-building"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Company Aktif</h4>
                        </div>
                        <div class="card-body" id="total-active-company">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h4>Due Date Company</h4>
                    </div>
                    <div class="card-body">
                            <label for="datepicker"><b>Filter :</b></label>
                            <div class="row">
                                <div class="col-4">
                                    <select class="form-control result" name="month" id="month">
                                        <option disabled selected>-- Select Month --</option>
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="form-control result" name="year" id="year">
                                        <option disabled selected>-- Select Year --</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                        <div class="table-responsive">
                            <table class="table table-striped" id="company-datatable">
                                <thead>
                                    <tr>
                                        <th>Nama Company</th>
                                        <th>Jumlah (Rp)</th>
                                        <th>Due Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Status</h4>
                    </div>
                    <div class="card-body">
                        <div class="summary">
                        <div class="summary-item">
                            <h6>-<span class="float-right">Active/All</span></h6>
                            <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <a href="#">
                                <img class="mr-3 rounded" width="40" src="{{URL::to('icons/avatar.png')}}" alt="product">
                                </a>
                                <div class="media-body">
                                <div class="media-right" id="status-admin"></div>
                                <div class="media-title"><a href="#">Admin</a></div>
                                </div>
                            </li>
                            </ul>
                            <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <a href="#">
                                <img class="mr-3 rounded" width="40" src="{{URL::to('icons/avatar.png')}}" alt="product">
                                </a>
                                <div class="media-body">
                                <div class="media-right" id="status-driver"></div>
                                <div class="media-title"><a href="#">Driver</a></div>
                                </div>
                            </li>
                            </ul>
                            <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <a href="#">
                                <img class="mr-3 rounded" width="40" src="{{URL::to('icons/avatar.png')}}" alt="product">
                                </a>
                                <div class="media-body">
                                <div class="media-right" id="status-workshop"></div>
                                <div class="media-title"><a href="#">Workshop</a></div>
                                </div>
                            </li>
                            </ul>
                            <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <a href="#">
                                <img class="mr-3 rounded" width="40" src="{{URL::to('icons/avatar.png')}}" alt="product">
                                </a>
                                <div class="media-body">
                                <div class="media-right" id="status-customer"></div>
                                <div class="media-title"><a href="#">Customer</a></div>
                                </div>
                            </li>
                            </ul>
                            <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <a href="#">
                                <img class="mr-3 rounded" width="40" src="{{URL::to('icons/armada.png')}}" alt="product">
                                </a>
                                <div class="media-body">
                                <div class="media-right" id="status-armada"></div>
                                <div class="media-title"><a href="#">Armada</a></div>
                                </div>
                            </li>
                            </ul>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
var table = $('#company-datatable').DataTable();

    $(function () {
        $.ajax({
            url: "{{ url('api/v1/dashboard/super-admin') }}",
            type: "GET",
            success: function (response) {
                var status_admin = response.company.total_active_user_admin+'/'+response.company.total_user_admin;
                var status_driver = response.company.total_active_user_driver+'/'+response.company.total_user_driver;
                var status_workshop = response.company.total_active_user_workshop+'/'+response.company.total_user_workshop;
                var status_customer = response.company.total_active_customer+'/'+response.company.total_customer;
                var status_armada = response.company.total_active_armada+'/'+response.company.total_armada;
                
                $('#total-company').text(response.company.total_company);
                $('#total-active-company').text(response.company.total_active_company);
                $('#status-admin').text(status_admin);
                $('#status-driver').text(status_driver);
                $('#status-workshop').text(status_workshop);
                $('#status-customer').text(status_customer);
                $('#status-armada').text(status_armada);

            },
            error: function () {

            }
        });
    });

    $(function(){
        // Change to whatever you want // minOffset = 0 for current year 
        var minOffset = -15, maxOffset = 0; 
        var thisYear = (new Date()).getFullYear();
        for (var i = minOffset; i <= maxOffset; i++) {
            var year = thisYear + i; 
            $('<option>', {value: year, text: year}).appendTo("#year");
        }
    });

    $(function() {
        var d = new Date();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        
        $("#month").val(month).attr('selected','selected');
        $("#year").val(year).attr('selected','selected');
        result(month, year);

        $(".result").change(function () {
            var month = $('#month').val();
            var year = $('#year').val();
            result(month, year);
        });
    });

    function result(month, year) {
        var url = "{{url('/api/v1/dashboard/super-admin')}}";
        $.ajax({
            url: url,
            type: "GET",
            data:{
                'month' : month,
                'year' : year
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.payment.data, function (key, val) {
                    table.row.add([
                        val.name, val.total_price, val.due_date
                    ]).draw();
                });
            }
        });
    }


</script>

@endsection
