@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Navigation</li>
            <li class="dropdown active">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                    @if(Session::get('role_id') == 2)
                        <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                    @endif
                </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                    <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada
                        Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                    <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                    <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                    <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/payment') }}" class="nav-link"><i
                        class="fas fa-money-bill"></i><span>Payment</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/maintenance') }}" class="nav-link"><i
                        class="fas fa-table"></i><span>Maintenance</span></a>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Order</h5>
                        <div class="card-stats">
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count" id="orders_ongoing"></div>
                                    <div class="card-stats-item-label">Berjalan</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count">I</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count" id="orders_pending"></div>
                                    <div class="card-stats-item-label">Pending</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Isu</h5>
                        <div class="card-stats">
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count" id="maintenances_open"></div>
                                    <div class="card-stats-item-label">Terbaru</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count">I</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count" id="maintenances_process"></div>
                                    <div class="card-stats-item-label">Proses</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Armada</h5>
                        <div class="card-stats">
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count" id="armadas_active"></div>
                                    <div class="card-stats-item-label">Aktif</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count">I</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-count" id="armadas_nonactive"></div>
                                    <div class="card-stats-item-label">Nonaktif</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h4>Invoices</h4>
                        <div class="card-header-action">
                            <a href="#" onclick="showData();" class="btn btn-danger">Lihat Semua <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive table-invoice">
                            <table class="table table-striped" id="invoice-datatable">
                                <thead>
                                    <tr>
                                        <th>No. Invoice</th>
                                        <th>Customer</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Due Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Performa Armada</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="armada-performance"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h4>Laba dan Rugi</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="profit-and-losss"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Performa Driver</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="driver-performance"></canvas>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>

{{-- Modal --}}
<div class="modal fade" id="modal-form" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-2 offset-10">
                        <a onclick="refresh()" class="btn btn-outline-warning">
                                <i class="fas fa-sync-alt"></i> Refresh</a>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped" id="invoices-detail-datatable">
                        <thead>
                            <tr>
                                <th>No. Order</th>
                                <th>No. Invoice</th>
                                <th>Customer</th>
                                <th>Jatuh Tempo</th>
                                <th>Due Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}
@endsection

@section('javascript')
<script type="text/javascript">
    var invoices = $('#invoices-detail-datatable').DataTable({
                    processing: true,
                    //   serverSide: true,
                    ajax: "{{ url('api/v1/invoices')}}" +  "?company_id=" + "{{ Session::get('company_id')}}",
                    columns: [
                        {data: 'no_order', name: 'no_order'},
                        {data: 'no_invoice', name: 'no_invoice'},
                        {data: 'customer.name', name: 'customer.name'},
                        {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                        {data: 'due_date', name: 'due_date'}
                    ]
                });

    function showData() {
        $('#modal-form').modal('show');
        $('.modal-title').text('Show Invoices');
        invoices.ajax.reload();
    }

    function refresh() {
        invoices.ajax.reload();
    }

    $(function () {
        $.ajax({
            url: "{{ url('api/v1/dashboard') }}",
            type: "GET",
            success: function (response) {

                const array_driver_name = [];
                const array_total_order = [];
                response.driver_performance.forEach(row => {
                    array_driver_name.push(row.name); 
                    array_total_order.push(row.total_order); 
                });
                driverPerformance(array_driver_name, array_total_order);

                const array_armada_name = [];
                const array_totalorder = [];
                const array_totalhours = [];
                response.armada_performance.forEach(row => {
                    array_armada_name.push(row.name); 
                    array_totalorder.push(row.total_order);
                    array_totalhours.push(row.total_hours); 
                });
                armadaPerformance(array_armada_name, array_totalorder, array_totalhours);

                const array_data_armada = [];
                const array_total_profit = [];
                const array_bgcolor_profit = [];
                response.profit_and_loss.forEach(row => {
                    array_data_armada.push(row.name); 
                    array_total_profit.push(row.total_profit);
                    if (row.total_profit < 0) {
                        array_bgcolor_profit.push('#Da2827');
                    } else {
                        array_bgcolor_profit.push('#bdc3c7');
                    }
                });
                profitAndLost(array_data_armada, array_total_profit, array_bgcolor_profit);
                
                
                $('#orders_ongoing').text(response.orders.ongoing);
                $('#orders_pending').text(response.orders.pending);
                $('#maintenances_open').text(response.maintenances.open);
                $('#maintenances_process').text(response.maintenances.process);
                $('#armadas_active').text(response.armadas.active);
                $('#armadas_nonactive').text(response.armadas.nonactive);

                var table = $('#invoice-datatable').DataTable({
                    processing: true,
                    data: response.invoices,
                    columns: [
                        {data: 'no_invoice', name: 'no_invoice'},
                        {data: 'customer', name: 'customer'},
                        {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                        {data: 'due_date', name: 'due_date'},
                    ],
                    columnDefs: [{
                        targets: 2,
                        render: function (data, type, row) {
                            if (row.jatuh_tempo == 'Iya') {
                                var badge = '<div class="badge badge-danger">'+ row.jatuh_tempo +'</div>';
                            } else {
                                var badge = '<div class="badge badge-warning">'+ row.jatuh_tempo +'</div>';
                            }
                            return badge;
                        }
                    }]
                });
            },
            error: function () {

            }
        });
    });


function armadaPerformance(array_armada_name, array_totalorder, array_totalhours) {
    var ctx = document.getElementById("armada-performance").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: array_armada_name,
            datasets: [{
                label: 'Total Hours',
                data: array_totalhours,
                borderWidth: 2,
                backgroundColor: '#Da2827',
                borderColor: '#Da2827',
                borderWidth: 2.5,
                pointBackgroundColor: '#ffffff',
                pointRadius: 4
            },
            {
                label: 'Total Orders',
                data: array_totalorder,
                borderWidth: 2,
                backgroundColor: '#bdc3c7',
                borderColor: '#bdc3c7',
                borderWidth: 2.5,
                pointBackgroundColor: '#ffffff',
                pointRadius: 4
            }]
        },
        options: {
            legend: {
                display: true
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        color: '#f2f2f2'
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    }
                }]
            },
        }
    });
}

    

function profitAndLost(array_data_armada, array_data_profit, array_bgcolor_profit) {
    var ctx = document.getElementById("profit-and-losss").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: array_data_armada,
            datasets: [{
                label: 'Rp. ',
                data: array_data_profit,
                backgroundColor: array_bgcolor_profit,
                borderWidth: 1,
                pointRadius: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        color: '#f2f2f2',
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    }
                }]
            },
        }
    });
}
    



function driverPerformance(array_driver_name, array_total_order) {
    var ctx = document.getElementById("driver-performance").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: array_total_order,
                backgroundColor: [
                    '#696969',
                    '#e4e4e4',
                    '#f87b75',
                    '#df322b',
                    '#9e0404',
                ],
                label: 'Dataset 1'
            }],
            labels: array_driver_name
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom',
            },
        }
    });
}
    

</script>

@endsection
