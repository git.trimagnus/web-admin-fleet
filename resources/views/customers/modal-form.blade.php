{{-- Modal --}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="company_id" name="company_id" value="{{ Session::get('company_id')}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nama Customer</label>
                                <input type="text" name="name" class="form-control" id="name" minlength=3 required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">Alamat</label>
                                <textarea name="address"  class="form-control" id="address" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="pic_name">Nama PIC</label>
                                <input type="text" name="pic_name" class="form-control" id="pic_name" minlength=3 required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="pic_phone">No.tlp PIC</label>
                                <input type="text" name="pic_phone" class="form-control" id="pic_phone" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pic_email">Email PIC</label>
                                <input type="text" name="pic_email" class="form-control" id="pic_email" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="tax">Tax</label>
                                <select class="form-control" id="tax" name="tax" required>
                                    <option disabled value="" selected>-</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="term">Term</label>
                                <select class="form-control" id="term" name="term" required>
                                    <option disabled value="" selected>-</option>
                                    <option value="7">1 Week</option>
                                    <option value="14">2 Weeks</option>
                                    <option value="21">3 Weeks</option>
                                    <option value="30">1 Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" id="status" name="status" required>
                                    <option disabled value="" selected>-</option>
                                    <option value="1">Active</option>
                                    <option value="0">Non Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="is_digdeplus">Is Digdeplus</label>
                                <select class="form-control" id="is_digdeplus" name="is_digdeplus" required>
                                    <option disabled value="" selected>-</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="company_id_isdigdeplus">ID Digdeplus</label>
                                <input type="text" name="company_id_isdigdeplus" class="form-control" id="company_id_isdigdeplus">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}