@extends('layouts.master')

@section('content')

    <div class="main-sidebar">
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="{{ url('/') }}">
                    <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
                </a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="{{ url('/') }}">DK</a>
            </div>
            <ul class="sidebar-menu">
                <li class="menu-header">Main Navigation</li>
                <li class="dropdown">
                    <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                </li>
                @if(Session::get('role_id') == 1)
                    <li class="dropdown">
                        <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
                    </li>
                @endif
                <li class="dropdown active">
                    <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                        @if(Session::get('role_id') == 2)
                            <li class="active"><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                        @endif
                    </ul>
                </li>
                @if(Session::get('role_id') == 1)
                    <li class="dropdown">
                        <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
                    </li>
                    <li class="dropdown">
                        <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
                    </li>
                @endif
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                        <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                        <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                        <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                        <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                        <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
                </li>
                <li class="dropdown">
                    <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
                </li>
                <li class="dropdown">
                    <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
                    <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                        <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                        <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                        <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                        <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                        <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                        <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                        <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                        <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                        <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                        <br>
                    </ul>
                </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
        </aside>
    </div>

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>User Addition Company</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                    <div class="breadcrumb-item">Company</div>
                    <div class="breadcrumb-item active">User Addition</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="container-fluid">
                                    @if(Session::get('role_id') == 2)
                                        <a onclick="addData()" class="btn btn-outline-primary float-right">
                                            <i class="fas fa-plus"></i> Add</a>
                                        <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                            <i class="fas fa-sync-alt"></i> Refresh</a>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="company-datatable">
                                        <thead>
                                        <tr>
                                            <th>No Payment</th>
                                            <th>Tgl. Order</th>
                                            <th>Number User</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="company-body">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post">
                        @csrf
                        @method('POST')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">User Addition</label>
                                    <input type="number" name="user_addition" class="form-control" id="name" minlength=3 required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- /Modal --}}

    {{--Modal 2--}}

    <div class="modal fade" id="modal-form2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post">
                        @csrf
                        @method('POST')
                        <input type="hidden" id="id" name="id">
                        <input type="hidden" id="company_id" name="company_id" value="{{ Session::get('company_id')}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_payment">No. Pembayaran</label>
                                    <input type="text" name="no_payment" class="form-control" id="no_payment" minlength=3 required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date_order">Tgl. Order</label>
                                    <input name="date_order"  class="form-control" id="date_order" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="number_user">Jumlah User</label>
                                    <input type="text" name="number_user" class="form-control" id="number_user" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="unit_price">Unit Price</label>
                                    <input type="text" name="unit_price" class="form-control" id="unit_price" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="total_price">Total Price</label>
                                    <input type="text" name="total_price" class="form-control" id="total_price" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date_paid">Tgl. Terbayar</label>
                                    <input type="text" name="date_paid" class="form-control" id="date_paid" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="via">Via</label>
                                    <input type="text" name="via" class="form-control" id="via" minlength=3 required>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option disabled value="" selected>-</option>
                                        <option value="0">Unpaid</option>
                                        <option value="1">Paid</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--End Modal 2--}}

@endsection

@section('javascript')

    <script type="text/javascript">

        function addData() {
            save_method = "add";
            $('input[name=_method]').val('POST');
            $('#modal-form').modal('show');
            $('#modal-form form')[0].reset();
            $('.modal-title').text('Input Data');
        }


        var table = $('#company-datatable').DataTable({
            processing: true,
            ajax: "{{ url('company/user-addition/index')}}",
            columns: [
                {data: 'no_payment', name: 'no_payment'},
                {data: 'date_order', name: 'date_order'},
                {data: 'number_user', name: 'number_user'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });

        function refresh() {
            table.ajax.reload();
        }

        $('#modal-form2').on('hidden.bs.modal', function (e) {
            $(".form-control").prop("disabled", false);
            $('button[type="submit"]').show();
        })

        function showData(id) {
            save_method = "show";
            $('#modal-form form')[0].reset();

            $.ajax({
                url: "{{ url('company/user-addition') }}" + '/' + id,
                type: "GET",
                success: function (response) {

                    $('#modal-form2').modal('show');
                    $('.modal-title').text('Show Data');

                    console.log(response)

                    $('#id').val(response.data.id);
                    $('#company_id').val(response.data.company_id);
                    $('#no_payment').val(response.data.no_payment);
                    $('#date_order').val(response.data.date_order);
                    $('#via').val(response.data.via ? response.data.via : '-');
                    $('#number_user').val(response.data.number_user);
                    $('#unit_price').val(response.data.unit_price);
                    $('#total_price').val(response.data.total_price);
                    $('#date_paid').val(response.data.date_paid ? response.data.date_paid : '-');
                    $("#status").val(response.data.status).attr('selected','selected');

                    $(".form-control").prop("disabled", true);
                    $('button[type="submit"]').hide();
                },
                error: function () {
                    alert("Nothing Data");
                }
            });
        }


        $(function(){
            $('#modal-form form').on('submit', function (e) {
                if (!e.isDefaultPrevented()){
                    var id = $('#id').val();
                    if (save_method == 'add') url = "{{ url('company/user-addition') }}";
                    else url = "{{ url('company/user-addition') . '/' }}" + id;

                    $.ajax({
                        url : url,
                        type : "POST",
                        data : $('#modal-form form').serialize(),
                        beforeSend: function() {
                            swal({
                                title: 'Now loading',
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                onOpen: () => {
                                swal.showLoading();
                        }
                        })
                        },
                        success : function(response) {
                            // table.ajax.reload();
                            swal({
                                title: 'Success!',
                                text: response.message,
                                type: 'success',
                                timer: '1500'
                            })
                            $('#modal-form').modal('hide');
                        },
                        error : function(response){
                            swal({
                                title: 'Opps...',
                                text: response.responseText,
                                type: 'error',
                                timer: '2000'
                            })
                        }
                    });
                    return false;
                }
            });
        });

    </script>

@endsection
