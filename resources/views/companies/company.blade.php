@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown active">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
            <ul class="dropdown-menu">
                <li class="active"><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                @endif
            </ul>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Company</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">Company</div>
                <div class="breadcrumb-item active">Company</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="company-datatable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nama</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="company-body">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- Modal --}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <form method="post" files=true enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nama Company</label>
                                <input type="text" name="name" class="form-control" id="name" minlength=3 required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">Alamat</label>
                                <textarea name="address"  class="form-control" id="address" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="owner_name">Pemilik Perusahaan</label>
                                <input type="text" name="owner_name" class="form-control" id="owner_name" minlength=3 required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">No.tlp Perusahaan</label>
                                <input type="text" name="phone" class="form-control" id="phone" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email Perusahaan</label>
                                <input type="text" name="email" class="form-control" id="email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="npwp">Npwp</label>
                                <input type="text" name="npwp" class="form-control" id="npwp" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="number_user">Jumlah User</label>
                                <input type="text" name="number_user" class="form-control" id="number_user" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="max_user">Maks. User</label>
                                    <input type="text" name="max_user" class="form-control" id="max_user" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="due_date">Due Date</label>
                                    <input type="text" name="due_date" class="form-control" id="due_date" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option disabled value="" selected>-</option>
                                        <option value="1">Active</option>
                                        <option value="0">Non Active</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="file_surat_jalan">Kop Surat Jalan</label>
                                <input type="file" name="file_surat_jalan" class="form-control" id="file_surat_jalan" accept="image/*">
                                <img id="file_surat_jalan_preview" src="#" width="350" height="300" alt="file_surat_jalan">      
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="file_invoice">Kop Invoice</label>
                                <input type='file' name="file_invoice" class="form-control" id="file_invoice" accept="image/*">
                                <img id="file_invoice_preview" src="#" width="350" height="300" alt="file_invoice">      
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form> -->

                <form method="post" enctype="multipart/form-data" id="form">
                    @csrf
                    @method('POST')
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nama Company</label>
                                <input type="text" name="name" class="form-control" id="name" minlength=3 required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">Alamat</label>
                                <textarea name="address"  class="form-control" id="address" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="owner_name">Pemilik Perusahaan</label>
                                <input type="text" name="owner_name" class="form-control" id="owner_name" minlength=3 required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">No.tlp Perusahaan</label>
                                <input type="text" name="phone" class="form-control" id="phone" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email Perusahaan</label>
                                <input type="text" name="email" class="form-control" id="email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="npwp">Npwp</label>
                                <input type="text" name="npwp" class="form-control" id="npwp" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="number_user">Jumlah User</label>
                                <input type="text" name="number_user" class="form-control" id="number_user" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="max_user">Maks. User</label>
                                    <input type="text" name="max_user" class="form-control" id="max_user" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="due_date">Due Date</label>
                                    <input type="text" name="due_date" class="form-control" id="due_date" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option disabled value="" selected>-</option>
                                        <option value="1">Active</option>
                                        <option value="0">Non Active</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="file_surat_jalan">Kop Surat Jalan</label>
                                <input type="file" name="file_surat_jalan" class="form-control" id="file_surat_jalan" accept="image/*">
                                <input type="hidden" name="hidden_image_kir" id="hidden_image_kir">
                                <img id="file_surat_jalan_preview" src="#" width="350" height="150" alt="file_surat_jalan">      
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="file_invoice">Kop Invoice</label>
                                <input type="file" name="file_invoice" class="form-control" id="file_invoice" accept="image/*">
                                <input type="hidden" name="hidden_file_invoice" id="hidden_file_invoice">
                                <img id="file_invoice_preview" src="#" width="350" height="150" alt="file_invoice">      
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}

@endsection

@section('javascript')

<script type="text/javascript">
    var table = $('#company-datatable').DataTable({
        processing: true,
        ajax: "{{ url('api/v1/companies')}}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'due_date', name: 'due_date'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false }
        ]
    });

    function refresh() {
        table.ajax.reload();
    }

    $('#modal-form').on('hidden.bs.modal', function (e) {
        $(".form-control").prop("disabled", false);
        $('button[type="submit"]').show();
    })

    function showData(id) {
        save_method = "show";
        $('#modal-form form')[0].reset();

        $.ajax({
            url: "{{ url('api/v1/companies') }}" + '/' + id,
            type: "GET",
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Show Data');

                $('#id').val(response.data.id);
                $('#name').val(response.data.name);
                $('#owner_name').val(response.data.owner_name);
                $('#email').val(response.data.email);
                $('#address').val(response.data.address);
                $('#phone').val(response.data.phone);
                $('#npwp').val(response.data.npwp);
                $('#number_user').val(response.data.number_user);
                $('#max_user').val(response.data.max_user);
                $('#file_surat_jalan').val(response.data.file_surat_jalan);
                $('#file_invoice').val(response.data.file_invoice);
                $('#hidden_file_surat_jalan').val(response.data.file_surat_jalan);
                $('#file_surat_jalan_preview').attr('src', response.data.file_surat_jalan);
                $('#hidden_file_invoice').val(response.data.file_invoice);
                $('#file_invoice_preview').attr('src', response.data.file_invoice);
                $('#due_date').val(response.data.due_date);
                $("#status").val(response.data.status).attr('selected','selected');

                $(".form-control").prop("disabled", true);
                $('button[type="submit"]').hide();
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function showFile(fileInput, img, showName) {
        if (fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $(img).attr('src', e.target.result);
        }
        reader.readAsDataURL(fileInput.files[0]);
        }
        $('#file_surat_jalan_preview').text(fileInput.files[0].name)
        $('#file_invoice_preview').text(fileInput.files[0].name)
        }
    
        $('#file_surat_jalan').on('change', function() {
            showFile(this, '#file_surat_jalan_preview');
        });

        $('#file_invoice').on('change', function() {
            showFile(this, '#file_invoice_preview');
        });

    function editData(id) {
        save_method = "edit";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";
        
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/companies') }}" + '/' + id,
            type: "GET",
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                $('#name').val(response.data.name);
                $('#owner_name').val(response.data.owner_name);
                $('#email').val(response.data.email);
                $('#address').val(response.data.address);
                $('#phone').val(response.data.phone);
                $('#npwp').val(response.data.npwp);
                $('#number_user').val(response.data.number_user);
                $('#max_user').val(response.data.max_user);
                $('#due_date').val(response.data.due_date);
                $('#file_surat_jalan').val(response.data.file_surat_jalan);
                $('#file_invoice').val(response.data.file_invoice);
                $("#status").val(response.data.status).attr('selected','selected');

                $('#name').prop("readonly", true);
                $('#email').prop("readonly", true);
                $('#number_user').prop("disabled", true);
                $('#max_user').prop("disabled", true);
                $('#due_date').prop("disabled", true);
                $("#status").prop("disabled", true);
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/v1/companies') }}";
                else url = "{{ url('api/v1/companies') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : new FormData($("#form")[0]),
                    contentType : false, 
                    processData : false,
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        table.ajax.reload();
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                      $('#modal-form').modal('hide');
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

</script>

@endsection
