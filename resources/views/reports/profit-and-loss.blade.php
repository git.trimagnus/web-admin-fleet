@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                @endif
            </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown active">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                <li class="active"><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
            </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Profit And Loss</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">Report</div>
                <div class="breadcrumb-item active">Profit And Loss</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                <a href="#" id="export_excel" class="btn btn-success float-right">Export Excel
                                        <i class="far fa-file-excel"></i></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <label for="datepicker"><b>Filter :</b></label>
                            <div class="row">
                                @if(Session::get('role_id') == 1)
                                    <div class="col-3">
                                        <select class="form-control select2 result" style="width:100%" name="company" id="company">
                                            
                                        </select>
                                    </div>
                                @endif
                                <div class="col-2">
                                    <select class="form-control result" name="month" id="month">
                                        <option disabled selected>-- Select Month --</option>
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <select class="form-control result" name="year" id="year">
                                        <option disabled selected>-- Select Year --</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <select class="form-control result" name="armada_id" id="armada_id">
                                    </select>
                                </div>
                                <div class="col-2">
                                    <a onclick="refresh()" class="btn btn-outline-warning">
                                            <i class="fas fa-sync-alt"></i> Refresh</a>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-striped" id="profit-and-loss-datatable">
                                    <thead>
                                        <tr>
                                            <th>Armada</th>
                                            <th>No. Order</th>
                                            <th>Tgl. Order</th>  
                                            <th>Nilai Order</th>
                                            <th>Kasbon Admin</th>
                                            <th>Actual Cost</th>
                                            <th>Laba</th>
                                            <th>Selisih Operasional</th>  
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('javascript')

<script type="text/javascript">
    var table = $('#profit-and-loss-datatable').DataTable();

    $(function() {
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) {
            $.ajax({
                url: "{{ url('api/v1/companies') }}",
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    $('#company').empty();
                    $('#company').append('<option disabled selected>-- Select Company --</option>');
                    $.each(response.data, function(key, val){
                        $('#company').append('<option value="'+ val.id +'">' + val.name + '</option>');
                    });
                }
            });
        }

        $("#company").change(function () {
            var company_id = $(this).val();

            $.ajax({
                url: "{{ url('api/v1/armadas') }}",
                type: "GET",
                data: {'company_id' : company_id},
                success: function (response) {
                    $('#armada_id').empty();
                    $('#armada_id').append('<option disabled selected>-- Select Armada --</option>');
                    $.each(response.data, function(key, val){
                        $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                    });
                }
            });
        });
    });

    //export excel
    $('#export_excel').click(function () {
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        var month = $('#month').val();
        var year = $('#year').val();
        var armada_id = $('#armada_id').val();
        var url = "{{url('/')}}" + "/api/v1/reports/profit-and-loss/xls?company_id=" + company_id + "&armada_id=" + armada_id + "&month=" + month + "&year=" + year;
        window.open(url);
    });

    $(function(){
        $.ajax({
            url: "{{url('/api/v1/years')}}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#year').empty();
                $('#year').append('<option disabled selected>-- Select Year --</option>');
                $.each(response.data, function(key, val){
                    $('#year').append('<option>' + val.year + '</option>');
                });
            }
        });
    });

    $(function(){
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $.ajax({
            url: "{{ url('api/v1/armadas') }}",
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {
                $('#armada_id').empty();
                $('#armada_id').append('<option disabled selected>-- Select Armada --</option>');
                $.each(response.data, function(key, val){
                    $('#armada_id').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });
    });


    $(function(){
        $(".result").change(function () {
            var role_id = "{{Session::get('role_id')}}";

            if (role_id == 1) var company_id = $('#company').val();
            else var company_id = "{{Session::get('company_id')}}";

            var month = $('#month').val();
            var year = $('#year').val();
            var armada_id = $('#armada_id').val();
            var url = "{{url('/api/v1/reports/profit-and-loss')}}";
            $.ajax({
                url: url,
                type: "GET",
                data:{
                    'company_id' : company_id,
                    'month' : month,
                    'year' : year,
                    'armada_id' : armada_id
                },
                success: function (response) {
                    table.clear().draw();
                    $.each(response.data, function (key, val) {
                        table.row.add([
                            val.name, val.no_order, val.order_date,
                            val.netto, val.total_budgetvalue,
                            val.total_realization, val.total_profit,
                            val.total_difference
                        ]).draw();
                    });
                }
            });
        });
    });

    function refresh() {
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        var month = $('#month').val();
        var year = $('#year').val();
        var armada_id = $('#armada_id').val();
        var url = "{{url('/api/v1/reports/profit-and-loss')}}";
        $.ajax({
            url: url,
            type: "GET",
            data:{
                'company_id' : company_id,
                'month' : month,
                'year' : year,
                'armada_id' : armada_id
            },
            beforeSend: function() {
                swal({
                    title: 'Now loading',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                    swal.showLoading();
                    }
                })
            },
            success: function (response) {
                table.clear().draw();
                $.each(response.data, function (key, val) {
                    table.row.add([
                        val.name, val.no_order, val.order_date,
                        val.netto, val.total_budgetvalue,
                        val.total_realization, val.total_profit,
                        val.total_difference
                    ]).draw();
                });
                swal.close();
            }
        });
    }

</script>

@endsection
