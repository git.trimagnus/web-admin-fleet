<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <th>Nama Driver</th>
                <th>Total Order</th>
                <th>Rate Discipline</th>  
                <th>Rate Service</th>  
                <th>Rate Safety</th>  
                <th>Avg Rate</th> 
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->name}}</td>
                <td>{{$row->total_order}}</td>
                <td>{{$row->avg_disciplinerating}}</td>
                <td>{{$row->avg_servicerating}}</td>
                <td>{{$row->avg_safetyrating}}</td>
                <td>{{$row->total_avg}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>