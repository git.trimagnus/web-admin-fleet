<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td>Nama Bengkel</td>
                <td>Total Repair</td>  
                <td>Good Rate</td>  
                <td>Fair Rate</td>  
                <td>Bad Rate</td>  
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->vendor}}</td>
                <td>{{$row->total_repair}}</td>
                <td>{{$row->total_good_rate}}</td>
                <td>{{$row->total_fair_rate}}</td>
                <td>{{$row->total_bad_rate}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>