<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td>Insurance Company</td>
                <td>Total Polis</td>  
                <td>Total Armada Cover</td>  
                <td>Total Good Rate</td>  
                <td>Total Fair Rate</td>  
                <td>Total Bad Rate</td>  
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->insurance_company}}</td>
                <td>{{$row->total_polis}}</td>
                <td>{{$row->total_armada}}</td>
                <td>{{$row->total_good_rate}}</td>
                <td>{{$row->total_fair_rate}}</td>
                <td>{{$row->total_bad_rate}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>