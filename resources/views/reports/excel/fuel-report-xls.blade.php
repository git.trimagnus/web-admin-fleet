<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <th>Tanggal</th>
                <th>Armada</th>  
                <th>Km</th>  
                <th>Liter</th>
                <th>KM per Liter</th>
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->date}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->km}}</td>
                <td>{{$row->liter}}</td>
                <td>{{$row->fuel_km}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>