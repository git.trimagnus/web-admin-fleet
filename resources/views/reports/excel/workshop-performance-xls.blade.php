<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td>Total Maintenance</td>
                <td>Total Ontime</td>
                <td>Total Non Ontime</td>  
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->total_maintenance}}</td>
                <td>{{$row->total_ontime}}</td>
                <td>{{$row->total_non_ontime}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>