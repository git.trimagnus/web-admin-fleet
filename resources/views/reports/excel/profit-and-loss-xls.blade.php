<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td>Armada</td>
                <td>No. Order</td>
                <td>Tgl. Order</td>  
                <td>Nilai Order</td>
                <td>Kasbon Admin</td>
                <td>Actual Cost</td>
                <td>Laba</td>
                <td>Selisih Operasional</td>  
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->name}}</td>
                <td>{{$row->no_order}}</td>
                <td>{{$row->order_date}}</td>
                <td>{{$row->netto}}</td>
                <td>{{$row->total_budgetvalue}}</td>
                <td>{{$row->total_realization}}</td>
                <td>{{$row->total_profit}}</td>
                <td>{{$row->total_difference}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>