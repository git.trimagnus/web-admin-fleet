<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td>Armada</td>
                <td>Total Order</td>
                <td>Total Hours</td>  
                <td>Total Maintenance Rutin</td>
                <td>Total Maintenance Non Rutin</td>
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->name}}</td>
                <td>{{ ($row->total_order != null) ? $row->total_order : 0  }}</td>
                <td>{{$row->total_hours}}</td>
                <td>{{ ($row->total_maintenance_rutin != null) ? $row->total_maintenance_rutin : 0  }}</td>
                <td>{{ ($row->total_maintenance_nonrutin != null) ? $row->total_maintenance_nonrutin : 0  }}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>