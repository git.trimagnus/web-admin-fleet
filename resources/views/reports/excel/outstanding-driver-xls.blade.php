<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <td>Nama Driver</td>
                <td>No. Order</td>  
                <td>Tgl. Order</td>  
                <td>Total Uang dari Admin</td>  
                <td>Actual Cost</td> 
                <td>Selisih Uang Operasional</td>  
            </tr>
            @foreach ($data as $row)
            <tr>
                <td>{{$row->name}}</td>
                <td>{{$row->no_order}}</td>
                <td>{{$row->order_date}}</td>
                <td>{{$row->total_budgetvalue}}</td>
                <td>{{$row->total_realization}}</td>
                <td>{{$row->total_difference}}</td>
            </tr>
            @endforeach   
        </table> 
    </body>
</html>