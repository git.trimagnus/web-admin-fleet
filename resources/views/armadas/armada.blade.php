@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                    @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                    @endif
                </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown active">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li class="active"><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
        </ul>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Armada</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">Armada Management</div>
                <div class="breadcrumb-item active">Armada</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                @if(Session::get('role_id') == 2)
                                <a onclick="addData()" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if(Session::get('role_id') == 1)
                            <label for="#"><b>Filter :</b></label>
                                <div class="row">
                                    <div class="col-3">
                                        <select class="form-control select2" style="width:100%" name="company" id="company">
                                            
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-striped" id="armada-datatable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>No. Polisi</th>
                                            <th>Merk</th>      
                                            <th>Tipe</th>      
                                            <th>Status</th>      
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('armadas.modal-form')

@endsection

@section('javascript')
<script type="text/javascript">
$(function() {
    var role_id = "{{Session::get('role_id')}}";
    
    if (role_id == 1) {
        var table = $('#armada-datatable').DataTable();

        $.ajax({
            url: "{{ url('api/v1/companies') }}",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $('#company').empty();
                $('#company').append('<option disabled selected>-- Select Company --</option>');
                $.each(response.data, function(key, val){
                    $('#company').append('<option value="'+ val.id +'">' + val.name + '</option>');
                });
            }
        });

        $("#company").change(function () {
            var company_id = $(this).val();
            var url = "{{ url('api/v1/armadas')}}" +  "?company_id=" + company_id;

            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    table.clear().draw();
                    $.each(response.data, function (key, val) {
                        table.row.add([
                            val.name, val.plate_number, val.merk, 
                            val.type, val.status, val.action
                        ]).draw();
                    });
                }
            });
        });

    } else {
        var url = "{{ url('api/v1/armadas')}}" +  "?company_id=" + "{{ Session::get('company_id')}}";
        var table = $('#armada-datatable').DataTable({
            processing: true,
            ajax: url,
            columns: [
                {data: 'name', name: 'name'},
                {data: 'plate_number', name: 'plate_number'},
                {data: 'merk', name: 'merk'},
                {data: 'type', name: 'type'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    }

});

    $(document).on('click', '.datepicker', function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }).focus();
        $(this).removeClass('datepicker'); 
    });

    function showFile(fileInput, img, showName) {
        if (fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $(img).attr('src', e.target.result);
        }
        reader.readAsDataURL(fileInput.files[0]);
        }
        $('#image_stnk_preview').text(fileInput.files[0].name)
        $('#image_kir_preview').text(fileInput.files[0].name)
        }
    
        $('#image_stnk').on('change', function() {
            showFile(this, '#image_stnk_preview');
        });

        $('#image_kir').on('change', function() {
            showFile(this, '#image_kir_preview');
        });

    function refresh() {
        $('#armada-datatable').DataTable().ajax.reload();
    }

    $('#modal-form').on('hidden.bs.modal', function (e) {
        $(".form-control").prop("disabled", false);
        $('button[type="submit"]').show();
    })

    function showData(id) {
        save_method = "show";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";
        
        $('button[type="submit"]').hide();
        $("#tiredetails").empty();
        $('#modal-form form')[0].reset();

        $.ajax({
            url: "{{ url('api/v1/armadas') }}" + '/' + id,
            type: "GET",
            data : {'company_id' : company_id},
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Show Data');

                $('#id').val(response.data.id);
                $('#name').val(response.data.name);
                $('#plate_number').val(response.data.plate_number);
                $('#year').val(response.data.year);
                $('#series').val(response.data.series);
                $('#merk').val(response.data.merk);
                $('#color').val(response.data.color);
                $('#type').val(response.data.type);
                $('#status option[value='+ response.data.status +']').prop('selected','selected');
                $('#odometer').val(response.data.odometer);
                $('#max_weight').val(response.data.max_weight);
                $('#length').val(response.data.length);
                $('#width').val(response.data.width);
                $('#height').val(response.data.height);
                $('#volume').val(response.data.volume);
                $('#carrosserie_type option[value='+ response.data.carrosserie_type +']').prop('selected','selected');
                $('#carrosserie_condition option[value='+ response.data.carrosserie_condition +']').prop('selected','selected');
                $('#carrosserie_location').val(response.data.carrosserie_location);
                $('#insurance option[value='+ response.data.insurance +']').prop('selected','selected');
                $('#insurance_company').val(response.data.insurance_company);
                $('#polis_type').val(response.data.polis_type);
                $('#no_polis').val(response.data.no_polis);
                $('#insurance_expired').val(response.data.insurance_expired);
                $('#polis_period').val(response.data.polis_period);
                $('#quality_insurance option[value='+ response.data.quality_insurance +']').prop('selected','selected');
                $('#description_insurance').val(response.data.description_insurance);
                $('#machine_number').val(response.data.machine_number);
                $('#chassis_number').val(response.data.chassis_number);
                $('#stnk_number').val(response.data.stnk_number);
                $('#stnk_expired').val(response.data.stnk_expired);
                $('#kir_number').val(response.data.kir_number);
                $('#kir_expired').val(response.data.kir_expired);
                $('#hidden_image_stnk').val(response.data.image_stnk);
                $('#image_stnk_preview').attr('src', response.data.image_stnk);
                $('#hidden_image_kir').val(response.data.image_kir);
                $('#image_kir_preview').attr('src', response.data.image_kir);
                $('#number_tires option[value='+ response.data.number_tires +']').prop('selected','selected');

                $.each(response.data.tiredetails, function(key, val){
                    //check for spare tire
                    if (val.tire_id == 101 ) {
                        $name = 'Serep 1';
                        
                    } else if (val.tire_id == 102){
                        $name = 'Serep 2';
                    } else {
                        $name = 'Posisi ' + val.tire_id;
                    }

                    var cols = '<div class="col-6">'+
'                                    <div class="card bg-light">'+
'                                    <div class="card-header">'+
'                                        <ul class="nav nav-tabs">'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link active" data-toggle="tab" href="#ban_' + val.tire_id + '" role="tab" aria-controls="ban_' + val.tire_id + '" aria-selected="true">Ban</a>'+
'                                            </li>'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link" data-toggle="tab" href="#detailban_' + val.tire_id + '" role="tab" aria-controls="detailban_' + val.tire_id + '" aria-selected="true">Detail</a>'+
'                                            </li>'+
'                                        </ul>'+
'                                    </div>'+
'                                    <div class="card-body">'+
'                                        <h4 class="card-title" id="position_' + val.tire_id + '">' + $name + '</h4>'+
'                                        <div class="tab-content">'+
'                                        <div class="tab-pane fade show active" id="ban_' + val.tire_id + '" role="tabpanel" aria-labelledby="ban-tab">'+
'                                            <input type="hidden" id="t_tire_id_' + val.tire_id + '" name="t_tire_id[' + val.tire_id + ']" value="' + val.tire_id + '">'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_merk_' + val.tire_id + '">Merk*</label>'+
'                                                        <input type="text" name="t_merk[' + val.tire_id + ']" class="form-control" id="t_merk_' + val.tire_id + '" value="' + val.merk + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_number_' + val.tire_id + '">Kode Ban*</label>'+
'                                                        <input type="text" name="t_tire_number[' + val.tire_id + ']" class="form-control" id="t_tire_number_' + val.tire_id + '" value="' + val.tire_number + '" required>                                                        '+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_serial_number_' + val.tire_id + '">No. Seri</label>'+
'                                                        <input type="text" name="t_serial_number[' + val.tire_id + ']" class="form-control" id="t_serial_number_' + val.tire_id + '" value="' + val.serial_number + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_production_code_' + val.tire_id + '">Kode Produksi</label>'+
'                                                        <input type="text" name="t_production_code[' + val.tire_id + ']" class="form-control" id="t_production_code_' + val.tire_id + '" value="' + val.production_code + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                        </div>'+
'                                        <div class="tab-pane" id="detailban_' + val.tire_id + '" role="tabpanel" aria-labelledby="detail-ban-tab">'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_date_installation_' + val.tire_id + '">Tanggal Pasang*</label>'+
'                                                        <input type="text" name="t_date_installation[' + val.tire_id + ']" class="form-control datepicker" id="t_date_installation_' + val.tire_id + '" value="' + val.date_installation + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_km_' + val.tire_id + '">Km Ban</label>'+
'                                                        <input type="number" name="t_km[' + val.tire_id + ']" class="form-control" id="t_km_' + val.tire_id + '" value="' + val.km + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_thick_tire_' + val.tire_id + '">Tebal Ban</label>'+
'                                                        <input type="number" name="t_thick_tire[' + val.tire_id + ']" class="form-control" id="t_thick_tire_' + val.tire_id + '" value="' + val.thick_tire + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_pressure_' + val.tire_id + '">Tekanan</label>'+
'                                                        <input type="number" name="t_tire_pressure[' + val.tire_id + ']" class="form-control" id="t_tire_pressure_' + val.tire_id + '" value="' + val.tire_pressure + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_stamp_' + val.tire_id + '">Stamp / Cap Ban*</label>'+
'                                                        <select class="form-control" id="t_stamp_' + val.tire_id + '" name="t_stamp[' + val.tire_id + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="sudah">Sudah</option>'+
'                                                            <option value="belum">Belum</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tirecondition_id_' + val.tire_id + '" class="col-form-label">Kondisi*</label>'+
'                                                        <select class="form-control" id="t_tirecondition_id_' + val.tire_id + '" name="t_tirecondition_id[' + val.tire_id + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="1">Orisinil</option>'+
'                                                            <option value="2">Vulkanisir</option>'+
'                                                            <option value="3">Rotasi</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>  '+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_description_' + val.tire_id + '">Deskripsi</label>'+
'                                                        <textarea name="t_description[' + val.tire_id + ']" id="t_description_' + val.tire_id + '" cols="30" rows="10"  class="form-control"></textarea>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_image_tire_' + val.tire_id + '">Gambar Ban</label>'+
'                                                        <input type="file" name="t_image_tire[' + val.tire_id + ']" class="form-control" id="t_image_tire_' + val.tire_id + '" accept="image/*">'+
'                                                    </div>'+
'                                                </div>'+
'                                             </div>';
                $('#tiredetails').append(cols);

                $('#t_stamp_' + val.tire_id + '').val(val.stamp).attr('selected','selected');
                $('#t_tirecondition_id_' + val.tire_id + '').val(val.tirecondition_id).attr('selected','selected');
                $('#t_description_' + val.tire_id + '').val(val.description);
            
            });

            $(".form-control").prop("disabled", true);
                
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function addData() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $("#tiredetails").empty();
        $('#modal-form form')[0].reset();
        $('#status').prop('selectedIndex',0);
        $('#carrosserie_type').prop('selectedIndex',0);
        $('#carrosserie_condition').prop('selectedIndex',0);
        $('#insurance').prop('selectedIndex',0);
        $('#quality_insurance').prop('selectedIndex',0);
        $('#number_tires').prop('selectedIndex',0);
        $('#image_stnk_preview').attr('src', '#');
        $('#image_kir_preview').attr('src', '#');
        $('.modal-title').text('Input Data');

    }

    function editData(id) {
        save_method = "edit";
        var role_id = "{{Session::get('role_id')}}";

        if (role_id == 1) var company_id = $('#company').val();
        else var company_id = "{{Session::get('company_id')}}";

        $('input[name=_method]').val('PATCH');
        $("#tiredetails").empty();
        $('#modal-form form')[0].reset();
        
        
        $.ajax({
            url: "{{ url('api/v1/armadas') }}" + '/' + id,
            type: "GET",
            data: {'company_id' : company_id},
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                $('#name').val(response.data.name);
                $('#plate_number').val(response.data.plate_number);
                $('#year').val(response.data.year);
                $('#series').val(response.data.series);
                $('#merk').val(response.data.merk);
                $('#color').val(response.data.color);
                $('#type').val(response.data.type);
                $('#status option[value='+ response.data.status +']').prop('selected','selected');
                $('#odometer').val(response.data.odometer);
                $('#max_weight').val(response.data.max_weight);
                $('#length').val(response.data.length);
                $('#width').val(response.data.width);
                $('#height').val(response.data.height);
                $('#volume').val(response.data.volume);
                $('#carrosserie_type option[value='+ response.data.carrosserie_type +']').prop('selected','selected');
                $('#carrosserie_condition option[value='+ response.data.carrosserie_condition +']').prop('selected','selected');
                $('#carrosserie_location').val(response.data.carrosserie_location);
                $('#insurance option[value='+ response.data.insurance +']').prop('selected','selected');
                $('#insurance_company').val(response.data.insurance_company);
                $('#polis_type').val(response.data.polis_type);
                $('#no_polis').val(response.data.no_polis);
                $('#insurance_expired').val(response.data.insurance_expired);
                $('#polis_period').val(response.data.polis_period);
                $('#quality_insurance option[value='+ response.data.quality_insurance +']').prop('selected','selected');
                $('#description_insurance').val(response.data.description_insurance);
                $('#machine_number').val(response.data.machine_number);
                $('#chassis_number').val(response.data.chassis_number);
                $('#stnk_number').val(response.data.stnk_number);
                $('#stnk_expired').val(response.data.stnk_expired);
                $('#kir_number').val(response.data.kir_number);
                $('#kir_expired').val(response.data.kir_expired);
                $('#hidden_image_stnk').val(response.data.image_stnk);
                $('#image_stnk_preview').attr('src', response.data.image_stnk);
                $('#hidden_image_kir').val(response.data.image_kir);
                $('#image_kir_preview').attr('src', response.data.image_kir);
                $('#number_tires option[value='+ response.data.number_tires +']').prop('selected','selected');

                $.each(response.data.tiredetails, function(key, val){

                    //check for spare tire
                    if (val.tire_id == 101 ) {
                        $name = 'Serep 1';
                        
                    } else if (val.tire_id == 102){
                        $name = 'Serep 2';
                    } else {
                        $name = 'Posisi ' + val.tire_id;
                    }

                    var cols = '<div class="col-6">'+
'                                    <div class="card bg-light">'+
'                                    <div class="card-header">'+
'                                        <ul class="nav nav-tabs">'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link active" data-toggle="tab" href="#ban_' + val.tire_id + '" role="tab" aria-controls="ban_' + val.tire_id + '" aria-selected="true">Ban</a>'+
'                                            </li>'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link" data-toggle="tab" href="#detailban_' + val.tire_id + '" role="tab" aria-controls="detailban_' + val.tire_id + '" aria-selected="true">Detail</a>'+
'                                            </li>'+
'                                        </ul>'+
'                                    </div>'+
'                                    <div class="card-body">'+
'                                        <h4 class="card-title" id="position_' + val.tire_id + '">' + $name + '</h4>'+
'                                        <div class="tab-content">'+
'                                        <div class="tab-pane fade show active" id="ban_' + val.tire_id + '" role="tabpanel" aria-labelledby="ban-tab">'+
'                                            <input type="hidden" id="t_tire_id_' + val.tire_id + '" name="t_tire_id[' + val.tire_id + ']" value="' + val.tire_id + '">'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_merk_' + val.tire_id + '">Merk*</label>'+
'                                                        <input type="text" name="t_merk[' + val.tire_id + ']" class="form-control" id="t_merk_' + val.tire_id + '" value="' + val.merk + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_number_' + val.tire_id + '">Kode Ban*</label>'+
'                                                        <input type="text" name="t_tire_number[' + val.tire_id + ']" class="form-control" id="t_tire_number_' + val.tire_id + '" value="' + val.tire_number + '" required>                                                        '+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_serial_number_' + val.tire_id + '">No. Seri</label>'+
'                                                        <input type="text" name="t_serial_number[' + val.tire_id + ']" class="form-control" id="t_serial_number_' + val.tire_id + '" value="' + val.serial_number + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_production_code_' + val.tire_id + '">Kode Produksi</label>'+
'                                                        <input type="text" name="t_production_code[' + val.tire_id + ']" class="form-control" id="t_production_code_' + val.tire_id + '" value="' + val.production_code + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                        </div>'+
'                                        <div class="tab-pane" id="detailban_' + val.tire_id + '" role="tabpanel" aria-labelledby="detail-ban-tab">'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_date_installation_' + val.tire_id + '">Tanggal Pasang*</label>'+
'                                                        <input type="text" name="t_date_installation[' + val.tire_id + ']" class="form-control datepicker" id="t_date_installation_' + val.tire_id + '" value="' + val.date_installation + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_km_' + val.tire_id + '">Km Ban</label>'+
'                                                        <input type="number" name="t_km[' + val.tire_id + ']" class="form-control" id="t_km_' + val.tire_id + '" value="' + val.km + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_thick_tire_' + val.tire_id + '">Tebal Ban</label>'+
'                                                        <input type="number" name="t_thick_tire[' + val.tire_id + ']" class="form-control" id="t_thick_tire_' + val.tire_id + '" value="' + val.thick_tire + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_pressure_' + val.tire_id + '">Tekanan</label>'+
'                                                        <input type="number" name="t_tire_pressure[' + val.tire_id + ']" class="form-control" id="t_tire_pressure_' + val.tire_id + '" value="' + val.tire_pressure + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_stamp_' + val.tire_id + '">Stamp / Cap Ban*</label>'+
'                                                        <select class="form-control" id="t_stamp_' + val.tire_id + '" name="t_stamp[' + val.tire_id + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="sudah">Sudah</option>'+
'                                                            <option value="belum">Belum</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tirecondition_id_' + val.tire_id + '" class="col-form-label">Kondisi*</label>'+
'                                                        <select class="form-control" id="t_tirecondition_id_' + val.tire_id + '" name="t_tirecondition_id[' + val.tire_id + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="1">Orisinil</option>'+
'                                                            <option value="2">Vulkanisir</option>'+
'                                                            <option value="3">Rotasi</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>  '+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_description_' + val.tire_id + '">Deskripsi</label>'+
'                                                        <textarea name="t_description[' + val.tire_id + ']" id="t_description_' + val.tire_id + '" cols="30" rows="10"  class="form-control"></textarea>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_image_tire_' + val.tire_id + '">Gambar Ban</label>'+
'                                                        <input type="file" name="t_image_tire[' + val.tire_id + ']" class="form-control" id="t_image_tire_' + val.tire_id + '" accept="image/*">'+
'                                                    </div>'+
'                                                </div>'+
'                                             </div>';
                $('#tiredetails').append(cols);

                $('#t_stamp_' + val.tire_id + '').val(val.stamp).prop('selected','selected');
                $('#t_tirecondition_id_' + val.tire_id + '').val(val.tirecondition_id).attr('selected','selected');
                $('#t_description_' + val.tire_id + '').val(val.description);

                            
            });
                
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $.ajax({
                url : "{{ url('api/v1/armadas') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success : function(response) {
                    $('#armada-datatable').DataTable().ajax.reload();
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function (response) {
                    swal({
                        title: 'Oops...',
                        text: response.responseText,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/v1/armadas') }}";
                else url = "{{ url('api/v1/armadas') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : new FormData($("#form")[0]),
                    contentType : false, 
                    processData : false,
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        $('#armada-datatable').DataTable().ajax.reload();
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                        $('#modal-form').modal('hide');
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });

    $('#number_tires').change(function(){ 
        var value = $(this).val();
        $("#tiredetails").empty();
        
        for (let counter = 1; counter <= value; counter++) {
            // newRow.append(cols);
            $('#tiredetails').append(tire(counter));
        }

        for (let counter = 101; counter <= 102; counter++) {
            // newRow.append(cols);
            $('#tiredetails').append(spareTire(counter));
        }
        
    });

    function tire(counter) {
        var cols = '<div class="col-6">'+
'                                    <div class="card bg-light">'+
'                                    <div class="card-header">'+
'                                        <ul class="nav nav-tabs">'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link active" data-toggle="tab" href="#ban_' + counter + '" role="tab" aria-controls="ban_' + counter + '" aria-selected="true">Ban</a>'+
'                                            </li>'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link" data-toggle="tab" href="#detailban_' + counter + '" role="tab" aria-controls="detailban_' + counter + '" aria-selected="true">Detail</a>'+
'                                            </li>'+
'                                        </ul>'+
'                                    </div>'+
'                                    <div class="card-body">'+
'                                        <h4 class="card-title" id="position_' + counter + '">Posisi ' + counter + '</h4>'+
'                                        <div class="tab-content">'+
'                                        <div class="tab-pane fade show active" id="ban_' + counter + '" role="tabpanel" aria-labelledby="ban-tab">'+
'                                            <input type="hidden" id="t_tire_id_' + counter + '" name="t_tire_id[' + counter + ']" value="' + counter + '">'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_merk_' + counter + '">Merk*</label>'+
'                                                        <input type="text" name="t_merk[' + counter + ']" class="form-control" id="t_merk_' + counter + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_number_' + counter + '">Kode Ban*</label>'+
'                                                        <input type="text" name="t_tire_number[' + counter + ']" class="form-control" id="t_tire_number_' + counter + '" required>                                                        '+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_serial_number_' + counter + '">No. Seri</label>'+
'                                                        <input type="text" name="t_serial_number[' + counter + ']" class="form-control" id="t_serial_number_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_production_code_' + counter + '">Kode Produksi</label>'+
'                                                        <input type="text" name="t_production_code[' + counter + ']" class="form-control" id="t_production_code_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                        </div>'+
'                                        <div class="tab-pane" id="detailban_' + counter + '" role="tabpanel" aria-labelledby="detail-ban-tab">'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_date_installation_' + counter + '">Tanggal Pasang*</label>'+
'                                                        <input type="text" name="t_date_installation[' + counter + ']" class="form-control datepicker" id="t_date_installation_' + counter + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_km_' + counter + '">Km Ban</label>'+
'                                                        <input type="number" name="t_km[' + counter + ']" class="form-control" id="t_km_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_thick_tire_' + counter + '">Tebal Ban</label>'+
'                                                        <input type="number" name="t_thick_tire[' + counter + ']" class="form-control" id="t_thick_tire_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_pressure_' + counter + '">Tekanan</label>'+
'                                                        <input type="number" name="t_tire_pressure[' + counter + ']" class="form-control" id="t_tire_pressure_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_stamp_' + counter + '">Stamp / Cap Ban*</label>'+
'                                                        <select class="form-control" id="t_stamp_' + counter + '" name="t_stamp[' + counter + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="sudah">Sudah</option>'+
'                                                            <option value="belum">Belum</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tirecondition_id_' + counter + '" class="col-form-label">Kondisi*</label>'+
'                                                        <select class="form-control" id="t_tirecondition_id_' + counter + '" name="t_tirecondition_id[' + counter + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="1">Orisinil</option>'+
'                                                            <option value="2">Vulkanisir</option>'+
'                                                            <option value="3">Rotasi</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>  '+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_description_' + counter + '">Deskripsi</label>'+
'                                                        <textarea name="t_description[' + counter + ']" id="t_description_' + counter + '" cols="30" rows="10"  class="form-control"></textarea>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_image_tire_' + counter + '">Gambar Ban</label>'+
'                                                        <input type="file" name="t_image_tire[' + counter + ']" class="form-control" id="t_image_tire_' + counter + '" accept="image/*">'+
'                                                    </div>'+
'                                                </div>'+
'                                             </div>';

        return cols;
        
    }

    function spareTire(counter) {

        if (counter == 101) {
            var number = 1;
        } else {
            var number = 2;
        }
        
        var cols = '<div class="col-6">'+
'                                    <div class="card bg-light">'+
'                                    <div class="card-header">'+
'                                        <ul class="nav nav-tabs">'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link active" data-toggle="tab" href="#ban_' + counter + '" role="tab" aria-controls="ban_' + counter + '" aria-selected="true">Ban</a>'+
'                                            </li>'+
'                                            <li class="nav-item">'+
'                                                <a class="nav-link" data-toggle="tab" href="#detailban_' + counter + '" role="tab" aria-controls="detailban_' + counter + '" aria-selected="true">Detail</a>'+
'                                            </li>'+
'                                        </ul>'+
'                                    </div>'+
'                                    <div class="card-body">'+
'                                        <h4 class="card-title" id="position_' + counter + '">Serep ' + number + '</h4>'+
'                                        <div class="tab-content">'+
'                                        <div class="tab-pane fade show active" id="ban_' + counter + '" role="tabpanel" aria-labelledby="ban-tab">'+
'                                            <input type="hidden" id="t_tire_id_' + counter + '" name="t_tire_id[' + counter + ']" value="' + counter + '">'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_merk_' + counter + '">Merk*</label>'+
'                                                        <input type="text" name="t_merk[' + counter + ']" class="form-control" id="t_merk_' + counter + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_number_' + counter + '">Kode Ban*</label>'+
'                                                        <input type="text" name="t_tire_number[' + counter + ']" class="form-control" id="t_tire_number_' + counter + '" required>                                                        '+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_serial_number_' + counter + '">No. Seri</label>'+
'                                                        <input type="text" name="t_serial_number[' + counter + ']" class="form-control" id="t_serial_number_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_production_code_' + counter + '">Kode Produksi</label>'+
'                                                        <input type="text" name="t_production_code[' + counter + ']" class="form-control" id="t_production_code_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                        </div>'+
'                                        <div class="tab-pane" id="detailban_' + counter + '" role="tabpanel" aria-labelledby="detail-ban-tab">'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_date_installation_' + counter + '">Tanggal Pasang*</label>'+
'                                                        <input type="text" name="t_date_installation[' + counter + ']" class="form-control datepicker" id="t_date_installation_' + counter + '" required>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_km_' + counter + '">Km Ban</label>'+
'                                                        <input type="number" name="t_km[' + counter + ']" class="form-control" id="t_km_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_thick_tire_' + counter + '">Tebal Ban</label>'+
'                                                        <input type="number" name="t_thick_tire[' + counter + ']" class="form-control" id="t_thick_tire_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>'+
'                                            <div class="row">'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tire_pressure_' + counter + '">Tekanan</label>'+
'                                                        <input type="number" name="t_tire_pressure[' + counter + ']" class="form-control" id="t_tire_pressure_' + counter + '">'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_stamp_' + counter + '">Stamp / Cap Ban*</label>'+
'                                                        <select class="form-control" id="t_stamp_' + counter + '" name="t_stamp[' + counter + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="sudah">Sudah</option>'+
'                                                            <option value="belum">Belum</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-4">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_tirecondition_id_' + counter + '" class="col-form-label">Kondisi*</label>'+
'                                                        <select class="form-control" id="t_tirecondition_id_' + counter + '" name="t_tirecondition_id[' + counter + ']" required>'+
'                                                            <option disabled value="" selected>-</option>'+
'                                                            <option value="1">Orisinil</option>'+
'                                                            <option value="2">Vulkanisir</option>'+
'                                                            <option value="3">Rotasi</option>'+
'                                                        </select>'+
'                                                    </div>'+
'                                                </div>'+
'                                            </div>  '+
'                                            <div class="row">'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_description_' + counter + '">Deskripsi</label>'+
'                                                        <textarea name="t_description[' + counter + ']" id="t_description_' + counter + '" cols="30" rows="10"  class="form-control"></textarea>'+
'                                                    </div>'+
'                                                </div>'+
'                                                <div class="col-6">'+
'                                                    <div class="form-group">'+
'                                                        <label for="t_image_tire_' + counter + '">Gambar Ban</label>'+
'                                                        <input type="file" name="t_image_tire[' + counter + ']" class="form-control" id="t_image_tire_' + counter + '" accept="image/*">'+
'                                                    </div>'+
'                                                </div>'+
'                                             </div>';

        return cols;
        
    }
    
</script>

@endsection
