{{-- Modal --}}
<div class="modal fade" id="modal-form" trole="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="form" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="hidden" id="id" name="id">
                <input type="hidden" id="company_id" name="company_id" value="{{ Session::get('company_id')}}">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="armada-tab" data-toggle="tab" href="#armada" role="tab" aria-controls="armada" aria-selected="true">Armada</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="spesifikasi-tab" data-toggle="tab" href="#spesifikasi" role="tab" aria-controls="spesifikasi" aria-selected="true">Spesifikasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="dokumen-tab" data-toggle="tab" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="true">Dokumen</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="asuransi-tab" data-toggle="tab" href="#asuransi" role="tab" aria-controls="asuransi" aria-selected="true">Asuransi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="ban-tab" data-toggle="tab" href="#ban" role="tab" aria-controls="ban" aria-selected="true">Ban</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="katalog-tab" data-toggle="tab" href="#katalog" role="tab" aria-controls="ban" aria-selected="true">Katalog Ban</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="armada" role="tabpanel" aria-labelledby="armada-tab">
                        <div class="row">
                            <div class="col-md-3 offset-md-3">
                                <div class="form-group">
                                    <label for="name">Nama Armada*</label>
                                    <input type="text" name="name" class="form-control" id="name" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="plate_number">No. Plat*</label>
                                    <input type="text" name="plate_number" class="form-control" id="plate_number" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 offset-md-3">
                                <div class="form-group">
                                    <label for="year">Tahun*</label>
                                    <input type="number" name="year" class="form-control" id="year" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="series">Jenis*</label>
                                    <input type="text" name="series" class="form-control" id="series" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="merk">Merk*</label>
                                    <input type="text" name="merk" class="form-control" id="merk" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 offset-md-3">
                                <div class="form-group">
                                    <label for="color">Warna*</label>
                                    <input type="text" name="color" class="form-control" id="color">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="type">Tipe*</label>
                                    <input type="text" name="type" class="form-control" id="type" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="status">Status*</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option disabled value="" selected>-</option>
                                        <option value="0">Active</option>
                                        <option value="1">Non Active</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="spesifikasi" role="tabpanel" aria-labelledby="spesifikasi-tab">
                        <div class="row">
                            <div class="col-md-2 offset-md-4">
                                <div class="form-group">
                                    <label for="odometer">Odometer</label>
                                    <input type="number" name="odometer" class="form-control" id="odometer">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="max_weight">Maks. Berat</label>
                                    <input type="number" name="max_weight" class="form-control" id="max_weight">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 offset-md-2">
                                <div class="form-group">
                                    <label for="length">Panjang</label>
                                    <input type="number" name="length" class="form-control" id="length">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="width">Lebar</label>
                                    <input type="number" name="width" class="form-control" id="width">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="height">Tinggi</label>
                                    <input type="number" name="height" class="form-control" id="height">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="volume">Volume</label>
                                    <input type="number" name="volume" class="form-control" id="volume">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 offset-md-2">
                                <div class="form-group">
                                    <label for="carrosserie_type">Tipe Karoseri</label>
                                    <select class="form-control" id="carrosserie_type" name="carrosserie_type">
                                        <option value="" selected>-</option>
                                        <option value="dump">Dump</option>
                                        <option value="box">Box</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="carrosserie_condition">Kondisi</label>
                                    <select class="form-control" id="carrosserie_condition" name="carrosserie_condition">
                                        <option value="" selected>-</option>
                                        <option value="terpasang">Terpasang</option>
                                        <option value="tidakterpasang">Tidak Terpasang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="carrosserie_location">Lokasi</label>
                                    <textarea class="form-control" name="carrosserie_location" id="carrosserie_location" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab">
                        <div class="row">
                            <div class="col-md-3 offset-md-3">
                                <div class="form-group">
                                    <label for="machine_number">No. Mesin</label>
                                    <input type="text" name="machine_number" class="form-control" id="machine_number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="chassis_number">No. Rangka</label>
                                    <input type="text" name="chassis_number" class="form-control" id="chassis_number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 offset-md-2">
                                <div class="form-group">
                                    <label for="stnk_number">No. STNK</label>
                                    <input type="text" name="stnk_number" class="form-control" id="stnk_number">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="stnk_expired">STNK Expired</label>
                                    <input type="text" name="stnk_expired" class="form-control datepicker" id="stnk_expired">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="kir_number">No. KIR</label>
                                    <input type="text" name="kir_number" class="form-control" id="kir_number">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="kir_expired">KIR Expired</label>
                                    <input type="text" name="kir_expired" class="form-control datepicker" id="kir_expired">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 offset-md-2">
                                <div class="form-group">
                                    <label for="image_stnk">Photo STNK</label>
                                    <input type="file" name="image_stnk" class="form-control" id="image_stnk" accept="image/*">
                                    <input type="hidden" name="hidden_image_stnk" id="hidden_image_stnk">
                                    <img id="image_stnk_preview" src="#" width="350" height="300" alt="image_stnk">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="image_kir">Photo KIR</label>
                                    <input type="file" name="image_kir" class="form-control" id="image_kir" accept="image/*">
                                    <input type="hidden" name="hidden_image_kir" id="hidden_image_kir">
                                    <img id="image_kir_preview" src="#" width="350" height="300" alt="image_kir">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="asuransi" role="tabpanel" aria-labelledby="asuransi-tab">
                        <div class="row">
                            <div class="col-md-2 offset-md-3">
                                <div class="form-group">
                                    <label for="insurance">Asuransi</label>
                                    <select class="form-control" id="insurance" name="insurance">
                                        <option value="" selected>-</option>
                                        <option value="ada">Ada</option>
                                        <option value="tidak">Tidak Ada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="insurance_company">Nama Perusahaan Asuransi</label>
                                    <input type="text" class="form-control" name="insurance_company" id="insurance_company">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 offset-md-3">
                                <div class="form-group">
                                    <label for="polis_type">Jenis Polis</label>
                                    <input type="text" class="form-control" name="polis_type" id="polis_type">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_polis">No. Polis</label>
                                    <input type="text" class="form-control" name="no_polis" id="no_polis">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 offset-md-3">
                                <div class="form-group">
                                    <label for="insurance_expired">Masa Berlaku</label>
                                    <input type="text" class="form-control datepicker" name="insurance_expired" id="insurance_expired">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="polis_period">Periode Polis</label>
                                    <input type="text" class="form-control datepicker" name="polis_period" id="polis_period">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="quality_insurance">Kualitas Asuransi</label>
                                    <select class="form-control" id="quality_insurance" name="quality_insurance">
                                        <option value="" selected>-</option>
                                        <option value="bagus">Bagus</option>
                                        <option value="cukup">Cukup Bagus</option>
                                        <option value="kurang">Kurang Bagus</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <div class="form-group">
                                    <label for="description_insurance">Keterangan</label>
                                    <textarea class="form-control" name="description_insurance" id="description_insurance" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ban" role="tabpanel" aria-labelledby="ban-tab">
                        <div class="row">
                            <div class="col-2">
                                <div class="form-group">
                                        <label for="number_tires">Jumlah Ban</label>
                                        <select class="form-control" id="number_tires" name="number_tires">
                                            <option disabled value="" selected>-</option>
                                            <option value="4">4</option>
                                            <option value="6">6</option>
                                            <option value="8">8</option>
                                            <option value="10">10</option>
                                            <option value="12">12</option>
                                            <option value="14">14</option>
                                            <option value="18">18</option>
                                            <option value="22">22</option>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="tiredetails">
                            
                        </div>
                    </div> 
                    <div class="tab-pane fade" id="katalog" role="tabpanel" aria-labelledby="katalog-tab">
                        <div class="row">
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_4.png" width="150" height="350" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 4</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_6.png" width="150" height="350" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 6</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_8.png" width="150" height="350" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 8</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_10.png" width="150" height="350" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 10</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_12.png" width="150" height="700" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 12</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_14.png" width="150" height="700" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 14</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_18.png" width="150" height="700" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 18</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card bg-light">
                                    <img class="card-img-top" src="http://api.digkontrol.com/uploads/armada/tirecatalog/ban_22.png" width="150" height="700" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-text text-center">Ban 22</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
{{-- /Modal --}}