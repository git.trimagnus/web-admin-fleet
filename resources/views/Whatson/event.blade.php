@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-building"></i><span>Company</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/company') }}">Company</a></li>
                    @if(Session::get('role_id') == 2)
                    <li><a class="nav-link" href="{{ url('/company/user-addition') }}">User Addition</a></li>
                    @endif
                </ul>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                    <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                    <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                    <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                    <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                    <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                    <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
                </ul>
        </li>
        @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Whats on Website</span></a>
                <ul class="dropdown-menu">
                        <li><a class="nav-link" href="{{ url('Whats-on/event') }}">Event</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/onmedia') }}">On Media</a></li>
                        <li><a class="nav-link" href="{{ url('Whats-on/updates') }}">Updates</a></li>
                        <br>
                    </ul>
            </li>
        @endif
    </ul>
    </aside>
</div>

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Event</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item">Whats on</div>
                <div class="breadcrumb-item active">Event</div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                                @if(Session::get('role_id') == 2)
                                <a data-toggle="modal" data-target="#addvideo" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add Video</a>
                                <a data-toggle="modal" data-target="#addpoto" class="btn btn-outline-primary float-right">
                                    <i class="fas fa-plus"></i> Add Photo</a>
                                <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                    <i class="fas fa-sync-alt"></i> Refresh</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- add video event -->
<div class="modal fade" id="addvideo" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Event video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label>Input Title</label>
                            <input type="text" name="Title" class="form-control" id="Title" placeholder="Title">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>Input link video</label>
                            <input type="text" name="link_video" class="form-control" id="link_video" placeholder="Link Youtube video">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Desciption</label>
                            <center>
                                <textarea class="description" name="description"></textarea>
                                <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
                                <script>
                                    tinymce.init({
                                        selector:'textarea.description',
                                        width: 900,
                                        height: 300
                                    });
                                </script>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href=""  class="btn btn-primary">Save</a>
            </div>                           
        </div>
    </div>
</div>
<!-- add poto event -->
<div class="modal fade" id="addpoto" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Event Photo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_image" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Input Title</label>
                                <input type="text" name="Title" class="form-control" id="Title" placeholder="Title">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="image_upload">Input Photo</label>
                                <input type="file" name="image_upload" class="form-control" id="image_upload" accept="image/*">
                            </div>
                        </div>
                        <div class="col-12">
                            <input type="hidden" name="hidden_image_preview" id="hidden_image_preview">
                            <img id="image_preview" src="#" width="100%" height="auto" alt="image_preview">
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Desciption</label>
                                <center>
                                    <textarea class="description" name="description"></textarea>
                                    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
                                    <script>
                                        tinymce.init({
                                            selector:'textarea.description',
                                            width: 900,
                                            height: 300
                                        });
                                    </script>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
                     
                                     
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
    function showFile(fileInput, img, showName) {
        if (fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $(img).attr('src', e.target.result);
        }
        reader.readAsDataURL(fileInput.files[0]);
        }
        $('#image_preview').text(fileInput.files[0].name)
        }
    
        $('#image_upload').on('change', function() {
            showFile(this, '#image_preview');
        });

    $(function(){
        $('#addpoto form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){

                $.ajax({
                    type : "POST",
                    data : new FormData($("#form_image")[0]),
                    contentType : false, 
                    processData : false,
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        // $('#armada-datatable').DataTable().ajax.reload();
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '1500'
                        })
                        $('#form_image').modal('hide');
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                        })
                    }
                });
                return false;
            }
        });
    });


// function post_image() {

//         var form = $("#form_image"); 
//         var formdata = false;
//         if (window.FormData){
//             formdata = new FormData(form[0]);
//         }

//         var formAction = form.attr('action');
//         $.ajaxSetup({
//             headers: {
//                 'X-CSRF-TOKEN': "{{csrf_token()}}"
//             }
//         });
//         $.ajax({
//             url: "{{route('create_event_poto')}}",
//             method: 'post',
//             data: formdata ? formdata : form.serialize(),
//             cache       : false,
//             contentType : false,
//             processData : false,
//             success: function(response, textStatus, request){
//                 // console.log(response);
//                 if (response.code != 500) {
//                     if (response.data.code == 201) {
//                         swal({
//                             title: "Data Successfully Saved",
//                             type: "success",
//                             icon: "success"
//                         });
//                     // alert('Data berhasil di Simpan!');
//                         setTimeout(function() {
//                             window.location.replace('{{route('index_event')}}');
//                         },2500);
//                     }
//                 }else{
//                         swal({
//                             title: "Data Failed to Save",
//                             text: "some field is required. please check your input form",
//                             // text: JSON.stringify(response[0]),
//                             type: "error",
//                             icon: "error"
//                         });
//                         // console.log(response);
//                     }
//                 // console.log(response);
                
//                 // $('#modalApproval').modal('hide');
//                 // alert('Data berhasil di Simpan!');
//                 // window.location.replace('{{route('index_event')}}');
//             },
//             error: function (response) {
//                 swal({
//                     title: "Data Gagal Di Simpan",
//                     text: JSON.stringify(response[0]),
//                     type: "error",
//                     icon: "error"
//                 });
//             }
//         });
//     // console.log("sukses!");
//     }
        
</script>
@endsection