@extends('layouts.master')

@section('content')

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
        <a href="{{ url('/') }}">
                <img src="{{URL::to('icons/logo2.png')}}" alt="Logo" width="160">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}">DK</a>
        </div>
        <ul class="sidebar-menu">
        <li class="menu-header">Main Navigation</li>
        <li class="dropdown">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        @if(Session::get('role_id') == 1)
        <li class="dropdown">
            <a href="{{ url('/subscription') }}" class="nav-link"><i class="fas fa-coins"></i><span>Subscription</span></a>
        </li>
        @endif
        <li class="dropdown">
                <a href="{{ url('/company') }}" class="nav-link"><i class="far fa-building"></i><span>Company</span></a>
            </li>
            @if(Session::get('role_id') == 1)
            <li class="dropdown">
                <a href="{{ url('/company-bill') }}" class="nav-link"><i class="fas fa-file-invoice-dollar"></i><span>Company Bill</span></a>
            </li>
            <li class="dropdown">
                <a href="{{ url('/revenue') }}" class="nav-link"><i class="fas fa-angle-double-up"></i><span>Revenue</span></a>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>User Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/user-management/admin') }}">Admin</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/driver') }}">Driver</a></li>
                <li><a class="nav-link" href="{{ url('/user-management/workshop') }}">Workshop</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/customer') }}" class="nav-link"><i class="fas fa-user"></i><span>Customer</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-shuttle-van"></i><span>Armada Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/armada-management/armada') }}">Armada</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/checklist') }}">Checklist</a></li>
                <li><a class="nav-link" href="{{ url('/armada-management/reminder') }}">Reminder</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i><span>Cost Management</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/cost-management/bill-cost') }}">Bill Cost</a></li>
                <li><a class="nav-link" href="{{ url('/cost-management/actual-cost') }}">Actual Cost</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ url('/order') }}" class="nav-link"><i class="fas fa-table"></i><span>Order</span></a>
        </li>
        <li class="dropdown active">
            <a href="{{ url('/payment') }}" class="nav-link"><i class="fas fa-money-bill"></i><span>Payment</span></a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/maintenance') }}" class="nav-link"><i class="fas fa-table"></i><span>Maintenance</span></a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i><span>Report</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url('/report/fuel-report') }}">Fuel Report</a></li>
                <li><a class="nav-link" href="{{ url('/report/armada-performance') }}">Armada Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/insurance-performance') }}">Insurance Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/delivery-order') }}">Delivery Order</a></li>
                <li><a class="nav-link" href="{{ url('/report/profit-and-loss') }}">Profit And Loss</a></li>
                <li><a class="nav-link" href="{{ url('/report/workshop-performance') }}">Workshop Performance</a></li>
                <li><a class="nav-link" href="{{ url('/report/external-workshop') }}">External Workshop</a></li>
                <li><a class="nav-link" href="{{ url('/report/rating-driver') }}">Rating Driver</a></li>
                <li><a class="nav-link" href="{{ url('/report/outstanding-driver') }}">Outstanding Driver</a></li>
                <li><a class="nav-link" href="{{ url('/report/outstanding-customer') }}">Outstanding Customer</a></li>
                    <br>
            </ul>
        </li>
    </aside>
</div>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Payment Order (Detail)</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></div>
                <div class="breadcrumb-item"><a href="{{ url('/payment') }}">Payment</a></div>
                <div class="breadcrumb-item active">Detail</div>
            </div>
        </div>

        <div class="section-body">
                <div class="card">
                    <div class="card-header">
                        <h4 id="no_order"></h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>Tanggal Order</td>
                                                <td id="order_date"></td>
                                            </tr>
                                            <tr>
                                                <td>No. Invoice</td>
                                                <td id="no_invoice"></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Customer</td>
                                                <td id="customer"></td>
                                            </tr>
                                            <tr>
                                                <td>Nilai Invoice</td>
                                                <td id="netto"></td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah Terbayar</td>
                                                <td id="payment"></td>
                                            </tr>
                                            <tr>
                                                <td>Sisa Bayar</td>
                                                <td id="outstanding"></td>
                                            </tr>'
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="container-fluid">
                            @if(Session::get('role_id') == 2)
                            <a onclick="addData()" class="btn btn-outline-primary float-right">
                                <i class="fas fa-plus"></i> Add</a>
                            <a onclick="refresh()" class="btn btn-outline-warning float-right">
                                <i class="fas fa-sync-alt"></i> Refresh</a>
                            @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="billingdetail-datatable">
                                    <thead>
                                        <tr>
                                            <th>No. Pembayaran</th>
                                            <th>Tanggal</th>  
                                            <th>Via</th>  
                                            <th>Dibayarkan</th>   
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('billingdetails.modal-form')

@endsection

@section('javascript')

<script type="text/javascript">
$(function() {
    var table = $('#billingdetail-datatable').DataTable({
        processing: true,
        ajax: "{{ url('api/v1/billingdetails')}}" + "/" + "?order_id=" + {{$order_id}} + "&company_id=" + {{$company_id}},
        columns: [
            {data: 'no_payment', name: 'no_payment'},
            {data: 'payment_date', name: 'payment_date'},
            {data: 'via', name: 'via'},
            {data: 'payment_amount', name: 'payment_amount'},
            {data: 'action', name: 'action', orderable: false, searchable: false }
        ]
    });
});

    $(function(){
        var url = "{{ url('api/v1/orders') }}" + "/" + {{$order_id}} + "?company_id=" + {{$company_id}};

        $.ajax({
            url: url,
            type: "GET",
            success: function (response) {
                
                $('#no_order').text(response.data.no_order);
                $('#order_date').text(response.data.order_date);
                $('#no_invoice').text(response.data.no_invoice);
                $('#customer').text(response.data.customer.name);

                $('#netto').text(response.data.netto);
                $('#payment').text(response.data.payment);
                $('#outstanding').text(response.data.outstanding);
     
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    });

    $('#payment_date').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });

    function refresh() {
        $('#billingdetail-datatable').DataTable().ajax.reload();
    }

    $('#modal-form').on('hidden.bs.modal', function (e) {
        $(".form-control").prop("disabled", false);
        $('button[type="submit"]').show();
    })

    function showData(id) {
        save_method = "show";
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/billingdetails') }}" + "/" + id + "?company_id=" + {{$company_id}},
            type: "GET",
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Show Data');

                $('#id').val(response.data.id);
                $('#payment_date').val(response.data.payment_date);
                $('#via').val(response.data.via);
                $('#payment_amount').val(response.data.payment_amount);

                $(".form-control").prop("disabled", true);
                $('button[type="submit"]').hide();
     
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function addData() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Input Data');
    }

    function editData(id) {
        save_method = "edit";

        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('api/v1/billingdetails') }}" + '/' + id + "?company_id=" + {{$company_id}},
            type: "GET",
            success: function (response) {

                $('#modal-form').modal('show');
                $('.modal-title').text('Edit Data');

                $('#id').val(response.data.id);
                $('#payment_date').val(response.data.payment_date);
                $('#via').val(response.data.via);
                $('#payment_amount').val(response.data.payment_amount);
     
            },
            error: function () {
                alert("Nothing Data");
            }
        });
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $.ajax({
                url : "{{ url('api/v1/billingdetails') }}" + '/' + id,
                type : "POST",
                data : {
                    '_method' : 'DELETE',
                    '_token' : csrf_token,
                    'order_id' : {{$order_id}}
                },
                beforeSend: function() {
                    swal({
                        title: 'Now loading',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        onOpen: () => {
                        swal.showLoading();
                        }
                    })
                },
                success : function(response) {
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '2000'
                    })
                    window.location.reload();
                },
                error : function (response) {
                    swal({
                        title: 'Oops...',
                        text: response.responseText,
                        type: 'error',
                        timer: '2000'
                    })
                }
            });
        });
    }


    $(function(){
        $('#modal-form form').on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('api/v1/billingdetails') }}";
                else url = "{{ url('api/v1/billingdetails') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
                    data : $('#modal-form form').serialize(),
                    beforeSend: function() {
                        swal({
                            title: 'Now loading',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                            swal.showLoading();
                            }
                        })
                    },
                    success : function(response) {
                        swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            timer: '2000'
                        })
                      $('#modal-form').modal('hide');
                      window.location.reload();
                    },
                    error : function(response){
                        swal({
                            title: 'Opps...',
                            text: response.responseText,
                            type: 'error',
                            timer: '2000'
                        })
                    }
                });
                return false;
            }
        });
    });


</script>

@endsection
