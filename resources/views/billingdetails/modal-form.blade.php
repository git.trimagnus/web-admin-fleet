{{-- Modal --}}
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post">
                        @csrf
                        @method('POST')
                        <input type="hidden" id="id" name="id">
                        <input type="hidden" id="order_id" name="order_id" value="{{$order_id}}">
                        <input type="hidden" id="company_id" name="company_id" value="{{$company_id}}">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="payment_date">Tgl. Bayar*</label>
                                <input type="text" name="payment_date" class="form-control" id="payment_date" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="via">Via*</label>
                                <input type="text" name="via" class="form-control" id="via" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="payment_amount">Jumlah dibayarkan*</label>
                                <input type="text" name="payment_amount" class="form-control" id="payment_amount" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- /Modal --}}