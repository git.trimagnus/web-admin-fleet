<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use function GuzzleHttp\json_encode;

class LoginController extends Controller
{
    public function login()
    {
        if (session()->has('token')) {
            return redirect('/');
        }
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $headers = [
            'device' => 'web',
            'Content-Type' => 'application/json'
        ];
 
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->post(config('global.url').'/api/v1/users/login', [
                'json' => $request->all(),
                'headers' => $headers
            ]);

            // $statuscode = $res->getStatusCode();
            $response = json_decode($res->getBody());
            // $response = $res->getBody();

        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return redirect('login')->with([
                'error' => 'These credentials do not match our records.'
                ]);
    
        }


        $token = $response->token;
        $email = $response->user->email;
        $role_id = $response->user->role_id;
        /*split the string bases on the @ position*/
        $parts = explode('@', $email);
        $username = $parts[0];
        $company_id = $response->user->company_id;
        $status = $response->user->status;

        // Login Super Admin
        if ($role_id == 1) {
            Session::put('token', $token);
            Session::put('username', $username);
            Session::put('role_id', $role_id);
            Session::put('company_id', $company_id);

            return redirect('/');

        } elseif ($role_id == 2 && $status == 2) {
            Session::put('token', $token);
            Session::put('username', $username);
            Session::put('role_id', $role_id);
            Session::put('company_id', $company_id);

            return redirect('/');

        // Login Admin
        } else {
            return redirect('login')->with([
                'error' => 'These credentials do not match our records.'
                ]);
        }

        
  
    }
    
    public function doLogout()
    {
        Session::flush();
        return redirect('login');
    }
    
}
