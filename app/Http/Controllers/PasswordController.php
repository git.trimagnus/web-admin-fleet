<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    public function change(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/users/change-password', [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
 
    }

    public function sendResetLinkEmail(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/forgot-password/email', [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return redirect('password/reset')->with([
                'message' => 'Password link has been sent. Please check your Email.'
            ]);
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return redirect('password/reset')->with([
                'message' => 'The selected email is invalid.'
            ]);
            return response()->json($response, 400);
        }

    }

    public function getViewPasswordReset()
    {
        return view('auth.password-reset');
        
    }
}
