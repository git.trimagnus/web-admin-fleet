<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{
    public function index()
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/dashboard',[
            'headers' => $headers,
            'json'    => [
                'company_id' => session('company_id'),
            ]
        ]);

        $response = json_decode($res->getBody());

        $today = date('Y-m-d');

        $invoices = [];
        foreach ($response->invoices as $row) {
            array_push($invoices,[
                'no_order' => $row->no_order,
                'no_invoice' => $row->no_invoice,
                'jatuh_tempo' => $jatuh_tempo = ($today >= $row->due_date) ? 'Iya' : 'Belum',
                'customer' => $row->customer->name,
                'due_date' => $row->due_date
            ]);
        }

        $result = [
            'status' => 'success',
            'orders' => [
                    'ongoing' => $response->orders->ongoing,
                    'pending' => $response->orders->pending,
            ],
            'maintenances' => [
                    'open' => $response->maintenances->open,
                    'process' => $response->maintenances->process
            ],
            'armadas' => [
                    'active' => $response->armadas->active,
                    'nonactive' => $response->armadas->nonactive
            ],
            'invoices' => $invoices,
            'armada_performance' => $response->armada_performance,
            'driver_performance' => $response->driver_performance,
            'profit_and_loss' => $response->profit_and_loss
        ];

        return response()->json($result, 200);

    }

    public function dashboardSuperAdmin(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/dashboard/super-admin',[
            'headers' => $headers,
            'json'    => [
                'month' => $request->month,
                'year' => $request->year
            ]
        ]);

        $response = json_decode($res->getBody());
        return response()->json($response, 200);
    }
}
