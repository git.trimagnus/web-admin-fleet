<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;

class ExcelController extends Controller
{
    public function fuelReport(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/fuel-report',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'armada_id' => $request->armada_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Fuel Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.fuel-report-xls', compact('data'));
            });
        })->export('xlsx');

    }
    
    public function armadaPerformance(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/armada-performance',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Armada Performance Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.armada-performance-xls', compact('data'));
            });
        })->export('xlsx');

    }

    public function insurancePerformance(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/insurance-performance',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                // 'month' => $request->month,
                // 'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Insurance Performance Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.insurance-performance-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function deliveryOrder(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/delivery-order',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'armada_id' => $request->armada_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Delivery Order Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.delivery-order-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function profitAndLoss(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/profit-and-loss',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'armada_id' => $request->armada_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Profit Loss Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.profit-and-loss-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function workshopPerformance(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/workshop-performance',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'updated_by' => $request->user_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Workshop Performance Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.workshop-performance-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function externalWorkshop(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/external-workshop',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('External Workshop Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.external-workshop-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function ratingDriver(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/rating-driver',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'user_id' => $request->user_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Rating Driver Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.rating-driver-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function outstandingDriver(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/outstanding-driver',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'user_id' => $request->user_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Outstanding Driver Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.outstanding-driver-xls', compact('data'));
            });
        })->export('xlsx');
    }

    public function outstandingCustomer(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/outstanding-customer',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'customer_id' => $request->customer_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        Excel::create('Outstanding Customer Report', function($excel) use ($data) {
            $excel->sheet('Excel sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.excel.outstanding-customer-xls', compact('data'));
            });
        })->export('xlsx');
    }

    
}
