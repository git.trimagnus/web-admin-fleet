<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        if (session('role_id') == 1) {
            $sendrequest = [];
        } else {
            $sendrequest = [
                'id' => session('company_id')
            ];
        }
        
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/companies',[
            'headers' => $headers,
            'json' => $sendrequest
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('status', function ($data){
            if ($data->status == 0) {
                return 'Non Active';
            } else  {
                return 'Active';      
            }
        })
        ->addColumn('action', function($data){
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>';
            }
        })
        ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/companies/'.$id, [
                'headers' => $headers
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return dd($request->file_surat_jalan);
        $request->validate([
            'name' => 'required',
            'owner_name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'npwp' => 'required',
            'file_surat_jalan' => 'nullable|mimes:jpeg,jpg,png',
            'file_invoice' => 'nullable|mimes:jpeg,jpg,png',
        ]);

        $file_surat_jalan = $request->file('file_surat_jalan') ? 
        'data:image/'.$request->file('file_surat_jalan')->extension().';base64,'.base64_encode(file_get_contents($request->file('file_surat_jalan'))) : null;

        $file_invoice = $request->file('file_invoice') ? 
        'data:image/'.$request->file('file_invoice')->extension().';base64,'.base64_encode(file_get_contents($request->file('file_invoice'))) : null;

        $sendrequest = [
            'name' => $request->name,
            'owner_name' => $request->owner_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'npwp' => $request->npwp,
            'file_surat_jalan' => $file_surat_jalan,
            'file_invoice' => $file_invoice
        ];

        // dd($sendrequest);

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/companies/'.$id, [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);

        // dd($res);

            // var_dump($res);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function getViewCompany()
    {
        return view('companies.company');
    }

    public function getAddMaxUser()
    {
        return view('companies.add-max-user');
    }

    /**
     * Function to add user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAddMaxUser(Request $request)
    {
        $request->validate([
            'user_addition' => 'required|integer|gt:0',
        ]);

        $company_id = Session::get('company_id');
        $user_addition = $request->user_addition;

        $sendrequest = [
            'company_id' => $company_id,
            'number_user' => $user_addition,
        ];

        $token = session('token');
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->post(config('global.url').'/api/v1/companies/user-additional', [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);

            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function indexAddUserTransaction()
    {
        $token = session('token');
        $company_id = session('company_id');

        $client = new \GuzzleHttp\Client();

        if (session('role_id') == 1) {
            $sendrequest = [];
        } else {
            $sendrequest = [

            ];
        }

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/companies/'.$company_id.'/add-user-transaction',[
            'headers' => $headers,
            'json' => $sendrequest
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
            ->editColumn('status', function ($data){
                if ($data->status == 0) {
                    return 'Unpaid';
                } else  {
                    return 'Paid';
                }
            })
            ->addColumn('action', function($data){
                if (session('role_id') == 1) {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
                } else {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
                }
            })
            ->make(true);
    }

    public function showAddUserTransaction($id)
    {
        $token = session('token');
        $company_id = session('company_id');

        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/companies/'.$company_id.'/add-user-transaction/'.$id, [
                'headers' => $headers
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

}
