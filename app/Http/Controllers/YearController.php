<?php

namespace App\Http\Controllers;

class YearController extends Controller
{
    public function index()
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/years',[
            'headers' => $headers
        ]);

        $response = json_decode($res->getBody());
        return response()->json($response, 200);

    }
}
