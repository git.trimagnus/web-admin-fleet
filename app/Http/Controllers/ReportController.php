<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    public function fuelReport(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/fuel-report',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'armada_id' => $request->armada_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }
    
    public function armadaPerformance(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/armada-performance',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('total_order', function ($data){
            if ($data->total_order == null) {
                return 0;
            } else  {
                return $data->total_order;      
            }
        })
        ->editColumn('total_maintenance_rutin', function ($data){
            if ($data->total_maintenance_rutin == null) {
                return 0;
            } else  {
                return $data->total_maintenance_rutin;      
            }
        })
        ->editColumn('total_maintenance_nonrutin', function ($data){
            if ($data->total_maintenance_nonrutin == null) {
                return 0;
            } else  {
                return $data->total_maintenance_nonrutin;      
            }
        })
        ->make(true);
    }

    public function insurancePerformance(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/insurance-performance',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                // 'month' => $request->month,
                // 'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function deliveryOrder(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/delivery-order',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'armada_id' => $request->armada_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function profitAndLoss(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/profit-and-loss',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'armada_id' => $request->armada_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function workshopPerformance(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/workshop-performance',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'updated_by' => $request->user_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function externalWorkshop(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/external-workshop',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function ratingDriver(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/rating-driver',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'user_id' => $request->user_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function outstandingDriver(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/outstanding-driver',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'user_id' => $request->user_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function outstandingCustomer(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/reports/outstanding-customer',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'customer_id' => $request->customer_id,
                'month' => $request->month,
                'year' => $request->year,
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)->make(true);
    }

    public function getViewFuelReport()
    {
        return view('reports.fuel-report');
    }

    public function getViewReportArmadaPerformance()
    {
        return view('reports.armada-performance');
    }

    public function getViewReportInsurancePerformance()
    {
        return view('reports.insurance-performance');
    }

    public function getViewReportDeliveryOrder()
    {
        return view('reports.delivery-order');
    }

    public function getViewProfitAndLoss()
    {
        return view('reports.profit-and-loss');
    }

    public function getViewReportWorkshopPerformance()
    {
        return view('reports.workshop-performance');
    }

    public function getViewReportExternalWorkshop()
    {
        return view('reports.external-workshop');
    }

    public function getViewReportRatingDriver()
    {
        return view('reports.rating-driver');
    }

    public function getViewReportOutstandingDriver()
    {
        return view('reports.outstanding-driver');
    }

    public function getViewReportOutstandingCustomer()
    {
        return view('reports.outstanding-customer');
    }

    
}
