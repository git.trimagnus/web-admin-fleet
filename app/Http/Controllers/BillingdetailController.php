<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BillingdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/billingdetails',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id,
                'order_id' => $request->order_id
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->addColumn('action', function($data){
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
            }   
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'order_id' => 'required',
            'company_id' => 'required',
            'payment_date' => 'required',
            'via' => 'required',
            'payment_amount' => 'required',
        ]);

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/billingdetails', [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/billingdetails/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'order_id' => 'required',
            'company_id' => 'required',
            'payment_date' => 'required',
            'via' => 'required',
            'payment_amount' => 'required',
        ]);

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/billingdetails/'.$id, [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $request->validate([
            'order_id' => 'required',
        ]);

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v1/billingdetails/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id'),
                    'order_id' => $request->order_id
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function getViewPayment()
    {
        return view('billingdetails.billingdetail');
    }

    public function getViewBillingdetail(Request $request, $id)
    {
        return view('billingdetails.detail',[
            'order_id' => $id,
            'company_id' => $request->company_id
        ]);
    }
}
