<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/orders',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('status_order', function ($data){
            // Progress :
            // 0. Open
            // 1. On progress
            // Pending :
            // 6. Pending
            // 3. Verified
            // 2. Delivered
            // History :
            // 4. Closed
            // 5. Cancel
            if ($data->status_order == 0) {
                return 'Open';
            } elseif ($data->status_order == 1) {
                return 'Progress';
            } elseif ($data->status_order == 2) {
                return 'Delivered';
            } elseif ($data->status_order == 3) {
                return 'Verified';
            } elseif ($data->status_order == 4) {
                return 'Closed';
            } elseif ($data->status_order == 5) {
                return 'Cancel';
            } else  {
                return 'Pending';      
            }
        })
        ->editColumn('departure_date', function($data){
            return date('Y-m-d', strtotime($data->departure_date));            
        })
        ->addColumn('destination', function($data){
            $destination = $data->routes[0]->name.' - '.$data->routes[1]->name;
            return $destination;
        })
        ->addColumn('action_order', function($data){

            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                if ($data->order_id_digdeplus != null) {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="return false;" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
                '<a onclick="sj('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Surat Jalan" data-original-title="Download Surat Jalan"><i class="fas fa-file-download"></i> SJ</a>'.
                '<a onclick="inv('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Invoice" data-original-title="Download Invoice"><i class="fas fa-file-download"></i> Invoice</a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
                } elseif ($data->status_order == 3) {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
                '<a onclick="sj('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Surat Jalan" data-original-title="Download Surat Jalan"><i class="fas fa-file-download"></i> SJ</a>'.
                '<a onclick="inv('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Invoice" data-original-title="Download Invoice"><i class="fas fa-file-download"></i> Invoice</a>'.
                '<a onclick="ratingDriver('. $data->id .')" class="btn btn-success btn-action mr-1" data-toggle="tooltip" title="Rating Driver" data-original-title="Rating Driver"><i class="fas fa-star"></i></a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
                } elseif ($data->status_order == 4) {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="sj('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Surat Jalan" data-original-title="Download Surat Jalan"><i class="fas fa-file-download"></i> SJ</a>'.
                '<a onclick="inv('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Invoice" data-original-title="Download Invoice"><i class="fas fa-file-download"></i> Invoice</a>'.
                '<a onclick="ratingDriver('. $data->id .')" class="btn btn-success btn-action mr-1" data-toggle="tooltip" title="Rating Driver" data-original-title="Rating Driver"><i class="fas fa-star"></i></a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
                } else {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
                '<a onclick="sj('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Surat Jalan" data-original-title="Download Surat Jalan"><i class="fas fa-file-download"></i> SJ</a>'.
                '<a onclick="inv('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Invoice" data-original-title="Download Invoice"><i class="fas fa-file-download"></i> Invoice</a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
                }
            }
        })
       ->addColumn('action_payment', function($data){
        if (session('role_id') == 1) {
            return '<a onclick="detail('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Detail" data-original-title="Detail">Detail</a>';
        } else {
            return '<a onclick="inv('. $data->id .')" class="btn btn-warning btn-action mr-1" data-toggle="tooltip" title="Download Invoice" data-original-title="Download Invoice"><i class="fas fa-file-download"></i>INV</a>'.
            '<a onclick="detail('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Detail" data-original-title="Detail">Detail</a>';
        }
            
        })
        ->rawColumns(['destination', 'action_order', 'action_payment'])
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return dd($request->has('item_detail_id'));
        // ini fungsi add nya
        $request->validate([
            'company_id' => 'required',
            'customer_id' => 'required',
            'departure_date' => 'required',
            'arrival_date' => 'required',
            'armada_id' => 'required',
            'user_id' => 'required',
            'r_name' => 'required|array',
            'r_address' => 'required|array',
            'r_latitude' => 'required|array',
            'r_longitude' => 'required|array',
            'r_pic_name' => 'required|array',
            'r_pic_contact' => 'required|array',
            'r_status' => 'required|array',
            // 'item_type' => 'present|nullable',
            // 'item_weight' => 'present|string',
            // 'item_qty' => 'present|string',
            // 'image_item2' => 'nullable|string',
            // 'image_item3' => 'nullable|string',
            'image_item' => 'nullable|mimes:jpeg,jpg,png',
            'image_item2' => 'nullable|mimes:jpeg,jpg,png',
            'image_item3' => 'nullable|mimes:jpeg,jpg,png',
            'item_detail' => 'present|nullable',
            'driver_note' => 'present|nullable',
            'status_order' => 'required',
            'cust_cost_id' => 'nullable|array',
            'cust_cost_value' => 'nullable|array',
            'bruto' => 'nullable',
            'disc' => 'nullable',
            'ppn' => 'nullable',
            'pph' => 'nullable',
            'netto' => 'nullable',
            'driver_cost_id' => 'nullable|array',
            'driver_cost_value' => 'nullable|array',
            'total_budgetvalue' => 'nullable',
        ]);

        $image_item = $request->hasFile('image_item') ? 
        'data:image/'.$request->file('image_item')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_item'))) : null;

        $image_item2 = $request->hasFile('image_item2') ? 
        'data:image/'.$request->file('image_item2')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_item2'))) : null;
        
        $image_item3 = $request->hasFile('image_item3') ? 
        'data:image/'.$request->file('image_item3')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_item3'))) : null;

        $routes = array();
        foreach ($request->r_address as $key => $value) {
            array_push($routes, [
                'name' => $request->r_address[$key],
                'address' => $request->r_address[$key],
                'latitude' => $request->r_latitude[$key],
                'longitude' => $request->r_longitude[$key],
                'pic_name' => $request->r_pic_name[$key],
                'pic_contact' => $request->r_pic_contact[$key],
                'status' => $request->r_status[$key],
            ]); 
        }

        $budgetdetails = array();
        if ($request->has('driver_cost_id')) {
            if ($request->driver_cost_id != null) {
                foreach ($request->driver_cost_id as $key => $value) {
                    array_push($budgetdetails, [
                        'cost_id' => $request->driver_cost_id[$key],
                        'value' => $request->driver_cost_value[$key],
                    ]);
                }
            }
        }

        $orderdetailcosts = array();
        if ($request->has('cust_cost_id')) {
            if ($request->cust_cost_id != null) {
                foreach ($request->cust_cost_id as $key => $value) {
                    array_push($orderdetailcosts, [
                        'cost_id' => $request->cust_cost_id[$key],
                        'value' => $request->cust_cost_value[$key],
                    ]);
                }
            }
        }

        $itemdetails = array();
        if ($request->has('item_detail_id')) {
            if ($request->item_detail_id != null) {
                foreach ($request->item_detail_id as $key => $value) {
                    array_push($itemdetails, [
                        'id' => $request->id[$key],
                        'item_type_baru' => $request->item_type_sempag[$key],
                        'item_code' => $request->item_code[$key],
                        'item_name' => $request->item_name[$key],
                        'unit' => $request->unit[$key],
                        'qty' => $request->qty[$key],
                        'weight' => $request->weight[$key],
                        'dimensi' => $request->dimensi[$key],
                        'type' => 0
                    ]);
                }
            }
        }

        $bruto = array_sum(array_column($orderdetailcosts, 'value'));
        $disc = $request->disc; // value cannot be comma
        $disc_value = $bruto * $disc / 100;
        $sub_total = $bruto - $disc_value;
        $ppn = $request->ppn; // value cannot be comma
        $ppn_value = $sub_total * $ppn / 100;
        $pph = $request->pph; // value cannot be comma
        $pph_value = $sub_total * $pph / 100;
        $netto = $sub_total + $ppn_value - $pph_value;
        $total_budgetvalue = array_sum(array_column($budgetdetails, 'value'));

        $sendrequest = [
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'departure_date' => $request->departure_date,
            'arrival_date' => $request->arrival_date,
            'item_type' => $request->item_type,
            'item_weight' => $request->item_weight,
            'item_qty' => $request->item_qty,
            'item_detail' => $request->item_detail,
            'image_item' => $image_item,
            'image_item2' => $image_item2,
            'image_item3' => $image_item3,
            'bruto' => $bruto,
            'disc' => $disc,
            'disc_value' => $disc_value,
            'ppn' => $ppn,
            'ppn_value' => $ppn_value,
            'pph' => $pph,
            'pph_value' => $pph_value,
            'netto' => $netto,
            'total_budgetvalue' => $total_budgetvalue,
            'driver_note' => $request->driver_note,
            'status_order' => $request->status_order,
            'routes' => $routes,
            'budgetdetails' => $budgetdetails,
            'orderdetailcosts' => $orderdetailcosts,
            'itemdetails' => $itemdetails,
            'order_id_digdeplus' => $request->order_id_digdeplus
        ];

        // return dd($sendrequest);

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/orders', [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/orders/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'company_id' => 'required',
            'customer_id' => 'required',
            'departure_date' => 'required',
            'arrival_date' => 'required',
            'armada_id' => 'required',
            'user_id' => 'required',
            'r_name' => 'required|array',
            'r_address' => 'required|array',
            'r_latitude' => 'required|array',
            'r_longitude' => 'required|array',
            'r_pic_name' => 'required|array',
            'r_pic_contact' => 'required|array',
            'r_status' => 'required|array',
            // 'item_type' => 'present|nullable',
            // 'item_weight' => 'present|string',
            // 'item_qty' => 'present|string',
            'image_item' => 'nullable|mimes:jpeg,jpg,png',
            'image_item2' => 'nullable|mimes:jpeg,jpg,png',
            'image_item3' => 'nullable|mimes:jpeg,jpg,png',
            'item_detail' => 'present|nullable',
            'driver_note' => 'present|nullable',
            'status_order' => 'required',
            'cust_cost_id' => 'nullable|array',
            'cust_cost_value' => 'nullable|array',
            'bruto' => 'required',
            'disc' => 'required',
            'ppn' => 'required',
            'pph' => 'required',
            'netto' => 'required',
            'driver_cost_id' => 'nullable|array',
            'driver_cost_value' => 'nullable|array',
            'total_budgetvalue' => 'required',
        ]);

        $image_item = $request->hasFile('image_item') ? 
        'data:image/'.$request->file('image_item')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_item'))) : null;

        $image_item2 = $request->hasFile('image_item2') ? 
        'data:image/'.$request->file('image_item2')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_item2'))) : null;

        $image_item3 = $request->hasFile('image_item3') ? 
        'data:image/'.$request->file('image_item3')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_item3'))) : null;

        $routes = array();
        foreach ($request->r_address as $key => $value) {
            array_push($routes, [
                'name' => $request->r_address[$key],
                'address' => $request->r_address[$key],
                'latitude' => $request->r_latitude[$key],
                'longitude' => $request->r_longitude[$key],
                'pic_name' => $request->r_pic_name[$key],
                'pic_contact' => $request->r_pic_contact[$key],
                'status' => $request->r_status[$key],
            ]); 
        }

        $budgetdetails = array();
        if ($request->has('driver_cost_id')) {
            foreach ($request->driver_cost_id as $key => $value) {
                array_push($budgetdetails, [
                    'cost_id' => $request->driver_cost_id[$key],
                    'value' => $request->driver_cost_value[$key],
                    'realization' => $request->driver_cost_realization[$key]
                ]);
            }
        }

        $orderdetailcosts = array();
        if ($request->has('cust_cost_id')) {
            if ($request->cust_cost_id != null) {
                foreach ($request->cust_cost_id as $key => $value) {
                    array_push($orderdetailcosts, [
                        'cost_id' => $request->cust_cost_id[$key],
                        'value' => $request->cust_cost_value[$key],
                    ]);
                }
            }
        }

        $itemdetails = array();
        if ($request->has('item_detail_id')) {
            if ($request->item_detail_id != null) {
                foreach ($request->item_detail_id as $key => $value) {
                    array_push($itemdetails, [
                        'item_type_baru' => $request->item_type_sempag[$key],
                        'item_code' => $request->item_code[$key],
                        'item_name' => $request->item_name[$key],
                        'unit' => $request->unit[$key],
                        'qty' => $request->qty[$key],
                        'weight' => $request->weight[$key],
                        'dimensi' => $request->dimensi[$key],
                        'type' => 0
                    ]);
                }
            }
        }

        $bruto = array_sum(array_column($orderdetailcosts, 'value'));
        $disc = $request->disc; // value cannot be comma
        $disc_value = $bruto * $disc / 100;
        $sub_total = $bruto - $disc_value;
        $ppn = $request->ppn; // value cannot be comma
        $ppn_value = $sub_total * $ppn / 100;
        $pph = $request->pph; // value cannot be comma
        $pph_value = $sub_total * $pph / 100;
        $netto = $sub_total + $ppn_value - $pph_value;
        $total_budgetvalue = array_sum(array_column($budgetdetails, 'value'));

        $sendrequest = [
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'departure_date' => $request->departure_date,
            'arrival_date' => $request->arrival_date,
            'item_type' => $request->item_type,
            'item_weight' => $request->item_weight,
            'item_qty' => $request->item_qty,
            'item_detail' => $request->item_detail,
            'image_item' => $image_item,
            'image_item2' => $image_item2,
            'image_item3' => $image_item3,
            'bruto' => $bruto,
            'disc' => $disc,
            'disc_value' => $disc_value,
            'ppn' => $ppn,
            'ppn_value' => $ppn_value,
            'pph' => $pph,
            'pph_value' => $pph_value,
            'netto' => $netto,
            'total_budgetvalue' => $total_budgetvalue,
            'driver_note' => $request->driver_note,
            'status_order' => $request->status_order,
            'routes' => $routes,
            'budgetdetails' => $budgetdetails,
            'orderdetailcosts' => $orderdetailcosts,
            'itemdetails' => $itemdetails,
            'order_id_digdeplus' => $request->order_id_digdeplus
        ];

        // return dd($sendrequest);


        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/orders/'.$id, [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\RequestException $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v1/orders/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id')
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function showRating($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/orders/'.$id.'/rating', [
                'headers' => $headers
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function updateRating(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->put(config('global.url').'/api/v1/orders/'.$id.'/rating', [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function sj(Request $request, $order_id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/orders/'. $order_id . '/sj', [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);

            $pdf = $res->getBody()->getContents();
            return response()->json(['sj' => base64_encode($pdf)]);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function tandaterima(Request $request, $order_id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/orders/'. $order_id . '/tandaterima', [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);

            $pdf = $res->getBody()->getContents();
            return response()->json(['tandaterima' => base64_encode($pdf)]);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function inv(Request $request, $order_id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/orders/'. $order_id . '/inv', [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);
            
            $pdf = $res->getBody()->getContents();
            return response()->json(['inv' => base64_encode($pdf)]);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function invoices(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/invoices', [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]

            ]);
            $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->addColumn('jatuh_tempo', function($data){
            $today = date('Y-m-d');
            if ($today >= $data->due_date) {
                return '<div class="badge badge-danger">Iya</div>';
            } else {
                return '<div class="badge badge-warning">Belum</div>';
            }
        })
        ->rawColumns(['jatuh_tempo'])
        ->make(true);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    public function getViewOrder()
    {
        return view('orders.order');
    }

    public function getDataDigdeplus(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $res = $client->request('GET',env('API_URL_DIGDEPLUS').'/api/digkontrol/get_order_id',[
            'auth' => ['digkontrol_access','$2y$10$Vy73ocAKJwuzxVR6LYQL8O7Z.yKarVs57Y67IFAB80aMFUFd1q.ou'
                      ],
            'headers' => $headers,
            'json'    => [  
                            "order_id_digdeplus"=>$request->order_id_digdeplus,
                            "id_digkontrol"=>$request->id_digkontrol
            ]
        ]);
        $response = json_decode($res->getBody());
        return response()->json($response, 200);
    }
}
