<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/users/driver',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->addColumn('action', function($data){            
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
            '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
            '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
            }
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'phone' => 'required',
            'status' => 'required',
            'equipment.simtype_id' => 'required',
            'equipment.sim_expired' => 'required',
            'equipment.no_sim' => 'required',
            'equipment.no_ktp' => 'required',
            'image_sim' => 'nullable|mimes:jpeg,jpg,png',
            'hidden_image_sim' => 'present',            
            'image_ktp' =>  'nullable|mimes:jpeg,jpg,png',
            'hidden_image_ktp' => 'present',
        ]);

        $image_sim = $request->hasFile('image_sim') ? 
        'data:image/'.$request->file('image_sim')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_sim'))) : null;

        $image_ktp = $request->hasFile('image_ktp') ? 
        'data:image/'.$request->file('image_ktp')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_ktp'))) : null;

        $sendrequest = [
            'company_id' => $request->company_id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone' => $request->phone,
            'image_profile' => null,
            'status' => $request->status,
            'equipment' => [
                'simtype_id' => $request->equipment['simtype_id'],
                'sim_expired' => $request->equipment['sim_expired'],
                'no_sim' => $request->equipment['no_sim'],
                'no_ktp' => $request->equipment['no_ktp'],
                'image_sim' => $image_sim,
                'image_ktp' =>  $image_ktp 
            ]
        ];
 
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/users/driver', [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/users/driver/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id')
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            // 'email' => 'required',
            // 'password' => 'required',
            'phone' => 'required',
            'status' => 'required',
            'equipment.simtype_id' => 'required',
            'equipment.sim_expired' => 'required',
            'equipment.no_sim' => 'required',
            'equipment.no_ktp' => 'required',
            'image_sim' => 'nullable|mimes:jpeg,jpg,png',
            'hidden_image_sim' => 'present',            
            'image_ktp' =>  'nullable|mimes:jpeg,jpg,png',
            'hidden_image_ktp' => 'present',
        ]);

        $image_sim = $request->hasFile('image_sim') ? 
        'data:image/'.$request->file('image_sim')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_sim'))) : null;

        $image_ktp = $request->hasFile('image_ktp') ? 
        'data:image/'.$request->file('image_ktp')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_ktp'))) : null;

        $sendrequest = [
            'company_id' => $request->company_id,
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => $request->password,
            'phone' => $request->phone,
            'image_profile' => null,
            'status' => $request->status,
            'equipment' => [
                'simtype_id' => $request->equipment['simtype_id'],
                'sim_expired' => $request->equipment['sim_expired'],
                'no_sim' => $request->equipment['no_sim'],
                'no_ktp' => $request->equipment['no_ktp'],
                'image_sim' => $image_sim,
                'image_ktp' =>  $image_ktp 
            ]
        ];

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/users/driver/'.$id, [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v1/users/driver/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id')
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }
    
    public function getViewDriver()
    {
        return view('users.driver');
    }
}
