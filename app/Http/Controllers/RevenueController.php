<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RevenueController extends Controller
{
    public function revenue(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/revenues',[
            'headers' => $headers,
            'json'    => [
                'year' => $request->year
            ]
        ]);

        $response = json_decode($res->getBody());
        return response()->json($response, 200);

    }

    public function getViewRevenue()
    {
        return view('revenue');
    }
}
