<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        //if true = mark as read all notification
        if ($request->has('read_at') && $request->read_at == true) {
            $sendrequest = ['read_at' => true];
        } else {
            $sendrequest = [];
        }

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/notifications', [
                'headers' => $headers,
                'json' => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }
}
