<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class updatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v2/updates',[
            'headers' => $headers,
            'json'    => []
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('show', function ($data){
            if ($data->show == 0) {
                return 'Tidak';
            }  else  {
                return 'Iya';
            }
        })
        ->addColumn('action', function($data){            
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.'<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.'<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
            } else {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            }
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $poto = $request->hasFile('image_upload') ? 
        'data:image/'.$request->file('image_upload')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_upload'))) : null;
        
        $title = $request->Title;
        $desc = $request->description;
        $video = $request->video_url;
        $tampilkans = $request->tampilkan;
        $is_image = $request->is_image;

        $sendrequest = [
            'title' => $title,
            'description' => $desc,
            'photo' => $poto,
            'video_url' => $video,
            'show' => $tampilkans,
            'is_image' => $is_image
        ];

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v2/updates', [
                'headers' => $headers,
                'json' => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v2/updates/'.$id, [
                'headers' => $headers,
                'json'    => []
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        $photo = $request->hasFile('image_upload') ? 
        'data:image/'.$request->file('image_upload')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_upload'))) : null;
        $title = $request->Title;
        $desc = $request->description;
        $is_image = $request->is_image;
        $video = $request->video_url;
        
        $status = $request->tampilkan;
        if ($status=1) {
            $code=1;
        }else{
            $code=0;
        }

        $show = 1;

        // return $photo;

        $sendrequest = [
            'title' => $title,
            'description' => $desc,
            'photo' => $photo,
            'video_url' => $video,
            'is_image' => $is_image,
            'show' => $status
        ];
        

        // return ($sendrequest);
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v2/updates/'.$id, [
                'headers' => $headers,
                'json' => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v2/updates/'.$id, [
                'headers' => $headers,
                'json'    => []
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }
    
    public function getViewArmada()
    {
        return view('updates.updates');
    }
}
