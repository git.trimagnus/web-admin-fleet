<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/customers',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('tax', function ($data){
            if ($data->tax == 0) {
                return 'No';
            } else {
                return 'Yes';      
            }
        })
        ->editColumn('term', function ($data){
            if ($data->term == 7) {
                return '1 Week';
            } elseif ($data->term == 14) {
                return '2 Weeks';
            } elseif ($data->term == 21) {
                return '3 Weeks';
            } else {
                return '1 Month';      
            }
        })
        ->editColumn('status', function ($data){
            if ($data->status == 0) {
                return 'Non Active';
            } else  {
                return 'Active';      
            }
        })
        ->addColumn('action', function($data){            
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
            '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>';
            }
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'pic_name' => 'required',
            'pic_phone' => 'required',
            'pic_email' => 'required',
            'tax' => 'required',
            'term' => 'required',
            'status' => 'required',
            'is_digdeplus' => 'required',
            'company_id_isdigdeplus' => 'nullable'
        ]);
 
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/customers', [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/customers/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'pic_name' => 'required',
            'pic_phone' => 'required',
            'pic_email' => 'required',
            'tax' => 'required',
            'term' => 'required',
            'status' => 'required',
            'is_digdeplus' => 'required',
            'company_id_isdigdeplus' => 'nullable'
        ]);
 
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/customers/'.$id, [
                'headers' => $headers,
                'json'    => $request->all()
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v1/customers/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id')
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }
    
    public function getViewCustomer()
    {
        return view('customers.customer');
    }
}
