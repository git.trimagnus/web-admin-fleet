<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ArmadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/armadas',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('status', function ($data){
            if ($data->status == 0) {
                return 'Active';
            }  else  {
                return 'Non Active';
            }
        })
        ->addColumn('action', function($data){            
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
            '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>';
            }
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_stnk = $request->hasFile('image_stnk') ? 
        'data:image/'.$request->file('image_stnk')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_stnk'))) : null;

        $image_kir = $request->hasFile('image_kir') ? 
        'data:image/'.$request->file('image_kir')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_kir'))) : null;

        $t_image_tire = $request->file('t_image_tire'); //Array Image

        $tiredetails = array();
        if ($request->has('t_tire_id')) {
            foreach ($request->t_tire_id as $key => $value) {
                $image_tire = $request->hasFile('t_image_tire.'.$key) ? 
                'data:image/'.$request->file('t_image_tire.'.$key)->extension().';base64,'.base64_encode(file_get_contents($t_image_tire[$key])) : null;
    
                $tiredetails[]= [
                    'tirecondition_id' => $request->t_tirecondition_id[$key],
                    'tire_id' => $request->t_tire_id[$key],
                    'tire_number' => $request->t_tire_number[$key],
                    'image_tire' => $image_tire,
                    'merk' => $request->t_merk[$key],
                    'production_code' => $request->t_production_code[$key],
                    'serial_number' => $request->t_serial_number[$key],
                    'date_installation' => $request->t_date_installation[$key],
                    'km' => $request->t_km[$key],
                    'thick_tire' => $request->t_thick_tire[$key],
                    'tire_pressure' => $request->t_tire_pressure[$key],
                    'stamp' => $request->t_stamp[$key],
                    'description' => $request->t_description[$key]
                ];
                
            }
        }
        

        $sendrequest = [
            'company_id' => $request->company_id,
            'name' => $request->name,
            'plate_number' => $request->plate_number,
            'merk' => $request->merk,
            'series' => $request->series,
            'color' => $request->color,
            'type' => $request->type,
            'odometer' => $request->odometer,
            'year' => $request->year,
            'stnk_number' => $request->stnk_number,
            'image_armada' => '',
            'image_stnk' => $image_stnk,
            'stnk_expired' => $request->stnk_expired,
            'kir_number' => $request->kir_number,
            'image_kir' => $image_kir,
            'kir_expired' => $request->kir_expired,
            'length' => $request->length,
            'width' => $request->width,
            'height' => $request->height,
            'volume' => $request->volume,
            'max_weight' => $request->max_weight,
            'carrosserie_type' => $request->carrosserie_type,
            'carrosserie_condition' => $request->carrosserie_condition,
            'carrosserie_location' => $request->carrosserie_location,
            'insurance' => $request->insurance,
            'insurance_company' => $request->insurance_company,
            'polis_type' => $request->polis_type,
            'no_polis' => $request->no_polis,
            'insurance_expired' => $request->insurance_expired,
            'polis_period' => $request->polis_period,
            'quality_insurance' => $request->quality_insurance,
            'description_insurance' => $request->description_insurance,
            'chassis_number' => $request->chassis_number,
            'machine_number' => $request->machine_number,
            'number_tires' => $request->number_tires,
            'status' => $request->status,
            'tiredetails' => $tiredetails
        ];

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/armadas', [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/armadas/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image_stnk = $request->hasFile('image_stnk') ? 
        'data:image/'.$request->file('image_stnk')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_stnk'))) : null;

        $image_kir = $request->hasFile('image_kir') ? 
        'data:image/'.$request->file('image_kir')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_kir'))) : null;

        $t_image_tire = $request->file('t_image_tire'); //Array Image

        $tiredetails = array();
        if ($request->has('t_tire_id')) {
            foreach ($request->t_tire_id as $key => $value) {
                $image_tire = $request->hasFile('t_image_tire.'.$key) ? 
                'data:image/'.$request->file('t_image_tire.'.$key)->extension().';base64,'.base64_encode(file_get_contents($t_image_tire[$key])) : null;

                $tiredetails[]= [
                    'tirecondition_id' => $request->t_tirecondition_id[$key],
                    'tire_id' => $request->t_tire_id[$key],
                    'tire_number' => $request->t_tire_number[$key],
                    'image_tire' => $image_tire,
                    'merk' => $request->t_merk[$key],
                    'production_code' => $request->t_production_code[$key],
                    'serial_number' => $request->t_serial_number[$key],
                    'date_installation' => $request->t_date_installation[$key],
                    'km' => $request->t_km[$key],
                    'thick_tire' => $request->t_thick_tire[$key],
                    'tire_pressure' => $request->t_tire_pressure[$key],
                    'stamp' => $request->t_stamp[$key],
                    'description' => $request->t_description[$key]
                ];
            }
        }

        $sendrequest = [
            'company_id' => $request->company_id,
            'name' => $request->name,
            'plate_number' => $request->plate_number,
            'merk' => $request->merk,
            'series' => $request->series,
            'color' => $request->color,
            'type' => $request->type,
            'odometer' => $request->odometer,
            'year' => $request->year,
            'stnk_number' => $request->stnk_number,
            'image_armada' => '',
            'image_stnk' => $image_stnk,
            'stnk_expired' => $request->stnk_expired,
            'kir_number' => $request->kir_number,
            'image_kir' => $image_kir,
            'kir_expired' => $request->kir_expired,
            'length' => $request->length,
            'width' => $request->width,
            'height' => $request->height,
            'volume' => $request->volume,
            'max_weight' => $request->max_weight,
            'carrosserie_type' => $request->carrosserie_type,
            'carrosserie_condition' => $request->carrosserie_condition,
            'carrosserie_location' => $request->carrosserie_location,
            'insurance' => $request->insurance,
            'insurance_company' => $request->insurance_company,
            'polis_type' => $request->polis_type,
            'no_polis' => $request->no_polis,
            'insurance_expired' => $request->insurance_expired,
            'polis_period' => $request->polis_period,
            'quality_insurance' => $request->quality_insurance,
            'description_insurance' => $request->description_insurance,
            'chassis_number' => $request->chassis_number,
            'machine_number' => $request->machine_number,
            'number_tires' => $request->number_tires,
            'status' => $request->status,
            'tiredetails' => $tiredetails
        ];
 
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/armadas/'.$id, [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v1/armadas/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id')
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }
    
    public function getViewArmada()
    {
        return view('armadas.armada');
    }
}
