<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];
        $res = $client->get(config('global.url').'/api/v1/maintenances',[
            'headers' => $headers,
            'json'    => [
                'company_id' => $request->company_id
            ]
        ]);

        $response = json_decode($res->getBody());
        $data = $response->data;

        return DataTables::of($data)
        ->editColumn('status', function ($data){
            if ($data->status == 0) {
                return 'Open';
            } elseif ($data->status == 1) {
                return 'Progress';
            } elseif ($data->status == 2) {
                return 'Pending';
            } else  {
                return 'Done';      
            }
        })
        ->addColumn('action', function($data){
            if (session('role_id') == 1) {
                return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>';
            } else {
                if ($data->status == 3) {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
                } else {
                    return '<a onclick="showData('. $data->id .')" class="btn btn-info btn-action mr-1" data-toggle="tooltip" title="Show" data-original-title="Show"><i class="fas fa-eye"></i></a>'.
                '<a onclick="editData('. $data->id .')" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>'.
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-action mr-1" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fas fa-trash-alt"></i></a>';
                }
            }
        })
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_id' => 'required',
            'armada_id' => 'required',
            'maintenancetype_id' => 'required',
            'reporter' => 'present|nullable|string',
            'odometer' => 'present|nullable|string',
            'expected_complete' => 'present|nullable|date_format:Y-m-d',
            'chronologic' => 'present|nullable|string',
            'description_accident' => 'present|nullable|string',
            'location_accident' => 'present|nullable|string',
            'date_accident' => 'present|nullable|date_format:Y-m-d',
            'image_1' => 'nullable|mimes:jpeg,jpg,png',
            'image_2' => 'nullable|mimes:jpeg,jpg,png',
            'vendor' => 'present|nullable',
            'performance_vendor' => 'present|nullable',
            'cost' => 'present|nullable',
            'note' => 'present|nullable',
            'status' => 'required',
        ]);

        $image_1 = $request->hasFile('image_1') ? 
        'data:image/'.$request->file('image_1')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_1'))) : null;

        $image_2 = $request->hasFile('image_2') ? 
        'data:image/'.$request->file('image_2')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_2'))) : null;

        $sendrequest = [
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'maintenancetype_id' => $request->maintenancetype_id,
            'reporter' => $request->reporter,
            'odometer' => $request->odometer,
            'expected_complete' => $request->expected_complete,
            'chronologic' => $request->chronologic,
            'description_accident' => $request->description_accident,
            'location_accident' => $request->location_accident,
            'date_accident' => $request->date_accident,
            'image_1' => $image_1,
            'image_2' => $image_2,
            'vendor' => $request->vendor,
            'performance_vendor' => $request->performance_vendor,
            'cost' => $request->cost,
            'note' => $request->note,
            'status' => $request->status,
        ];

        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->post(config('global.url').'/api/v1/maintenances', [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->get(config('global.url').'/api/v1/maintenances/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => $request->company_id
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'company_id' => 'required',
            'armada_id' => 'required',
            'maintenancetype_id' => 'required',
            'reporter' => 'present|nullable|string',
            'odometer' => 'present|nullable|string',
            'expected_complete' => 'present|nullable|date_format:Y-m-d',
            'chronologic' => 'present|nullable|string',
            'description_accident' => 'present|nullable|string',
            'location_accident' => 'present|nullable|string',
            'date_accident' => 'present|nullable|date_format:Y-m-d',
            'image_1' => 'nullable|mimes:jpeg,jpg,png',
            'image_2' => 'nullable|mimes:jpeg,jpg,png',
            'vendor' => 'present|nullable',
            // 'performance_vendor' => 'present|nullable',
            'cost' => 'present|nullable',
            'note' => 'present|nullable',
            'status' => 'required',
        ]);

        $image_1 = $request->hasFile('image_1') ? 
        'data:image/'.$request->file('image_1')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_1'))) : null;

        $image_2 = $request->hasFile('image_2') ? 
        'data:image/'.$request->file('image_2')->extension().';base64,'.base64_encode(file_get_contents($request->file('image_2'))) : null;

        $sendrequest = [
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'maintenancetype_id' => $request->maintenancetype_id,
            'reporter' => $request->reporter,
            'odometer' => $request->odometer,
            'expected_complete' => $request->expected_complete,
            'chronologic' => $request->chronologic,
            'description_accident' => $request->description_accident,
            'location_accident' => $request->location_accident,
            'date_accident' => $request->date_accident,
            'image_1' => $image_1,
            'image_2' => $image_2,
            'vendor' => $request->vendor,
            'performance_vendor' => $request->performance_vendor,
            'cost' => $request->cost,
            'note' => $request->note,
            'status' => $request->status,
        ];
 
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
            ];

        try {
            $res = $client->put(config('global.url').'/api/v1/maintenances/'.$id, [
                'headers' => $headers,
                'json'    => $sendrequest
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = session('token');   
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type' => 'application/json'
        ];

        try {
            $res = $client->delete(config('global.url').'/api/v1/maintenances/'.$id, [
                'headers' => $headers,
                'json'    => [
                    'company_id' => session('company_id')
                ]
            ]);
            $response = json_decode($res->getBody());
            return response()->json($response, 200);
        }
        catch (\Exception $e) {
            $res = $e->getResponse();
            $response = json_decode($res->getBody());
            return response()->json($response, 400);
        }
    }
    
    public function getViewMaintenance()
    {
        return view('maintenances.maintenance');
    }
}
