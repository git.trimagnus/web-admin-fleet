<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('coba', function () {
    return view('coba');
});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@login')->name('login');
Route::get('password/reset', 'PasswordController@getViewPasswordReset')->name('password.reset');

Route::get('logout', 'Auth\LoginController@doLogout')->name('logout');

//These route can be used to only allow authenticated users
Route::group(['middleware' => 'auth.token'], function () {
    Route::get('/', 'HomeController@index');

    //super-admin only
    Route::group(['middleware' => 'superadmin'], function () {
        Route::get('subscription', 'SubscriptionController@getViewSubscription');
        Route::get('company-bill', 'PaymentController@getViewPayment');
        Route::get('revenue', 'RevenueController@getViewRevenue');

    });

    Route::get('company', 'CompanyController@getViewCompany');
    Route::get('company/user-addition','CompanyController@getAddMaxUser');
    Route::post('company/user-addition','CompanyController@storeAddMaxUser')->name('store.maxuser');
    Route::get('company/user-addition/index','CompanyController@indexAddUserTransaction')->name('index.maxuser');
    Route::get('company/user-addition/{id}','CompanyController@showAddUserTransaction')->name('show.maxuser');

    Route::group(['prefix' => 'user-management'], function () {
        Route::get('admin', 'Users\AdminController@getViewAdmin');
        Route::get('driver', 'Users\DriverController@getViewDriver');
        Route::get('workshop', 'Users\WorkshopController@getViewWorkshop');
    });
    
    Route::get('customer', 'CustomerController@getViewCustomer');

    Route::group(['prefix' => 'cost-management'], function () {
        Route::get('bill-cost', 'CostController@getViewBillCost');
        Route::get('actual-cost', 'CostController@getViewActualCost');
    });

    Route::group(['prefix' => 'armada-management'], function () {
        Route::get('armada', 'ArmadaController@getViewArmada');
        Route::get('checklist', 'ChecklistvehicleController@getViewChecklistvehicle');
        Route::get('reminder', 'ArmadareminderController@getViewArmadareminder');
    });


    Route::group(['prefix' => 'Whats-on'], function () {
        Route::get('onmedia', 'onmediaController@getViewArmada');
        // Route::post('/onmedias', 'onmediaController@store')->name('onmedias');
    });

    Route::group(['prefix' => 'Whats-on'], function () {
        Route::get('event', 'EventController@getViewArmada');
        // Route::post('/onmedias', 'onmediaController@store')->name('onmedias');
    });

    Route::group(['prefix' => 'Whats-on'], function () {
        Route::get('updates', 'updatesController@getViewArmada');
        // Route::post('/onmedias', 'onmediaController@store')->name('onmedias');
    });

    /*Add Whats on di website*/
   
    
    Route::get('order', 'OrderController@getViewOrder');
    Route::get('payment', 'BillingdetailController@getViewPayment');
    Route::get('payment/{order_id}', 'BillingdetailController@getViewBillingdetail');

    Route::get('maintenance', 'MaintenanceController@getViewMaintenance');

    Route::get('report/fuel-report', 'ReportController@getViewFuelReport');
    Route::get('report/armada-performance', 'ReportController@getViewReportArmadaPerformance');
    Route::get('report/insurance-performance', 'ReportController@getViewReportInsurancePerformance');
    Route::get('report/delivery-order', 'ReportController@getViewReportDeliveryOrder');
    Route::get('report/profit-and-loss', 'ReportController@getViewProfitAndLoss');
    Route::get('report/workshop-performance', 'ReportController@getViewReportWorkshopPerformance');
    Route::get('report/external-workshop', 'ReportController@getViewReportExternalWorkshop');
    Route::get('report/rating-driver', 'ReportController@getViewReportRatingDriver');
    Route::get('report/outstanding-driver', 'ReportController@getViewReportOutstandingDriver');
    Route::get('report/outstanding-customer', 'ReportController@getViewReportOutstandingCustomer');

});

// API V1
Route::group(['prefix' => 'api/v2'], function() {
    Route::apiResource('onmedias', 'onmediaController');
    Route::apiResource('updates', 'updatesController');
    Route::apiResource('events', 'EventController');
});

Route::group(['prefix' => 'api/v1'], function() {
    Route::get('/', 'HomeController@index');
    Route::post('login', 'Auth\LoginController@doLogin');

    Route::post('forgot-password/email', 'PasswordController@sendResetLinkEmail');

    Route::get('notifications', 'NotificationController@index');
    
    Route::get('dashboard', 'DashboardController@index');
    Route::get('dashboard/super-admin', 'DashboardController@dashboardSuperAdmin');

    Route::apiResource('companies', 'CompanyController')->only(['index', 'show', 'update']);

    Route::apiResource('subscriptions', 'SubscriptionController');

    Route::group(['prefix' => 'users'], function () {
        Route::get('admin-workshop', 'Users\UserController@index');
        Route::apiResource('admin', 'Users\AdminController');
        Route::apiResource('driver', 'Users\DriverController');
        Route::apiResource('workshop', 'Users\WorkshopController');
        Route::put('change-password', 'PasswordController@change');
    });
    
    Route::apiResource('customers', 'CustomerController');
    Route::apiResource('simtypes', 'SimtypeController');
    Route::apiResource('costs', 'CostController');

    Route::apiResource('odomoterhistories', 'OdometerhistoryController');

    Route::apiResource('armadas', 'ArmadaController');
    Route::apiResource('checklistvehicles', 'ChecklistvehicleController');
    Route::apiResource('armadareminders', 'ArmadareminderController');
    Route::apiResource('tires', 'TireController');
    Route::apiResource('orders', 'OrderController');
    Route::get('orders/{order_id}/rating', 'OrderController@showRating');
    Route::patch('orders/{order_id}/rating', 'OrderController@updateRating');
    Route::get('orders/{order_id}/sj', 'OrderController@sj');
    Route::get('orders/{order_id}/tandaterima', 'OrderController@tandaterima');
    Route::get('orders/{order_id}/inv', 'OrderController@inv');
    Route::get('invoices', 'OrderController@invoices');
    Route::get('schedules/armada-available', 'ScheduleController@scheduleArmadaAvailable');
    Route::get('schedules/driver-available', 'ScheduleController@scheduleDriverAvailable');

    Route::get('get-digdeplus','OrderController@getDataDigdeplus');

    Route::apiResource('payments', 'PaymentController');
    Route::get('revenues', 'RevenueController@revenue');

    Route::apiResource('billingdetails', 'BillingdetailController');
    Route::apiResource('maintenances', 'MaintenanceController');

    Route::apiResource('maintenances', 'MaintenanceController');

    Route::get('years', 'YearController@index');

    //Report
    Route::get('reports/fuel-report', 'ReportController@fuelReport');
    Route::get('reports/armada-performance', 'ReportController@armadaPerformance');
    Route::get('reports/insurance-performance', 'ReportController@insurancePerformance');
    Route::get('reports/delivery-order', 'ReportController@deliveryOrder');
    Route::get('reports/profit-and-loss', 'ReportController@profitAndLoss');
    Route::get('reports/workshop-performance', 'ReportController@workshopPerformance');
    Route::get('reports/external-workshop', 'ReportController@externalWorkshop');
    Route::get('reports/rating-driver', 'ReportController@ratingDriver');
    Route::get('reports/outstanding-driver', 'ReportController@outstandingDriver');
    Route::get('reports/outstanding-customer', 'ReportController@outstandingCustomer');


    //Export Report Excel
    Route::get('reports/fuel-report/xls', 'ExcelController@fuelReport');
    Route::get('reports/armada-performance/xls', 'ExcelController@armadaPerformance');
    Route::get('reports/insurance-performance/xls', 'ExcelController@insurancePerformance');
    Route::get('reports/delivery-order/xls', 'ExcelController@deliveryOrder');
    Route::get('reports/profit-and-loss/xls', 'ExcelController@profitAndLoss');
    Route::get('reports/workshop-performance/xls', 'ExcelController@workshopPerformance');
    Route::get('reports/external-workshop/xls', 'ExcelController@externalWorkshop');
    Route::get('reports/rating-driver/xls', 'ExcelController@ratingDriver');
    Route::get('reports/outstanding-driver/xls', 'ExcelController@outstandingDriver');
    Route::get('reports/outstanding-customer/xls', 'ExcelController@outstandingCustomer');


});
